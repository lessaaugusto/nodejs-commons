import { Logger } from "./packages/nodejs-logger/src";

beforeAll(() => {
  Logger.sharedInit({ serviceName: "nodejs-commons" });
});
