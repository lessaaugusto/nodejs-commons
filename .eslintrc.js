const path = require("path");
const eslintConfig = require("@nosebit/eslint-config");

module.exports = eslintConfig.configBuild({
  configBuilderOptions: {
    tsConfigFile: path.resolve(__dirname, "tsconfig.json"),
  },
  rules: {
    "jsdoc/require-description-complete-sentence": [
      "off",
    ],
  },
  settings: {
    "import/external-module-folders": [
      "@nosebit"
    ]
  }
});
