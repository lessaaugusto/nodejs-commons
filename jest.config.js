/* eslint-disable import/no-commonjs */

module.exports = {
  collectCoverage: true,
  collectCoverageFrom: [
    "src/**/*.ts",
    "!**/*.d.ts",
    "!**/*.test.ts",
  ],
  coverageDirectory: "coverage",
  forceExit: true,
  moduleDirectories: [
    "<rootDir>",
    "<rootDir>/packages",
    "node_modules",
  ],
  moduleFileExtensions: [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node",
  ],
  moduleNameMapper: {
    "^@nosebit/nodejs-logger": "<rootDir>/packages/nodejs-logger/src", // eslint-disable-line max-len
  },
  setupFilesAfterEnv: [
    "./jest.setup.ts",
  ],
  testRegex: "(/__tests__/.*|(\\.|/)(test|spec))\\.tsx?$",
  transform: {
    "^.+\\.tsx?$": "ts-jest",
  },
};
