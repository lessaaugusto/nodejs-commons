import { Logger } from "@nosebit/nodejs-logger";

import { RaftPeer } from "./index";

// Initialize common modules
Logger.sharedInit({
  level: "debug",
  serviceName: "raft-test",
});

// Create 3 peers
const p1 = new RaftPeer({
  bootstrapExpect: 3,
  listenHost: "127.0.0.1:9123",
  name: "P1",
});

const p2 = new RaftPeer({
  bootstrapExpect: 3,
  joinUrl: "127.0.0.1:9123",
  listenHost: "127.0.0.1:9124",
  name: "P2",
});

const p3 = new RaftPeer({
  bootstrapExpect: 3,
  joinUrl: "127.0.0.1:9123",
  listenHost: "127.0.0.1:9125",
  name: "P3",
});

// Name to peer map
const hostToPeer: {[key: string]: RaftPeer} = {
  "127.0.0.1:9123": p1,
  "127.0.0.1:9124": p2,
  "127.0.0.1:9125": p3,
};

const TIMEOUT = 30000;

// After some time simulate a failure on leader
setTimeout(() => {
  const { leaderHost } = p1;
  const leader = hostToPeer[leaderHost];
  leader.simulateFailure();
}, TIMEOUT);
