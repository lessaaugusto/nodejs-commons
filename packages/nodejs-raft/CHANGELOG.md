# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.2.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-raft@2.2.0...@nosebit/nodejs-raft@2.2.1) (2019-11-30)

**Note:** Version bump only for package @nosebit/nodejs-raft





# [2.2.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-raft@2.1.1...@nosebit/nodejs-raft@2.2.0) (2019-11-30)


### Bug Fixes

* Update yarn.lock which was making lint and test to fail on ci. ([119aad5](https://github.com/nosebit/nodejs-commons/commit/119aad539d6f808e9ff3c7eaf2484445a486d69f))


### Features

* Add volta config and replace redis by ioredis. ([24f11f2](https://github.com/nosebit/nodejs-commons/commit/24f11f2cc2ca16538c0457c5ccd3de562db74051))





## [2.1.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-raft@2.1.0...@nosebit/nodejs-raft@2.1.1) (2019-11-29)

**Note:** Version bump only for package @nosebit/nodejs-raft





# [2.1.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-raft@2.0.0...@nosebit/nodejs-raft@2.1.0) (2019-11-28)


### Features

* Add volta config and replace redis by ioredis. ([cdc915a](https://github.com/nosebit/nodejs-commons/commit/cdc915a))





# [2.0.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-raft@0.5.2...@nosebit/nodejs-raft@2.0.0) (2019-11-27)


### Bug Fixes

* Fix call to old Logger.init in raft playground. ([ee3da23](https://github.com/nosebit/nodejs-commons/commit/ee3da23))
* Upgrade @types/node version which was conflicting with typescript own internal types and prevent lib from being built. ([f18913f](https://github.com/nosebit/nodejs-commons/commit/f18913f))
* Use ~ in package.jsons instead of ^. ([d31040a](https://github.com/nosebit/nodejs-commons/commit/d31040a))


### Features

* New way of handling shared instances. ([79cc300](https://github.com/nosebit/nodejs-commons/commit/79cc300))


### BREAKING CHANGES

* Not using mutable exports anymore and removed init and shared functions to initialize shared instance.





# [1.0.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-raft@0.5.3...@nosebit/nodejs-raft@1.0.0) (2019-11-15)


### Bug Fixes

* Fix call to old Logger.init in raft playground. ([86043ec](https://github.com/nosebit/nodejs-commons/commit/86043ec))


### Features

* New way of handling shared instances. ([8b67d8f](https://github.com/nosebit/nodejs-commons/commit/8b67d8f))


### BREAKING CHANGES

* Not using mutable exports anymore and removed init and shared functions to initialize shared instance.





## [0.5.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-raft@0.5.2...@nosebit/nodejs-raft@0.5.3) (2019-11-10)

**Note:** Version bump only for package @nosebit/nodejs-raft





## [0.5.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-raft@0.5.1...@nosebit/nodejs-raft@0.5.2) (2019-08-21)

**Note:** Version bump only for package @nosebit/nodejs-raft





## [0.5.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-raft@0.5.0...@nosebit/nodejs-raft@0.5.1) (2019-07-10)

**Note:** Version bump only for package @nosebit/nodejs-raft





# [0.5.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-raft@0.4.2...@nosebit/nodejs-raft@0.5.0) (2019-07-06)


### Features

* **nodejs-raft:** Add flag to force leadership of current instance. ([1412d03](https://github.com/nosebit/nodejs-commons/commit/1412d03))





## [0.4.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-raft@0.4.1...@nosebit/nodejs-raft@0.4.2) (2019-06-20)

**Note:** Version bump only for package @nosebit/nodejs-raft





## [0.4.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-raft@0.4.0...@nosebit/nodejs-raft@0.4.1) (2019-06-09)

**Note:** Version bump only for package @nosebit/nodejs-raft





# 0.4.0 (2019-05-12)


### Features

* migrate current code from github. ([7da766d](https://github.com/nosebit/nodejs-commons/commit/7da766d))
