"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Imports
//#####################################################
const dns_1 = require("@nosebit/nodejs-utils/lib/dns");
const moment_1 = __importDefault(require("moment"));
const winston = __importStar(require("winston"));
const winston_logstash_transport_1 = require("winston-logstash-transport");
//#####################################################
// Local variables
//#####################################################
// The global winston driver
let _driver;
//#####################################################
// Main function
//#####################################################
/**
 * This function retreives a global winston driver.
 *
 * @param opts - A set of config options.
 */
function winstonDriver(opts = {}) {
    if (_driver) {
        return _driver;
    }
    // Ignore log messages if the have { private: true }
    const ignorePrivate = winston.format((info) => {
        if (info.private) {
            return false;
        }
        return info;
    });
    const { combine, timestamp, printf, colorize, } = winston.format;
    // Create winston logger
    _driver = winston.createLogger({
        format: winston.format.json(),
        level: opts.level || "info",
    });
    if (process.env.LOG_TO_CONSOLE || process.env.NODE_ENV !== "prod") {
        _driver.add(new winston.transports.Console({
            format: combine(colorize(), ignorePrivate(), timestamp(), printf((info) => {
                let dataStr = "";
                let traceStr = "";
                if (info.data) {
                    try {
                        dataStr = JSON.stringify(info.data);
                    }
                    catch (error) {
                        dataStr = info.data;
                    }
                    if (dataStr && dataStr.length) {
                        dataStr = ` - ${dataStr}`;
                    }
                }
                if (info.trace) {
                    try {
                        traceStr = JSON.stringify(info.trace);
                    }
                    catch (error) {
                        traceStr = info.trace;
                    }
                    if (traceStr && traceStr.length) {
                        traceStr = ` - ${traceStr}`;
                    }
                }
                const suffix = info.suffix
                    ? ` ${info.suffix}`
                    : "";
                const time = moment_1.default(info.timestamp).format("HH:mm:ss:SSS");
                const callerName = info.callerName ? ` ${info.callerName}` : "";
                const message = info.message ? ` ${info.message}` : "";
                return `${time} ${info.level} ${callerName}${message}${suffix}${dataStr}${traceStr}`; // eslint-disable-line max-len
            })),
        }));
    }
    if (process.env.LOG_TO_FILE) {
        _driver.add(new winston.transports.File({
            filename: "error.log",
            level: "error",
        }));
        _driver.add(new winston.transports.File({
            filename: "combined.log",
        }));
    }
    if (process.env.LOGSTASH_URL) {
        const urlParts = dns_1.getUrlParts(process.env.LOGSTASH_URL);
        const logstashOpts = {
            host: urlParts.hostname,
            node_name: "logstash",
            port: urlParts.port,
        };
        _driver.add(new winston_logstash_transport_1.LogstashTransport(logstashOpts));
    }
    return _driver;
}
exports.default = winstonDriver;
//# sourceMappingURL=winston.js.map