"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Imports
//#####################################################
const jaeger_client_1 = require("jaeger-client");
const v4_1 = __importDefault(require("uuid/v4"));
const __1 = require("..");
//#####################################################
// Constants
//#####################################################
// Tracer key id
const TRACE_ID = "uber-trace-id";
//#####################################################
// Main class
//#####################################################
/**
 * This class implements a jaeger tracer.
 */
class JaegerTracer {
    /**
     * This function creates a new jaeger tracer instance.
     *
     * @param config - The config options.
     */
    constructor(config) {
        this._driver = jaeger_client_1.initTracer({
            reporter: {
                agentHost: config.host || "localhost",
            },
            sampler: {
                host: config.host || "localhost",
                param: 1,
                type: "const",
            },
            serviceName: config.serviceName,
        }, {
            logger: __1.Logger.sharedExists()
                ? __1.Logger.shared
                : null,
        });
    }
    /**
     * This function starts a new span.
     *
     * @param operationName - Name of the operation to be traced.
     * @param config - A set of config options.
     */
    startSpan(operationName, config) {
        return this._driver.startSpan(operationName, config);
    }
    /**
     * This function build a span object from context id.
     *
     * @param id - The context id.
     */
    getSpanFromId(id) {
        const ctx = id
            ? this._driver.extract("http_headers", { [TRACE_ID]: id })
            : null;
        return ctx;
    }
    /**
     * This function extract the trace id from the span object.
     *
     * @param span - The span object.
     */
    getTraceId(span) {
        if (!span) {
            return v4_1.default();
        }
        const headers = {
            [TRACE_ID]: "",
        };
        this._driver.inject(span, "http_headers", headers);
        return headers[TRACE_ID];
    }
}
exports.default = JaegerTracer;
//# sourceMappingURL=jaeger.js.map