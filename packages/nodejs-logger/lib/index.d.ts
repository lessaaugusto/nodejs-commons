import JaegerTracer from "./tracers/jaeger";
/**
 * The unit of tracing.
 */
export interface ITracerSpan {
    finish(finishTime?: number): void;
}
/**
 * The options to create spans.
 */
export interface ITracerSpanConfig {
    childOf?: ITracerSpan | object;
}
/**
 * The tracer interface.
 */
export interface ITracer {
    startSpan(operationName: string, config?: ITracerSpanConfig): ITracerSpan;
    getSpanFromId(id: string): ITracerSpan | object;
    getTraceId(span: ITracerSpan): string;
}
/**
 * Options for trace method.
 */
interface ITraceMethodConfig {
    allowExternalTracing?: boolean;
    args?: any[];
    argsToLog?: string[];
    fileName?: string;
    methodId: string;
    parentCtx: LogContext;
    preventTracing?: boolean;
    resultToLog?: string[] | boolean;
}
/**
 * Logger driver interface
 */
export interface ILoggerDriver {
    log(level: string, message: string, meta?: any): void;
}
/**
 * The logger config
 */
interface ILoggerConfig {
    driver?: ILoggerDriver;
    fileName?: string;
    level?: string;
    tracer?: ITracer;
    traceInfo?: TraceInfo;
}
/**
 * Log method config options.
 */
interface ILogMeta {
    fileName?: string;
}
/**
 * Interface for logger meta info.
 */
interface ILoggerMeta {
    traceId: string;
}
/**
 * This is the main class which is used to log and trace
 * a piece of code.
 */
declare class Logger {
    /**
     * Shared instance.
     */
    private static _shared;
    /**
     * Shared service name.
     */
    private static _sharedServiceName;
    /**
     * Shared tracer.
     */
    private static _sharedTracer;
    /**
     * Shared log level.
     */
    private static _sharedLogLevel;
    /**
     * This function initializes a shared logger.
     *
     * @param config - A set of configs.
     * @param config.serviceName - The service name owning the logger factory.
     * @param config.level - The logging level.
     * @param config.tracer - The tracer.
     */
    static sharedInit(config?: {
        serviceName: string;
        level?: string;
        tracer?: ITracer;
    }): Logger;
    /**
     * This function get the shared service name.
     */
    static get serviceName(): string;
    /**
     * This function retrieves the global service name.
     */
    static get shared(): Logger;
    /**
     * This function checks if we have a shared logger.
     */
    static sharedExists(): boolean;
    /**
     * This function retrieves the shared tracer.
     */
    static get tracer(): ITracer;
    /**
     * This function decode a trace id string in it's parts.
     *
     * @param  id - The trace id to decode.
     */
    static decodeTraceId(id: string): {
        contextId: string;
        stackId: string;
    } | {
        contextId?: undefined;
        stackId?: undefined;
    };
    /**
     * This function decode a trace id string in it's parts.
     *
     * @param contextId - The trace id to decode.
     * @param stackId - The stack id.
     */
    static encodeTraceIds(contextId?: string, stackId?: string): string;
    /**
     * This is the scope id for this logger.
     */
    readonly scopeId?: string;
    /**
     * The fileName to be used in all logging.
     */
    readonly fileName?: string;
    /**
     * Entity which drives the logging.
     */
    readonly driver: ILoggerDriver;
    /**
     * Entity which drives the tracing.
     */
    readonly tracer: ITracer;
    /**
     * Trace info for this logger.
     */
    readonly traceInfo: TraceInfo;
    /**
     * This is a getter to retrieve correct trace id from this logger trace context.
     */
    get traceId(): string;
    /**
     * Creates an instance of Logger.
     *
     * @param scopeId - Name identifying the scope of logging.
     * @param config - A set of config options.
     */
    constructor(scopeId: string, config?: ILoggerConfig);
    /**
     * This function implements a generic log function which is
     * the base to all other methods.
     *
     * @param level - The log level (info, error, etc).
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of metadata.
     */
    private _log;
    /**
     * This function creates a debug log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    debug(message: string, data?: any, meta?: ILogMeta): void;
    /**
     * This function creates an info log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    info(message: string, data?: any, meta?: ILogMeta): void;
    /**
     * This function creates a warning log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    warn(message: string, data?: any, meta?: ILogMeta): void;
    /**
     * This function creates an error log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     * @param meta - A set of config options.
     */
    error(message: string, data?: any, meta?: ILogMeta): void;
    /**
     * This function emits a critical error log.
     *
     * @param message - The message to be logged.
     * @param data - Data to be logged.
     */
    critical(message: string, data?: any): void;
    /**
     * This function creates a new logger based on this logger.
     *
     * @param scopeId - The scope of new logger.
     */
    fork(scopeId: string): Logger;
}
/**
 * The trace info.
 */
declare class TraceInfo {
    /**
     * A string identifying the context stack.
     */
    stackId?: string;
    /**
     * A string identifying the tracer context.
     */
    id?: string;
    /**
     * The method name associated with this context.
     */
    method?: string;
    /**
     * The span associated with this context.
     */
    span?: ITracerSpan;
    /**
     * The parent context in the stack.
     */
    parent?: TraceInfo;
    /**
     * Creates an instance of TraceInfo.
     */
    constructor();
}
/**
 * The log context info is what gets passed along functions
 * for logging and tracing.
 */
declare class LogContext<T extends ILoggerMeta = any> {
    /**
     * This function creates a log context from a trace id.
     *
     * @param traceId - The trace id string.
     * @param baseCtx - A base context to be used.
     */
    static fromTraceId(traceId: string, baseCtx?: LogContext): LogContext<any>;
    /**
     * This function creates a log context from a logger meta.
     *
     * @param meta - The trace id string.
     * @param baseCtx - A base context to be used.
     */
    static fromMeta(meta: ILoggerMeta, baseCtx?: LogContext): LogContext<any>;
    /**
     * This functions only return data if we are in envorionments other
     * than production.
     *
     * @param data - The secret data to log if we are allowed to (dev like envs).
     */
    static secret<T>(data: T): T | string;
    /**
     * This function is the main log function and it's used to trace
     * execution of a piece of code.
     *
     * @param config - The trace config.
     * @param methodToTrace - The method to be traced.
     */
    static trace<T extends ILoggerMeta = any>(config: ITraceMethodConfig, methodToTrace: (ctx: LogContext) => any): any;
    /**
     * The trace info.
     */
    traceInfo: TraceInfo;
    /**
     * The logger for the context.
     */
    logger: Logger;
    /**
     * Meta data.
     */
    meta: T;
    /**
     * This getter retrives the traceId from traceInfo.
     */
    get traceId(): string;
    /**
     * This function going to clone this context mergin it with
     * some provided data.
     */
    clone(): LogContext<any>;
}
/**
 * This function decorates a method so it can be logged.
 *
 * @param config - Config options.
 * @param target - The target.
 * @param key - The key.
 * @param descriptor - The descriptor.
 */
declare function log<T extends ILoggerMeta = any>(config?: {
    allowExternalTracing?: boolean;
    argsToLog?: string[];
    ctxArgIdx?: number;
    loggerPrefix?: (tracerBag: object) => string;
    preventTracing?: boolean;
    resultToLog?: string[] | boolean;
    skipArgs?: number[];
}): (...decArgs: any[]) => void;
declare const globalCtx: LogContext<any>;
export { globalCtx, JaegerTracer, log, LogContext, Logger, TraceInfo, };
