/* eslint-disable max-classes-per-file */

import Sentry from "@sentry/node";
import lodash from "lodash";
import moment from "moment";
import shortid from "shortid";
import uuid from "uuid";

import winstonDriver from "./drivers/winston";
import JaegerTracer from "./tracers/jaeger";

//#####################################################
// Types
//#####################################################
/**
 * The unit of tracing.
 */
export interface ITracerSpan {
  finish(finishTime?: number): void;
}

/**
 * The options to create spans.
 */
export interface ITracerSpanConfig {
  childOf?: ITracerSpan | object;
}

/**
 * The tracer interface.
 */
export interface ITracer {
  startSpan(operationName: string, config?: ITracerSpanConfig): ITracerSpan;
  getSpanFromId(id: string): ITracerSpan | object;
  getTraceId(span: ITracerSpan): string;
}

/**
 * Options for trace method.
 */
interface ITraceMethodConfig {
  allowExternalTracing?: boolean;
  args?: any[];
  argsToLog?: string[];
  fileName?: string;
  methodId: string;
  parentCtx: LogContext;
  preventTracing?: boolean;
  resultToLog?: string[] | boolean;
}

/**
 * Logger driver interface
 */
export interface ILoggerDriver {
  log(level: string, message: string, meta?: any): void;
}

/**
 * The logger config
 */
interface ILoggerConfig {
  driver?: ILoggerDriver;
  fileName?: string;
  level?: string;
  tracer?: ITracer;
  traceInfo?: TraceInfo;
}

/**
 * Log method config options.
 */
interface ILogMeta {
  fileName?: string;
}

/**
 * Interface for logger meta info.
 */
interface ILoggerMeta {
  traceId: string;
}

//#####################################################
// Auxiliary Functions
//#####################################################
/**
 * This function going to build a map to log with function
 * arguments.
 *
 * @param args - The function arguments.
 * @param argsToLog - A list of arg spec which we should use
 * to log the arg.
 */
function argsMapToLog(
  args: any[] = [],
  argsToLog: string[],
) {
  const argsMap: {[key: string]: any} = {};

  lodash.forEach(argsToLog, (argSpec: string, idx: number) => {
    const argValue = lodash.get(args, `${idx}`);

    if (argSpec === "_" || lodash.isFunction(argValue)) {
      return;
    }

    const dotIdx = argSpec.indexOf(".");
    let argName: string = null;
    let argValPaths: string[] = [];
    let argValueToLog = argValue;

    if (dotIdx > 0) {
      argName = argSpec.substr(0, dotIdx);
      const argSpecVal = argSpec.substr(dotIdx + 1);
      const multiValPathsMatch = (/{[^{}]+}/).exec(argSpecVal);

      if (multiValPathsMatch) {
        const valPathsStr = multiValPathsMatch[0].replace(/{|}/g, "");
        argValPaths = valPathsStr.split(/, ?/g);
      }
    } else {
      argName = argSpec;
    }

    if (argValPaths.length > 0) {
      argValueToLog = {};

      argValPaths.forEach((valPathWithOpt) => {
        // Check for options
        const valPathParts = valPathWithOpt.split(":");
        const valPath = valPathParts.length ? valPathParts[0] : null;
        const valPathOpt = valPathParts.length ? valPathParts[1] : null;

        if (valPathOpt !== "secret" || process.env.NODE_ENV !== "prod") {
          lodash.set(
            argValueToLog,
            valPath,
            lodash.get(argValue, valPath),
          );
        }
      });
    }

    argsMap[argName] = argValueToLog;
  });

  return argsMap;
}

/**
 * Get caller.
 */
function getCallerFile() {
  const originalFunc = Error.prepareStackTrace;

  let callerfile;

  try {
    const error = new Error();

    Error.prepareStackTrace = function prepareStackTrace(_error, stack) {
      return stack;
    };

    const stack: any = error.stack; // eslint-disable-line prefer-destructuring
    const currentfile = stack.shift().getFileName();

    while (error.stack.length) {
      callerfile = stack.shift().getFileName();

      if (currentfile !== callerfile) {
        break;
      }
    }
  } catch (error) {}

  Error.prepareStackTrace = originalFunc;

  return callerfile;
}

//#####################################################
// Main class
//#####################################################
/**
 * This is the main class which is used to log and trace
 * a piece of code.
 */
class Logger {
  /**
   * Shared instance.
   */
  private static _shared: Logger;

  /**
   * Shared service name.
   */
  private static _sharedServiceName: string;

  /**
   * Shared tracer.
   */
  private static _sharedTracer: ITracer;

  /**
   * Available log levels.
   */
  private static readonly _logLevels = [
    "error",
    "warn",
    "info",
    "debug",
  ];

  /**
   * Shared log level.
   */
  private static _sharedLogLevel: string = [
    "prod",
    "rc",
  ].findIndex((item) => item === process.env.NODE_ENV) >= 0 // eslint-disable-line no-magic-numbers
    ? "info"
    : "debug";

  /**
   * This function initializes a shared logger.
   *
   * @param config - A set of configs.
   * @param config.serviceName - The service name owning the logger factory.
   * @param config.level - The logging level.
   * @param config.tracer - The tracer.
   */
  public static sharedInit(config: {
    serviceName: string;
    level?: string;
    tracer?: ITracer;
  } = {
    serviceName: "",
  }): Logger {
    if (!Logger._shared) {
      Logger._sharedServiceName = config.serviceName;
      Logger._sharedTracer = config.tracer;

      if (config.level) {
        Logger._sharedLogLevel = config.level;
      }

      Logger._shared = new Logger("<ROOT>");

      if (process.env.SENTRY_URL) {
        Sentry.init({
          dsn: process.env.SENTRY_URL,
        });
      }
    }

    return Logger._shared;
  }

  /**
   * This function get the shared service name.
   */
  public static get serviceName() {
    return Logger._sharedServiceName || "app";
  }

  /**
   * This function retrieves the global service name.
   */
  public static get shared() {
    if (!Logger._shared) {
      throw new Error("shared logger not initialized");
    }

    return Logger._shared;
  }

  /**
   * This function checks if we have a shared logger.
   */
  public static sharedExists() {
    return Boolean(Logger._shared);
  }

  /**
   * This function retrieves the shared tracer.
   */
  public static get tracer() {
    return Logger._sharedTracer;
  }

  /**
   * This function decode a trace id string in it's parts.
   *
   * @param  id - The trace id to decode.
   */
  public static decodeTraceId(id: string) {
    // Check if string is a trace id.
    if (id.startsWith("@TRACE_ID_")) {
      const ids = id.replace(/^@TRACE_ID_/, "").split(/_@STACK_ID_/);

      return {
        contextId: ids.length ? ids[0] : null,
        stackId: ids.length > 1 ? ids[1] : null,
      };
    }

    return {};
  }

  /**
   * This function decode a trace id string in it's parts.
   *
   * @param contextId - The trace id to decode.
   * @param stackId - The stack id.
   */
  public static encodeTraceIds(
    contextId: string = uuid(),
    stackId: string = shortid.generate(),
  ) {
    return `@TRACE_ID_${contextId}_@STACK_ID_${stackId || ""}`;
  }

  /**
   * This is the scope id for this logger.
   */
  public readonly scopeId?: string;

  /**
   * The fileName to be used in all logging.
   */
  public readonly fileName?: string;

  /**
   * Entity which drives the logging.
   */
  public readonly driver: ILoggerDriver;

  /**
   * Entity which drives the tracing.
   */
  public readonly tracer: ITracer;

  /**
   * Trace info for this logger.
   */
  public readonly traceInfo: TraceInfo;

  /**
   * This is a getter to retrieve correct trace id from this logger trace context.
   */
  get traceId() {
    let traceId: string;

    if (this.traceInfo) {
      const contextId = this.tracer.getTraceId(this.traceInfo.span);
      const { stackId } = this.traceInfo;

      traceId = Logger.encodeTraceIds(contextId, stackId);
    } else {
      // Set random trace id.
      traceId = Logger.encodeTraceIds(uuid());
    }

    return traceId;
  }

  /**
   * Creates an instance of Logger.
   *
   * @param scopeId - Name identifying the scope of logging.
   * @param config - A set of config options.
   */
  constructor(scopeId: string, config: ILoggerConfig = {}) {
    this.scopeId = scopeId;
    this.fileName = config.fileName;
    this.traceInfo = config.traceInfo;

    this.driver = config.driver
      || winstonDriver({
        level: Logger._sharedLogLevel,
      });

    this.tracer = config.tracer;
  }

  /**
   * This function implements a generic log function which is
   * the base to all other methods.
   *
   * @param level - The log level (info, error, etc).
   * @param message - The message to be logged.
   * @param data - Data to be logged.
   * @param meta - A set of metadata.
   */
  private _log(
    level: string,
    message: string,
    data?: any,
    meta: ILogMeta = {},
  ) {
    // Check if we going to log.
    const currentLevelIdx = Logger._logLevels.indexOf(
      Logger._sharedLogLevel,
    );
    const levelIdx = Logger._logLevels.indexOf(level);

    if (levelIdx < 0 || levelIdx > currentLevelIdx) {
      return;
    }

    // Get caller filepath.
    let { fileName } = meta;

    // Prepend service name to filepath.
    if (fileName) {
      fileName = `${Logger.serviceName}/${fileName.replace(/^\//, "")}`;
    }

    // Parse message
    const scopedMessage = this.scopeId
      ? `${this.scopeId} : ${message}`
      : message;

    // Try to convert data to plain object.
    let plainData;

    try {
      plainData = JSON.parse(JSON.stringify(data));
    } catch (error) {
      // Could not convert data to plain object.
    }

    this.driver.log(level, scopedMessage, {
      data: plainData,
      serviceName: Logger.serviceName,
      trace: {
        fileName,
        stackId: this.traceInfo ? this.traceInfo.stackId : null,
      },
    });
  }

  /**
   * This function creates a debug log.
   *
   * @param message - The message to be logged.
   * @param data - Data to be logged.
   * @param meta - A set of config options.
   */
  public debug(message: string, data?: any, meta?: ILogMeta) {
    this._log("debug", message, data, meta);
  }

  /**
   * This function creates an info log.
   *
   * @param message - The message to be logged.
   * @param data - Data to be logged.
   * @param meta - A set of config options.
   */
  public info(message: string, data?: any, meta?: ILogMeta) {
    this._log("info", message, data, meta);
  }

  /**
   * This function creates a warning log.
   *
   * @param message - The message to be logged.
   * @param data - Data to be logged.
   * @param meta - A set of config options.
   */
  public warn(message: string, data?: any, meta?: ILogMeta) {
    this._log("warn", message, data, meta);
  }

  /**
   * This function creates an error log.
   *
   * @param message - The message to be logged.
   * @param data - Data to be logged.
   * @param meta - A set of config options.
   */
  public error(message: string, data?: any, meta?: ILogMeta) {
    this._log("error", message, data, meta);
  }

  /**
   * This function emits a critical error log.
   *
   * @param message - The message to be logged.
   * @param data - Data to be logged.
   */
  public critical(message: string, data?: any) {
    this._log("error", message, data);

    if (Sentry) {
      Sentry.captureException({ data, message });
    }
  }

  /**
   * This function creates a new logger based on this logger.
   *
   * @param scopeId - The scope of new logger.
   */
  public fork(scopeId: string) {
    return new Logger(`${this.scopeId}.${scopeId}`);
  }
}

//#####################################################
// Auxiliary Classes
//#####################################################
/**
 * The trace info.
 */
class TraceInfo {
  /**
   * A string identifying the context stack.
   */
  public stackId?: string;

  /**
   * A string identifying the tracer context.
   */
  public id?: string;

  /**
   * The method name associated with this context.
   */
  public method?: string;

  /**
   * The span associated with this context.
   */
  public span?: ITracerSpan;

  /**
   * The parent context in the stack.
   */
  public parent?: TraceInfo;

  /**
   * Creates an instance of TraceInfo.
   */
  constructor() {
    this.id = `@TRACE_ID_${uuid()}`;
    this.stackId = shortid();
  }
}

/**
 * The log context info is what gets passed along functions
 * for logging and tracing.
 */
class LogContext<T extends ILoggerMeta = any> {
  /**
   * This function creates a log context from a trace id.
   *
   * @param traceId - The trace id string.
   * @param baseCtx - A base context to be used.
   */
  public static fromTraceId(
    traceId: string,
    baseCtx?: LogContext,
  ) {
    let ctx: LogContext;

    if (baseCtx) {
      ctx = baseCtx.clone();
    } else {
      ctx = new LogContext();
      ctx.traceInfo = new TraceInfo();
    }

    ctx.traceInfo.id = traceId;

    return ctx;
  }

  /**
   * This function creates a log context from a logger meta.
   *
   * @param meta - The trace id string.
   * @param baseCtx - A base context to be used.
   */
  public static fromMeta(
    meta: ILoggerMeta,
    baseCtx?: LogContext,
  ) {
    let ctx: LogContext;

    if (baseCtx) {
      ctx = baseCtx.clone();
    } else {
      ctx = new LogContext();
      ctx.traceInfo = new TraceInfo();
    }

    ctx.traceInfo.id = meta.traceId;

    return ctx;
  }

  /**
   * This functions only return data if we are in envorionments other
   * than production.
   *
   * @param data - The secret data to log if we are allowed to (dev like envs).
   */
  public static secret<T>(data: T): T | string {
    return process.env.NODE_END === "prod"
      ? "<SECRET>"
      : data;
  }

  /**
   * This function is the main log function and it's used to trace
   * execution of a piece of code.
   *
   * @param config - The trace config.
   * @param methodToTrace - The method to be traced.
   */
  public static trace<T extends ILoggerMeta = any>(
    config: ITraceMethodConfig,
    methodToTrace: (ctx: LogContext) => any,
  ) {
    /**
     * If we don't want any tracing functionality then we can just create
     * a simple context which contains just a logger.
     */
    if (config.preventTracing) {
      const simpleCtx = new LogContext();

      simpleCtx.traceInfo = new TraceInfo();
      simpleCtx.traceInfo.id = `@TRACE_ID_${uuid()}`;
      simpleCtx.traceInfo.stackId = shortid.generate();

      simpleCtx.logger = new Logger(config.methodId); // eslint-disable-line no-use-before-define

      return methodToTrace(simpleCtx);
    }

    const spanLabel = config.fileName
      ? `${config.fileName}:${config.methodId}`
      : config.methodId;

    // Create a new context info.
    const ctx = new LogContext<T>();

    // Create new trace info
    const info = new TraceInfo();
    ctx.traceInfo = info;

    // Set context meta from parent
    if (config.parentCtx && config.parentCtx.meta) {
      ctx.meta = config.parentCtx.meta;
    }

    // Set parent context
    if (config.parentCtx && config.parentCtx.traceInfo) {
      info.parent = config.parentCtx.traceInfo;
      info.stackId = `${info.parent.stackId}/${shortid.generate()}`;
    } else {
      info.stackId = shortid.generate();
    }

    // Set trace method name
    info.method = config.methodId;

    // Set parent method
    const parentMethod = info.parent && info.parent.method
      ? info.parent.method
      : "<ROOT>";

    // Start an internal timer.
    const start = moment();

    // Log start
    const fromStr = parentMethod
      ? `(from: ${parentMethod})`
      : null;

    // Start the trace span.
    if (config.allowExternalTracing && Logger.tracer) {
      info.span = Logger.tracer.startSpan(spanLabel, {
        childOf: info.parent
          ? info.parent.span
          : Logger.tracer.getSpanFromId(
            config.parentCtx && config.parentCtx.traceId,
          ),
      });

      // Set trace info id.
      info.id = `@TRACE_ID_${Logger.tracer.getTraceId(info.span)}`;
    } else {
      // Set random trace id.
      info.id = `@TRACE_ID_${uuid()}`;
    }

    // Creates a logger for the method context.
    const logger = new Logger(info.method, { traceInfo: info }); // eslint-disable-line no-use-before-define
    ctx.logger = logger;

    /**
     * Get args that should be logged and create log map. Suppose we have a
     * function with signature myFun(arg1, ..., argN). Then this trace function
     * can receive the list of arguments as args=[arg1, ..., argN] and a list
     * argsToLog mapping which arguments we want to log. For example:
     *
     * - `argsToLog=[arg1Name]` : Going to log just the first argument using
     * arg1Name as name (prints an object with arg1Name:arg1 kv pair);
     * - `argsToLog=["_", arg2Name]` : Going to log just the second argument.
     * - `argsToLog=[arg1Name, "arg2.subarg1"]` : Going to log just the subarg1 key
     * of arg2 object.
     * - `argsToLog=[arg1Name, "arg2.{subarg1,subarg2}"]` : Going to log just
     * subarg1 and subarg 2 of arg2 object.
     */
    const argsMap = argsMapToLog(
      config.args,
      config.argsToLog,
    );

    const meta: any = {};

    if (config.fileName) {
      meta.fileName = config.fileName;
    }

    // Log start of method
    logger.info(`start ${fromStr}`, argsMap, meta);

    // Method response
    const response = methodToTrace(ctx);

    /**
     * Encapsulate finishignin trace into a function to deal with promises.
     *
     * @param result - The results recieved.
     */
    const finish = (result: any) => {
      // Finish the span
      if (info.span) {
        info.span.finish();
      }

      // Stop the internal timer
      const end = moment();
      const duration = moment.duration(end.diff(start)).as("milliseconds");

      // Build result to log.
      let parsedResult: any;
      let resultToLog: any;

      // Try to convert result to a plain object.
      try {
        parsedResult = JSON.parse(JSON.stringify(result));
      } catch (error) {
        parsedResult = result;
      }

      if (lodash.isArray(config.resultToLog)) {
        resultToLog = argsMapToLog(
          parsedResult,
          config.resultToLog,
        );
      }

      // Log success
      if (lodash.isBoolean(config.resultToLog) && !config.resultToLog) {
        logger.info(
          `success (duration: ${duration}ms)`,
          null,
          meta,
        );
      } else {
        logger.info(
          `success (duration: ${duration}ms)`,
          resultToLog,
          meta,
        );
      }

      return result;
    };

    // Method returns a promise.
    if (response instanceof Promise) {
      response
        .then(finish)
        .catch((error) => {
          // Stop the internal timer
          const end = moment();
          const duration = moment.duration(end.diff(start)).as("milliseconds");

          // Log error
          logger.error(
            `fail (duration: ${duration}ms)`,
            error.message,
            { fileName: config.fileName },
          );

          return error;
        });
    } else {
      finish(response);
    }

    return response;
  }

  /**
   * The trace info.
   */
  public traceInfo: TraceInfo;

  /**
   * The logger for the context.
   */
  public logger: Logger;

  /**
   * Meta data.
   */
  public meta: T;

  /**
   * This getter retrives the traceId from traceInfo.
   */
  get traceId() {
    return this.traceInfo && this.traceInfo.id;
  }

  /**
   * This function going to clone this context mergin it with
   * some provided data.
   */
  public clone() {
    const cloneCtx = new LogContext();
    cloneCtx.logger = this.logger;

    cloneCtx.traceInfo = new TraceInfo();
    Object.assign(cloneCtx.traceInfo, this.traceInfo);

    return cloneCtx;
  }
}

//#####################################################
// Decorators
//#####################################################
/**
 * This function decorates a method so it can be logged.
 *
 * @param config - Config options.
 * @param target - The target.
 * @param key - The key.
 * @param descriptor - The descriptor.
 */
function log<T extends ILoggerMeta = any>(
  config: {
    allowExternalTracing?: boolean;
    argsToLog?: string[];
    ctxArgIdx?: number;
    loggerPrefix?: (tracerBag: object) => string;
    preventTracing?: boolean;
    resultToLog?: string[] | boolean;
    skipArgs?: number[];
  } = {},
) {
  return function decorator(...decArgs: any[]) {
    if (decArgs.length !== 3 || typeof decArgs[2] === "number") {
      throw new Error("Method does not allow decoration");
    }

    /* eslint-disable prefer-destructuring */
    const target: {
      constructor: {
        name: string;
      };
    } = decArgs[0];

    const descriptor: PropertyDescriptor = decArgs[2];
    /* eslint-enable prefer-destructuring */

    const originalMethod = descriptor.value;
    const methodName = originalMethod.name;

    descriptor.value = function value(...args: any[]) {
      if (!Logger.sharedExists()) {
        return originalMethod.apply(this, args);
      }

      let fileName = getCallerFile();

      if (fileName) {
        fileName = fileName.replace(`${process.cwd()}/`, "");
      }

      const className = target.constructor.name;

      // Try to get parent log context passed through.
      const lastArg = lodash.last(args);
      let parentCtx: LogContext<T>;

      if (lastArg instanceof LogContext) {
        parentCtx = args.pop();
      }

      return LogContext.trace({
        allowExternalTracing: config.allowExternalTracing,
        args,
        argsToLog: config.argsToLog,
        fileName,
        methodId: `${className}.${methodName}`,
        parentCtx,
        preventTracing: config.preventTracing,
      }, (ctx: LogContext) => {
        let childArgs = args;

        /**
         * If user provided a specific position for context, then
         * let's adjust args to be sure ctx going to be in right
         * index. This is important when method has optional args
         * with default values.
         */
        if (config.ctxArgIdx > 0 && args.length < config.ctxArgIdx) {
          childArgs = [
            ...args,
            ...(new Array(config.ctxArgIdx - args.length)), // eslint-disable-line @typescript-eslint/no-extra-parens
          ];
        }

        // Push log context into the args.
        childArgs.push(ctx);

        return originalMethod.apply(this, childArgs);
      });
    };
  };
}

//#####################################################
// Global default context
//#####################################################
const globalCtx = new LogContext();
globalCtx.logger = new Logger("global");

//#####################################################
// Export
//#####################################################
export {
  globalCtx,
  JaegerTracer,
  log,
  LogContext,
  Logger,
  TraceInfo,
};
