//#####################################################
// Imports
//#####################################################
import { getUrlParts } from "@nosebit/nodejs-utils/lib/dns";
import moment from "moment";
import * as winston from "winston";
import { LogstashTransport } from "winston-logstash-transport";

import { ILoggerDriver } from "..";

//#####################################################
// Types
//#####################################################
/**
 * Interface for winston driver options.
 */
interface IWinstonDriverOpts {
  level?: string;
}

//#####################################################
// Local variables
//#####################################################
// The global winston driver
let _driver: winston.Logger;

//#####################################################
// Main function
//#####################################################
/**
 * This function retreives a global winston driver.
 *
 * @param opts - A set of config options.
 */
function winstonDriver(
  opts: IWinstonDriverOpts = {},
): ILoggerDriver {
  if (_driver) {
    return _driver;
  }

  // Ignore log messages if the have { private: true }
  const ignorePrivate = winston.format((info) => {
    if (info.private) {
      return false;
    }

    return info;
  });

  const {
    combine,
    timestamp,
    printf,
    colorize,
  } = winston.format;

  // Create winston logger
  _driver = winston.createLogger({
    format: winston.format.json(),
    level: opts.level || "info",
  });

  if (process.env.LOG_TO_CONSOLE || process.env.NODE_ENV !== "prod") {
    _driver.add(new winston.transports.Console({
      format: combine(
        colorize(),
        ignorePrivate(),
        timestamp(),
        printf((info) => {
          let dataStr = "";
          let traceStr = "";

          if (info.data) {
            try {
              dataStr = JSON.stringify(info.data);
            } catch (error) {
              dataStr = info.data;
            }

            if (dataStr && dataStr.length) {
              dataStr = ` - ${dataStr}`;
            }
          }

          if (info.trace) {
            try {
              traceStr = JSON.stringify(info.trace);
            } catch (error) {
              traceStr = info.trace;
            }

            if (traceStr && traceStr.length) {
              traceStr = ` - ${traceStr}`;
            }
          }

          const suffix = info.suffix
            ? ` ${info.suffix}`
            : "";

          const time = moment(info.timestamp).format("HH:mm:ss:SSS");
          const callerName = info.callerName ? ` ${info.callerName}` : "";
          const message = info.message ? ` ${info.message}` : "";

          return `${time} ${info.level} ${callerName}${message}${suffix}${dataStr}${traceStr}`; // eslint-disable-line max-len
        }),
      ),
    }));
  }

  if (process.env.LOG_TO_FILE) {
    _driver.add(
      new winston.transports.File({
        filename: "error.log",
        level: "error",
      }),
    );

    _driver.add(
      new winston.transports.File({
        filename: "combined.log",
      }),
    );
  }

  if (process.env.LOGSTASH_URL) {
    const urlParts = getUrlParts(process.env.LOGSTASH_URL);
    const logstashOpts: any = {
      host: urlParts.hostname,
      node_name: "logstash", // eslint-disable-line @typescript-eslint/camelcase
      port: urlParts.port,
    };

    _driver.add(
      new LogstashTransport(logstashOpts),
    );
  }

  return _driver as ILoggerDriver;
}

//#####################################################
// Export
//#####################################################
export {
  winstonDriver as default,
};
