# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.2.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-logger@2.2.0...@nosebit/nodejs-logger@2.2.1) (2019-11-30)

**Note:** Version bump only for package @nosebit/nodejs-logger





# [2.2.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-logger@2.1.1...@nosebit/nodejs-logger@2.2.0) (2019-11-30)


### Bug Fixes

* **nodejs-db:** Start using winston-logstash-transport to make logstash work. ([3861acf](https://github.com/nosebit/nodejs-commons/commit/3861acf5ec66fc9964d717fa7e7e9cd88441399d))
* Update yarn.lock which was making lint and test to fail on ci. ([119aad5](https://github.com/nosebit/nodejs-commons/commit/119aad539d6f808e9ff3c7eaf2484445a486d69f))


### Features

* Add volta config and replace redis by ioredis. ([24f11f2](https://github.com/nosebit/nodejs-commons/commit/24f11f2cc2ca16538c0457c5ccd3de562db74051))





## [2.1.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-logger@2.1.0...@nosebit/nodejs-logger@2.1.1) (2019-11-29)


### Bug Fixes

* **nodejs-db:** Start using winston-logstash-transport to make logstash work. ([ad30163](https://github.com/nosebit/nodejs-commons/commit/ad30163))





# [2.1.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-logger@2.0.0...@nosebit/nodejs-logger@2.1.0) (2019-11-28)


### Features

* Add volta config and replace redis by ioredis. ([cdc915a](https://github.com/nosebit/nodejs-commons/commit/cdc915a))





# [2.0.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-logger@0.5.1...@nosebit/nodejs-logger@2.0.0) (2019-11-27)


### Bug Fixes

* Upgrade @types/node version which was conflicting with typescript own internal types and prevent lib from being built. ([f18913f](https://github.com/nosebit/nodejs-commons/commit/f18913f))
* Use ~ in package.jsons instead of ^. ([d31040a](https://github.com/nosebit/nodejs-commons/commit/d31040a))


### Features

* New way of handling shared instances. ([79cc300](https://github.com/nosebit/nodejs-commons/commit/79cc300))
* Some features required for squad27. ([4cf9cc2](https://github.com/nosebit/nodejs-commons/commit/4cf9cc2))


### BREAKING CHANGES

* Not using mutable exports anymore and removed init and shared functions to initialize shared instance.





# [1.0.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-logger@0.6.0...@nosebit/nodejs-logger@1.0.0) (2019-11-15)


### Features

* New way of handling shared instances. ([8b67d8f](https://github.com/nosebit/nodejs-commons/commit/8b67d8f))


### BREAKING CHANGES

* Not using mutable exports anymore and removed init and shared functions to initialize shared instance.





# [0.6.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-logger@0.5.1...@nosebit/nodejs-logger@0.6.0) (2019-11-10)


### Features

* Some features required for squad27. ([4cf9cc2](https://github.com/nosebit/nodejs-commons/commit/4cf9cc2))





## [0.5.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-logger@0.5.0...@nosebit/nodejs-logger@0.5.1) (2019-08-21)

**Note:** Version bump only for package @nosebit/nodejs-logger





# [0.5.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-logger@0.4.2...@nosebit/nodejs-logger@0.5.0) (2019-07-10)


### Features

* **nodejs-logger:** Add option to create context from traceId and base context. ([674587c](https://github.com/nosebit/nodejs-commons/commit/674587c))





## [0.4.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-logger@0.4.1...@nosebit/nodejs-logger@0.4.2) (2019-07-06)

**Note:** Version bump only for package @nosebit/nodejs-logger





## [0.4.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-logger@0.4.0...@nosebit/nodejs-logger@0.4.1) (2019-06-09)

**Note:** Version bump only for package @nosebit/nodejs-logger





# 0.4.0 (2019-05-12)


### Features

* migrate current code from github. ([7da766d](https://github.com/nosebit/nodejs-commons/commit/7da766d))
