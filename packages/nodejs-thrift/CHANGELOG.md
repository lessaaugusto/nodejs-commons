# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.7.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-thrift@0.7.0...@nosebit/nodejs-thrift@0.7.1) (2019-11-30)

**Note:** Version bump only for package @nosebit/nodejs-thrift





# [0.7.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-thrift@0.6.1...@nosebit/nodejs-thrift@0.7.0) (2019-11-30)


### Features

* Add volta config and replace redis by ioredis. ([24f11f2](https://github.com/nosebit/nodejs-commons/commit/24f11f2cc2ca16538c0457c5ccd3de562db74051))





## [0.6.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-thrift@0.6.0...@nosebit/nodejs-thrift@0.6.1) (2019-11-29)

**Note:** Version bump only for package @nosebit/nodejs-thrift





# [0.6.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-thrift@0.5.5...@nosebit/nodejs-thrift@0.6.0) (2019-11-28)


### Features

* Add volta config and replace redis by ioredis. ([cdc915a](https://github.com/nosebit/nodejs-commons/commit/cdc915a))





## [0.5.5](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-thrift@0.5.2...@nosebit/nodejs-thrift@0.5.5) (2019-11-27)


### Bug Fixes

* Upgrade @types/node version which was conflicting with typescript own internal types and prevent lib from being built. ([f18913f](https://github.com/nosebit/nodejs-commons/commit/f18913f))
* Use ~ in package.jsons instead of ^. ([d31040a](https://github.com/nosebit/nodejs-commons/commit/d31040a))





## [0.5.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-thrift@0.5.3...@nosebit/nodejs-thrift@0.5.4) (2019-11-15)

**Note:** Version bump only for package @nosebit/nodejs-thrift





## [0.5.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-thrift@0.5.2...@nosebit/nodejs-thrift@0.5.3) (2019-11-10)

**Note:** Version bump only for package @nosebit/nodejs-thrift





## [0.5.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-thrift@0.5.1...@nosebit/nodejs-thrift@0.5.2) (2019-08-21)

**Note:** Version bump only for package @nosebit/nodejs-thrift





## [0.5.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-thrift@0.5.0...@nosebit/nodejs-thrift@0.5.1) (2019-07-10)

**Note:** Version bump only for package @nosebit/nodejs-thrift





# [0.5.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-thrift@0.4.1...@nosebit/nodejs-thrift@0.5.0) (2019-07-06)


### Features

* **nodejs-thrift:** Expose method to disconnect thrift client. ([f538783](https://github.com/nosebit/nodejs-commons/commit/f538783))





## [0.4.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-thrift@0.4.0...@nosebit/nodejs-thrift@0.4.1) (2019-06-09)

**Note:** Version bump only for package @nosebit/nodejs-thrift





# 0.4.0 (2019-05-12)


### Features

* migrate current code from github. ([7da766d](https://github.com/nosebit/nodejs-commons/commit/7da766d))
