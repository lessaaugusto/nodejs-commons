import { LogContext } from "@nosebit/nodejs-logger";
import thrift from "thrift";
/**
 * This class create a base interface for all services.
 */
declare class ThriftService<TClientType> {
    /**
     * The client used to comunicate with service instance.
     */
    private _client;
    /**
     * The service constructor.
     */
    private readonly _srv;
    /**
     * The service connection.
     */
    private _connection;
    /**
     * Client getter.
     */
    get client(): TClientType;
    /**
     * This function creates a new instance of this class.
     *
     * @param srv - The service constructor.
     */
    constructor(srv: thrift.TClientConstructor<TClientType>);
    /**
     * This function connects this service to a remote service instance.
     *
     * @param url - The url of the remote service instance.
     * @param ctx - The log context.
     */
    connect(url: string, ctx?: LogContext): Promise<string>;
    /**
     * This function disconnects this service from a remote
     * service instance.
     *
     * @param ctx - The log context.
     */
    disconnect(): void;
}
export { ThriftService as default, };
