"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Imports
//#####################################################
const nodejs_logger_1 = require("@nosebit/nodejs-logger");
const dns_1 = require("@nosebit/nodejs-utils/lib/dns");
const thrift_1 = __importDefault(require("thrift"));
//#####################################################
// Main class
//#####################################################
/**
 * This class create a base interface for all services.
 */
class ThriftService {
    /**
     * This function creates a new instance of this class.
     *
     * @param srv - The service constructor.
     */
    constructor(srv) {
        this._srv = srv;
    }
    /**
     * Client getter.
     */
    get client() {
        return this._client;
    }
    /**
     * This function connects this service to a remote service instance.
     *
     * @param url - The url of the remote service instance.
     * @param ctx - The log context.
     */
    connect(url, ctx = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                /**
                 * This function create a new connection.
                 *
                 * @param urlParts - The url object of target service.
                 */
                const createConnection = (urlParts) => {
                    this._connection = thrift_1.default.createConnection(urlParts.hostname, urlParts.port, {
                        max_attempts: 100,
                    });
                    this._client = thrift_1.default.createClient(this._srv, this._connection);
                    this._connection.on("connect", () => {
                        const parts = urlParts.hostname.split(".");
                        const serviceName = parts.length > 0 ? parts[0] : null;
                        if (serviceName) {
                            ctx.logger.debug(`service ${serviceName} connected`);
                        }
                        resolve();
                    });
                    // On error, log it.
                    this._connection.on("error", (error) => {
                        ctx.logger.error("thrift createConnection error", error);
                        reject(error);
                    });
                };
                const originalUrlParts = dns_1.getUrlParts(url);
                // If no port was provided, then let's try to get it.
                if (originalUrlParts.port) {
                    createConnection(originalUrlParts);
                }
                else {
                    dns_1.resolveUrl(originalUrlParts.hostname).then((newUrlParts) => {
                        ctx.logger.debug(`service address resolved : ${url}`, newUrlParts.port);
                        createConnection(newUrlParts);
                    }).catch((error) => {
                        ctx.logger.error("could not resolve url", error);
                    });
                }
            });
        });
    }
    /**
     * This function disconnects this service from a remote
     * service instance.
     *
     * @param ctx - The log context.
     */
    disconnect() {
        if (this._connection) {
            this._connection.end();
        }
    }
}
__decorate([
    nodejs_logger_1.log({
        argsToLog: ["url"],
    })
], ThriftService.prototype, "connect", null);
__decorate([
    nodejs_logger_1.log()
], ThriftService.prototype, "disconnect", null);
exports.default = ThriftService;
//# sourceMappingURL=service.js.map