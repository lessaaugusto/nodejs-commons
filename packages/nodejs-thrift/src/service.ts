//#####################################################
// Imports
//#####################################################
import {
  globalCtx,
  log,
  LogContext,
} from "@nosebit/nodejs-logger";
import {
  getUrlParts,
  IUrlParts,
  resolveUrl,
} from "@nosebit/nodejs-utils/lib/dns";
import thrift from "thrift";

//#####################################################
// Main class
//#####################################################
/**
 * This class create a base interface for all services.
 */
class ThriftService<TClientType> {
  /**
   * The client used to comunicate with service instance.
   */
  private _client: TClientType;

  /**
   * The service constructor.
   */
  private readonly _srv: thrift.TClientConstructor<TClientType>;

  /**
   * The service connection.
   */
  private _connection: thrift.Connection;

  /**
   * Client getter.
   */
  public get client(): TClientType {
    return this._client;
  }

  /**
   * This function creates a new instance of this class.
   *
   * @param srv - The service constructor.
   */
  constructor(srv: thrift.TClientConstructor<TClientType>) {
    this._srv = srv;
  }

  /**
   * This function connects this service to a remote service instance.
   *
   * @param url - The url of the remote service instance.
   * @param ctx - The log context.
   */
  @log({
    argsToLog: ["url"],
  })
  public async connect(
    url: string,
    ctx: LogContext = globalCtx,
  ): Promise<string> {
    return new Promise((resolve, reject) => {
      /**
       * This function create a new connection.
       *
       * @param urlParts - The url object of target service.
       */
      const createConnection = (urlParts: IUrlParts) => {
        this._connection = thrift.createConnection(
          urlParts.hostname,
          urlParts.port, {
            max_attempts: 100, // eslint-disable-line @typescript-eslint/camelcase
          },
        );

        this._client = thrift.createClient(
          this._srv,
          this._connection
        );

        this._connection.on("connect", () => {
          const parts = urlParts.hostname.split(".");
          const serviceName = parts.length > 0 ? parts[0] : null;

          if (serviceName) {
            ctx.logger.debug(`service ${serviceName} connected`);
          }

          resolve();
        });

        // On error, log it.
        this._connection.on("error", (error) => {
          ctx.logger.error("thrift createConnection error", error);
          reject(error);
        });
      };

      const originalUrlParts = getUrlParts(url);

      // If no port was provided, then let's try to get it.
      if (originalUrlParts.port) {
        createConnection(originalUrlParts);
      } else {
        resolveUrl(originalUrlParts.hostname).then((newUrlParts) => {
          ctx.logger.debug(
            `service address resolved : ${url}`,
            newUrlParts.port,
          );
          createConnection(newUrlParts);
        }).catch((error) => {
          ctx.logger.error("could not resolve url", error);
        });
      }
    });
  }

  /**
   * This function disconnects this service from a remote
   * service instance.
   *
   * @param ctx - The log context.
   */
  @log()
  public disconnect(): void {
    if (this._connection) {
      this._connection.end();
    }
  }
}

//#####################################################
// Export
//#####################################################
export {
  ThriftService as default,
};
