"use strict";
/* eslint-disable import/prefer-default-export */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Imports
//#####################################################
const events_1 = __importDefault(require("events"));
const nodejs_logger_1 = require("@nosebit/nodejs-logger");
const node_rdkafka_1 = __importDefault(require("node-rdkafka"));
const lodash_1 = __importDefault(require("lodash"));
const moment_1 = __importDefault(require("moment"));
//#####################################################
// Main class
//#####################################################
/**
 * This function implements a kafka client. The typical
 * way to use this is to instantiate a producer only
 * instante using something like the following.
 *
 * ```bash
 * Kafka.init({
 *   producerOnly: true,
 *   kafkaHost: ...
 * });
 * ```
 *
 * or to create a producer/consumer.
 *
 * ```bash
 * Kafka.init({
 *   consumerTopics: [user],
 *   kafkaHost: ...
 * });
 * ```
 */
class KafkaClient {
    /**
     * This function creates a new instance of this class.
     *
     * @param config - The client config.
     */
    constructor(config) {
        /**
         * The producer queue which store all producer messages while
         * producer is not ready yet.
         */
        this._producerQueue = [];
        /**
         * Indicates whether producer is ready.
         */
        this._producerIsReady = false;
        /**
         * Indicates if producer is already disconnected.
         */
        this._isProducerDisconnected = false;
        /**
         * Indicates if consumer is already disconnected.
         */
        this._isConsumerDisconnected = false;
        /**
         * Consumer callbacks.
         */
        this._consumerCallbacks = {};
        this._initialized = this._init(config);
    }
    /**
     * This static function retrieves the shared instance.
     *
     * @param config - The client config.
     */
    static sharedInit(config) {
        if (!KafkaClient._shared) {
            KafkaClient._shared = new KafkaClient(config);
        }
        return KafkaClient._shared;
    }
    /**
     * This function gets the shared client.
     */
    static get shared() {
        if (!KafkaClient._shared) {
            throw new Error("kafka shared client not initialized");
        }
        return KafkaClient._shared;
    }
    /**
     * Check if shared instance exists.
     */
    static sharedExists() {
        return Boolean(KafkaClient._shared);
    }
    /**
     * This getter retrieves the initialized property.
     */
    get initialized() {
        return this._initialized;
    }
    /**
     * This function initializes the client.
     *
     * @param config - A set of config options.
     */
    _init(config) {
        return __awaiter(this, void 0, void 0, function* () {
            if (config.useNodeEmitter) {
                this._emitter = new events_1.default();
            }
            this._env = config.env;
            this._kafkaHost = config.kafkaHost;
            const consumerOpts = Object.assign({}, (config.consumerOpts || {}));
            const producerOpts = Object.assign({ "batch.num.messages": 1000000, "client.id": "kafka", "compression.codec": "none", "dr_cb": true, "message.send.max.retries": 10, "queue.buffering.max.messages": 100000, "queue.buffering.max.ms": 1000, "retry.backoff.ms": 200, "socket.keepalive.enable": true }, (config.producerOpts || {}));
            // Create the consumer.
            if (this._kafkaHost && !config.producerOnly) {
                const consumer = config.createConsumer
                    ? config.createConsumer(consumerOpts)
                    : new node_rdkafka_1.default.KafkaConsumer(Object.assign(Object.assign({}, consumerOpts), { "metadata.broker.list": this._kafkaHost }), {});
                this._consumer = yield this._setupConsumer(consumer, config.consumerTopics);
            }
            // Create the producer
            if (this._kafkaHost && !config.consumerOnly) {
                const producer = config.createProducer
                    ? config.createProducer(producerOpts)
                    : new node_rdkafka_1.default.Producer(Object.assign(Object.assign({}, producerOpts), { "metadata.broker.list": this._kafkaHost }));
                this._producerIsReady = false;
                this._producer = yield this._setupProducer(producer);
            }
        });
    }
    /**
     * This function setups a consumer for this client.
     *
     * @param consumer - Consumer to config.
     * @param opts - The consumer config options.
     * @param topics - The topic we going to consume.
     * @param ctx - The log context.
     */
    _setupConsumer(consumer, topics, { logger } = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            const self = this;
            if (!this._kafkaHost) {
                return null;
            }
            // Process topics to add env.
            let topicsWithEnv = lodash_1.default.flatten([topics]);
            if (this._env) {
                topicsWithEnv = topicsWithEnv.map((topic) => `${topic}.${this._env}`);
            }
            return new Promise((resolve) => {
                // Log when producer is ready.
                consumer.on("ready", () => {
                    logger.info("consumer ready");
                    // Subscribe to events
                    consumer.subscribe(topicsWithEnv);
                    /**
                     * Consume from the topics. This is what determines
                     * the mode we are running in. By not specifying a
                     * callback (or specifying only a callback) we get
                     * messages as soon as they are available.
                     */
                    consumer.consume();
                    // Return consumer to the caller.
                    resolve(consumer);
                });
                // Log when consumer report an error.
                consumer.on("event.error", (error) => {
                    logger.error("consumer error", error);
                    // @TODO : Retry a little more times upon error.
                    resolve(consumer);
                });
                // Consume incoming messages coming from kafka.
                consumer.on("data", (data) => {
                    logger.info("new consumer data", nodejs_logger_1.LogContext.secret(data));
                    self.consumeMessage(data);
                });
                // Connect to the broker manually
                consumer.connect();
            });
        });
    }
    /**
     * This function setups a producer for this client.
     *
     * @param producer - The producer instance to setup.
     * @param ctx - The log context.
     */
    _setupProducer(producer, { logger } = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            const self = this;
            if (!this._kafkaHost) {
                return Promise.resolve(null);
            }
            return new Promise((resolve) => {
                // Connect to the broker manually
                producer.connect();
                // Log when producer is ready.
                producer.on("ready", () => {
                    logger.info("producer is ready");
                    self._producerIsReady = true;
                    // Send all queued messages.
                    while (self._producerQueue.length > 0) { // eslint-disable-line no-magic-numbers
                        const { key, data } = self._producerQueue.shift();
                        this.emit(key, data);
                    }
                    resolve(producer);
                });
                // On producer error close connection and try again later.
                producer.on("event.error", (error) => {
                    logger.error("producer error", error);
                });
            });
        });
    }
    /**
     * This function consume a new incoming message.
     *
     * @param message - Incoming message.
     * @param ctx - Log context.
     */
    consumeMessage(message, { logger } = nodejs_logger_1.globalCtx) {
        const messageValue = message.value.toString();
        let value;
        try {
            value = messageValue
                ? JSON.parse(messageValue)
                : message;
        }
        catch (error) {
            logger.error("could not parse message", error);
            value = {
                data: messageValue,
                key: message.topic,
            };
        }
        /**
         * Find out the key coming from remote sender (from
         * message). The key sent is something in the form:
         *
         * TOPIC.ENV:EVT_NAME.
         */
        const keyParts = value.key.split(":");
        const topicNameWithEnv = keyParts.length ? keyParts[0] : null;
        const evtName = keyParts.length > 1 ? keyParts[1] : null;
        const topicNameParts = topicNameWithEnv.split(".");
        const topicName = topicNameParts.length ? topicNameParts[0] : null;
        const env = topicNameParts.length > 1 ? topicNameParts[1] : null;
        const keyWithoutEnv = evtName
            ? `${topicName}:${evtName}`
            : topicName;
        /**
         * Prevent processing event from different env. This way
         * if the code running this kafka client is running in
         * 'prod' mode it going to ignore any events coming from
         * any other env ('dev' for example).
         */
        if ((!env && !this._env) || env === this._env) {
            /**
             * Now we going to iterate over all registered consumer
             * callbacks to verify which callback we going to call
             * (i.e., which match the message key).
             */
            const keysWithCallbacks = Object.keys(this._consumerCallbacks);
            keysWithCallbacks.forEach((keyWithListener) => {
                const targetKeyParts = keyWithListener.split(":");
                const targetTopicName = targetKeyParts.length
                    ? targetKeyParts[0]
                    : null;
                const targetEvtName = targetKeyParts.length > 1
                    ? targetKeyParts[1]
                    : null;
                /**
                 * Check if keyWithListener match incoming key.
                 */
                if (keyWithListener === "*"
                    || (topicName === targetTopicName
                        && targetEvtName === "*") || keyWithoutEnv === keyWithListener) {
                    // Iterate over all registered callbacks.
                    this._consumerCallbacks[keyWithListener].forEach((callback) => {
                        callback.fn(value.data, keyWithoutEnv);
                    });
                }
            });
        }
    }
    /**
     * This function emits an event to a specific topic.
     *
     * @param key - The key identifying both topic and eventName in the format `topic_1:..:topic_N:evtName`.
     * @param data - The data to be sent.
     * @param opts - Emit options.
     * @param ctx - The log context.
     */
    emit(key, data, opts = {}, { logger } = nodejs_logger_1.globalCtx) {
        // Try to convert data to plain object.
        const plainData = JSON.parse(JSON.stringify(data || {}));
        // Use nodejs event emitter instead of kafka when instantiated.
        if (this._emitter) {
            this._emitter.emit(key, plainData);
            // Prevent emitting to outside world.
            if (!this._kafkaHost || opts.internalOnly) {
                return;
            }
        }
        logger.debug("data", {
            data,
            key,
            producerIsReady: this._producerIsReady,
        });
        if (!this._producer) {
            logger.debug("producer not available yet");
        }
        if (!this._producerIsReady) {
            logger.debug("producer is not ready yet");
            // Producer is not ready so let"s add message to producer queue.
            this._producerQueue.push({ data, key });
            return;
        }
        // Here producer is ready, so we going to produce the message.
        const keyParts = key.split(":");
        if (keyParts.length === 1) { // eslint-disable-line no-magic-numbers
            return;
        }
        const topics = keyParts.slice(0, keyParts.length - 1); // eslint-disable-line no-magic-numbers
        const evtName = keyParts[keyParts.length - 1]; // eslint-disable-line no-magic-numbers
        const timestamp = moment_1.default().unix();
        for (let i = 0; i < topics.length; i++) { // eslint-disable-line no-magic-numbers
            const topic = this._env ? `${topics[i]}.${this._env}` : topics[i];
            const message = {
                data: plainData,
                key: `${topic}:${evtName}`,
            };
            const messageStr = JSON.stringify(message);
            const messageBuffer = Buffer.from(messageStr);
            try {
                this._producer.produce(topic, null, messageBuffer, null, timestamp);
            }
            catch (error) {
                logger.error("producer produce failed", error);
            }
        }
    }
    /**
     * This function register a callback to be called when kafka
     * consumer receive an event.
     *
     * @param key - Message key in the form topic:evtName.
     * @param callback - The callback that should be executed upon receiving the event.
     * @param opts - A set of options.
     * @param ctx - The log context.
     */
    on(key, callback, opts = {}, { logger } = nodejs_logger_1.globalCtx) {
        // Use nodejs event emitter instead of kafka when instantiated.
        if (this._emitter) {
            this._emitter.on(key, callback);
            // Prevent listening from outside world.
            if (!this._kafkaHost || opts.internalOnly) {
                return;
            }
        }
        /**
         * This is kind of silly, but we need to use consumer
         * so that typescript does not complain (fail to compile).
         */
        if (!this._consumer) {
            logger.debug("consumer not available yet");
        }
        // Register to callback map.
        this._consumerCallbacks[key] = this._consumerCallbacks[key] || [];
        this._consumerCallbacks[key].push({
            fn: callback,
            opts,
        });
    }
    /**
     * This function disconnect this client from the remote broker.
     */
    disconnect() {
        return __awaiter(this, void 0, void 0, function* () {
            const promises = [];
            if (this._consumer && !this._isConsumerDisconnected) {
                promises.push(new Promise((resolve, reject) => {
                    this._consumer.disconnect((error, data) => {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve(data);
                            this._isConsumerDisconnected = true;
                        }
                    });
                }));
            }
            if (this._producer && !this._isProducerDisconnected) {
                promises.push(new Promise((resolve, reject) => {
                    this._producer.disconnect((error, data) => {
                        if (error) {
                            reject(error);
                        }
                        else {
                            resolve(data);
                            this._isProducerDisconnected = true;
                        }
                    });
                }));
            }
            if (!promises.length) {
                return Promise.resolve();
            }
            return Promise.all(promises);
        });
    }
}
__decorate([
    nodejs_logger_1.log({
        resultToLog: false,
    })
], KafkaClient.prototype, "_setupConsumer", null);
__decorate([
    nodejs_logger_1.log({
        resultToLog: false,
    })
], KafkaClient.prototype, "_setupProducer", null);
__decorate([
    nodejs_logger_1.log({
        argsToLog: ["message|topic"],
        resultToLog: false,
    })
], KafkaClient.prototype, "consumeMessage", null);
__decorate([
    nodejs_logger_1.log({
        argsToLog: ["key", "data"],
        resultToLog: false,
    })
], KafkaClient.prototype, "emit", null);
__decorate([
    nodejs_logger_1.log({
        argsToLog: ["key"],
    })
], KafkaClient.prototype, "on", null);
__decorate([
    nodejs_logger_1.log()
], KafkaClient.prototype, "disconnect", null);
exports.KafkaClient = KafkaClient;
//# sourceMappingURL=index.js.map