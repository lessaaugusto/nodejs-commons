/// <reference types="node" />
import { LogContext } from "@nosebit/nodejs-logger";
import Kafka from "node-rdkafka";
export interface IProducerOptions {
    "batch.num.messages"?: number;
    "client.id"?: string;
    "compression.codec"?: string;
    "dr_cb"?: boolean;
    "message.send.max.retries"?: number;
    "queue.buffering.max.messages"?: number;
    "queue.buffering.max.ms"?: number;
    "retry.backoff.ms"?: number;
    "socket.keepalive.enable"?: boolean;
}
export interface IConsumerOptions {
    "group.id"?: string;
}
/**
 * Interface for producer.
 */
export interface IProducer {
    connect: () => void;
    disconnect: (callback?: (error: any, data: any) => void) => void;
    on: (message: string, callback: (data: any) => void) => void;
    produce: (topic: string, 
    /**
     * Optionally we can manually specify a partition for the message.
     * This defaults to -1 - which will use librdkafka's default
     * partitioner (consistent random for keyed messages, random
     * for unkeyed messages).
     */
    partition: number | null, messageBuffer: Buffer, key: string, 
    /**
     * You can send a timestamp here. If your broker version
     * supports it, then it will get added. Otherwise, we
     * default to 0.
     */
    timestamp: number) => void;
}
/**
 * Interface for create producer method.
 */
export declare type ICreateProducerFunc = (opts: IProducerOptions) => IProducer;
/**
 * Interface for consumer and consumer group.
 */
export interface IConsumer {
    connect: () => void;
    disconnect: (callback?: (error: any, data: any) => void) => void;
    subscribe: (topics: string[]) => IConsumer;
    consume: () => void;
    on: (message: string, callback: (data: any) => void) => void;
}
/**
 * Interface for create consumer method.
 */
export declare type ICreateConsumerFunc = (opts: IConsumerOptions) => IConsumer;
/**
 * Interface for kafka client config options.
 */
export interface IKafkaClientConfig {
    env?: string;
    producerOnly?: boolean;
    consumerOnly?: boolean;
    kafkaHost?: string;
    producerOpts?: IProducerOptions;
    consumerOpts?: IConsumerOptions;
    consumerTopics?: string | string[];
    createProducer?: ICreateProducerFunc;
    createConsumer?: ICreateConsumerFunc;
    useNodeEmitter?: boolean;
}
interface IEmitOpts {
    internalOnly?: boolean;
}
/**
 * Interface for bare minimum methods exposed by kafka client.
 */
export interface IKafkaClient {
    emit: <T = any>(key: string, data: T, opts?: IEmitOpts, ctx?: LogContext) => void;
    on: <T = any>(key: string, callback: (data: T) => any, opts?: IEmitOpts, ctx?: LogContext) => void;
}
/**
 * This function implements a kafka client. The typical
 * way to use this is to instantiate a producer only
 * instante using something like the following.
 *
 * ```bash
 * Kafka.init({
 *   producerOnly: true,
 *   kafkaHost: ...
 * });
 * ```
 *
 * or to create a producer/consumer.
 *
 * ```bash
 * Kafka.init({
 *   consumerTopics: [user],
 *   kafkaHost: ...
 * });
 * ```
 */
declare class KafkaClient implements IKafkaClient {
    /**
     * Shared instance.
     */
    static _shared: KafkaClient;
    /**
     * This static function retrieves the shared instance.
     *
     * @param config - The client config.
     */
    static sharedInit(config?: IKafkaClientConfig): KafkaClient;
    /**
     * This function gets the shared client.
     */
    static get shared(): KafkaClient;
    /**
     * Check if shared instance exists.
     */
    static sharedExists(): boolean;
    /**
     * Environment name which going to be used to scope topic names.
     */
    private _env;
    /**
     * Node emitter to be used instead of kafka. Useful when running a monolith
     * version of backend.
     */
    private _emitter;
    /**
     * The producer queue which store all producer messages while
     * producer is not ready yet.
     */
    private readonly _producerQueue;
    /**
     * Indicates whether producer is ready.
     */
    private _producerIsReady;
    /**
     * Indicates if producer is already disconnected.
     */
    private _isProducerDisconnected;
    /**
     * Indicates if consumer is already disconnected.
     */
    private _isConsumerDisconnected;
    /**
     * The zookeeper host url/ip.
     */
    private _kafkaHost;
    /**
     * The producer.
     */
    private _producer;
    /**
     * The consumer.
     */
    private _consumer;
    /**
     * Consumer callbacks.
     */
    private readonly _consumerCallbacks;
    /**
     * This is a promise that gets resolved when client is fully initialized.
     */
    private readonly _initialized;
    /**
     * This getter retrieves the initialized property.
     */
    get initialized(): Promise<void>;
    /**
     * This function creates a new instance of this class.
     *
     * @param config - The client config.
     */
    constructor(config: IKafkaClientConfig);
    /**
     * This function initializes the client.
     *
     * @param config - A set of config options.
     */
    private _init;
    /**
     * This function setups a consumer for this client.
     *
     * @param consumer - Consumer to config.
     * @param opts - The consumer config options.
     * @param topics - The topic we going to consume.
     * @param ctx - The log context.
     */
    private _setupConsumer;
    /**
     * This function setups a producer for this client.
     *
     * @param producer - The producer instance to setup.
     * @param ctx - The log context.
     */
    private _setupProducer;
    /**
     * This function consume a new incoming message.
     *
     * @param message - Incoming message.
     * @param ctx - Log context.
     */
    consumeMessage(message: Kafka.ConsumerStreamMessage, { logger }?: LogContext): void;
    /**
     * This function emits an event to a specific topic.
     *
     * @param key - The key identifying both topic and eventName in the format `topic_1:..:topic_N:evtName`.
     * @param data - The data to be sent.
     * @param opts - Emit options.
     * @param ctx - The log context.
     */
    emit<T = any>(key: string, data: T, opts?: IEmitOpts, { logger }?: LogContext): void;
    /**
     * This function register a callback to be called when kafka
     * consumer receive an event.
     *
     * @param key - Message key in the form topic:evtName.
     * @param callback - The callback that should be executed upon receiving the event.
     * @param opts - A set of options.
     * @param ctx - The log context.
     */
    on<T = any>(key: string, callback: (data: T, key?: string) => any, opts?: IEmitOpts, { logger }?: LogContext): void;
    /**
     * This function disconnect this client from the remote broker.
     */
    disconnect(): Promise<void | any[]>;
}
export { KafkaClient, };
