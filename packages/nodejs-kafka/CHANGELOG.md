# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [3.2.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@3.2.0...@nosebit/nodejs-kafka@3.2.1) (2019-11-30)

**Note:** Version bump only for package @nosebit/nodejs-kafka





# [3.2.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@3.1.1...@nosebit/nodejs-kafka@3.2.0) (2019-11-30)


### Bug Fixes

* Update yarn.lock which was making lint and test to fail on ci. ([119aad5](https://github.com/nosebit/nodejs-commons/commit/119aad539d6f808e9ff3c7eaf2484445a486d69f))


### Features

* Add volta config and replace redis by ioredis. ([24f11f2](https://github.com/nosebit/nodejs-commons/commit/24f11f2cc2ca16538c0457c5ccd3de562db74051))





## [3.1.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@3.1.0...@nosebit/nodejs-kafka@3.1.1) (2019-11-29)

**Note:** Version bump only for package @nosebit/nodejs-kafka





# [3.1.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@3.0.0...@nosebit/nodejs-kafka@3.1.0) (2019-11-28)


### Features

* Add volta config and replace redis by ioredis. ([cdc915a](https://github.com/nosebit/nodejs-commons/commit/cdc915a))





# [3.0.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@1.1.2...@nosebit/nodejs-kafka@3.0.0) (2019-11-27)


### Bug Fixes

* Upgrade @types/node version which was conflicting with typescript own internal types and prevent lib from being built. ([f18913f](https://github.com/nosebit/nodejs-commons/commit/f18913f))
* Use ~ in package.jsons instead of ^. ([d31040a](https://github.com/nosebit/nodejs-commons/commit/d31040a))


### Features

* New parsing of nosebit search queries to mongo nose queries. ([02d024f](https://github.com/nosebit/nodejs-commons/commit/02d024f))
* New way of handling shared instances. ([79cc300](https://github.com/nosebit/nodejs-commons/commit/79cc300))
* Some features required for squad27. ([4cf9cc2](https://github.com/nosebit/nodejs-commons/commit/4cf9cc2))


### BREAKING CHANGES

* Not using mutable exports anymore and removed init and shared functions to initialize shared instance.





# [2.0.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@1.2.0...@nosebit/nodejs-kafka@2.0.0) (2019-11-15)


### Features

* New way of handling shared instances. ([8b67d8f](https://github.com/nosebit/nodejs-commons/commit/8b67d8f))


### BREAKING CHANGES

* Not using mutable exports anymore and removed init and shared functions to initialize shared instance.





# [1.2.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@1.1.2...@nosebit/nodejs-kafka@1.2.0) (2019-11-10)


### Features

* New parsing of nosebit search queries to mongo nose queries. ([02d024f](https://github.com/nosebit/nodejs-commons/commit/02d024f))
* Some features required for squad27. ([4cf9cc2](https://github.com/nosebit/nodejs-commons/commit/4cf9cc2))





## [1.1.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@1.1.1...@nosebit/nodejs-kafka@1.1.2) (2019-08-21)

**Note:** Version bump only for package @nosebit/nodejs-kafka





## [1.1.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@1.1.0...@nosebit/nodejs-kafka@1.1.1) (2019-07-10)

**Note:** Version bump only for package @nosebit/nodejs-kafka





# [1.1.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@1.0.1...@nosebit/nodejs-kafka@1.1.0) (2019-07-06)


### Features

* **nodejs-kafka:** Expose method to disconnect both consumer and producer. ([1eccac7](https://github.com/nosebit/nodejs-commons/commit/1eccac7))





## [1.0.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@1.0.0...@nosebit/nodejs-kafka@1.0.1) (2019-06-22)


### Performance Improvements

* **nodejs-kafka:** Replace node-kafka by more performatic node-rdkafka that uses librdkafka under t ([40df7bc](https://github.com/nosebit/nodejs-commons/commit/40df7bc))





# [1.0.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@0.4.1...@nosebit/nodejs-kafka@1.0.0) (2019-06-20)


### Build System

* **nodejs-kafka:** Upgrade node-kafka so that we can use kafkaHost instead of zkHost. ([7e7205f](https://github.com/nosebit/nodejs-commons/commit/7e7205f))


### BREAKING CHANGES

* **nodejs-kafka:** Use of zkHost in config going to break the code now. Use kafkaHost instead.





## [0.4.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-kafka@0.4.0...@nosebit/nodejs-kafka@0.4.1) (2019-06-09)

**Note:** Version bump only for package @nosebit/nodejs-kafka





# 0.4.0 (2019-05-12)


### Features

* migrate current code from github. ([7da766d](https://github.com/nosebit/nodejs-commons/commit/7da766d))
