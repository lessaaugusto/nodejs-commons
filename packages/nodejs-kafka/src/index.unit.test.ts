//#####################################################
// Imports
//#####################################################
import {
  IConsumer,
  IKafkaClientConfig,
  IProducer,
  KafkaClient,
} from "./index";

//#####################################################
// Local variables
//#####################################################
let consumerMock: IConsumer;
let producerMock: IProducer;
let clientConfig: IKafkaClientConfig;
let kafkaClient: KafkaClient;

//#####################################################
// Utilitary methods
//#####################################################
/**
 * This function creates a new mock client and accessory mocks.
 *
 * @param args - The arguments.
 */
async function createClient(args: {
  consumerMock?: Partial<IConsumer>;
  producerMock?: Partial<IProducer>;
  config?: Partial<IKafkaClientConfig>;
} = {}) {
  const producerRegisteredCallbacks: {
    [key: string]: (data: any) => void;
  } = {};

  consumerMock = {
    connect: jest.fn(),
    consume: jest.fn(),
    disconnect: jest.fn(),
    on: jest.fn(),
    subscribe: jest.fn(),
    ...(args.consumerMock || {}), // eslint-disable-line @typescript-eslint/no-extra-parens
  };

  producerMock = {
    connect: jest.fn(),
    disconnect: jest.fn(),
    produce: jest.fn(),
    ...(args.producerMock || {}), // eslint-disable-line @typescript-eslint/no-extra-parens
    on: (eventName: string, callback: (data: any) => void) => {
      producerRegisteredCallbacks[eventName] = callback;

      if (args.producerMock && args.producerMock.on) {
        args.producerMock.on(eventName, callback);
      }
    },
  };

  clientConfig = {
    createConsumer: jest.fn(() => consumerMock),
    createProducer: jest.fn(() => producerMock),
    kafkaHost: "kafkaHostMock",
    ...(args.config || {}), // eslint-disable-line @typescript-eslint/no-extra-parens
  };

  kafkaClient = new KafkaClient(clientConfig);

  // Make producer immediatly available.
  if (producerRegisteredCallbacks.ready) {
    producerRegisteredCallbacks.ready(null);

    // Wait a little bit so that producer finished to be ready.
    await new Promise<any>((resolve) => setTimeout(resolve, 100)); // eslint-disable-line @typescript-eslint/no-magic-numbers
  }

  return kafkaClient;
}

//#####################################################
// Test definitions
//#####################################################
describe("kafka client", () => {
  it("should create producer only", async () => {
    await createClient({
      config: {
        producerOnly: true,
      },
    });

    expect(clientConfig.createProducer).toHaveBeenCalled();
    expect(clientConfig.createConsumer).not.toHaveBeenCalled();
  });

  it("should produce events to a single topic", async () => {
    await createClient({
      config: {
        producerOnly: true,
      },
    });

    const data = { test: "ok" };

    kafkaClient.emit("someTopic:myEvent", data);

    expect(producerMock.produce).toHaveBeenCalledTimes(1); // eslint-disable-line no-magic-numbers
    expect(producerMock.produce).toHaveBeenCalledWith(
      "someTopic",
      null,
      expect.any(Buffer),
      null,
      expect.any(Number),
    );
  });

  it("should produce events to multiple topics", async () => {
    await createClient({
      config: {
        producerOnly: true,
      },
    });

    const data = { test: "ok" };

    kafkaClient.emit("someTopic1:someTopic2:myEvent", data);

    expect(producerMock.produce).toHaveBeenCalledTimes(2);
    expect(producerMock.produce).toHaveBeenNthCalledWith(
      1,
      "someTopic1",
      null,
      expect.any(Buffer),
      null,
      expect.any(Number),
    );
    expect(producerMock.produce).toHaveBeenNthCalledWith(
      2,
      "someTopic2",
      null,
      expect.any(Buffer),
      null,
      expect.any(Number),
    );
  });

  it("should create consumer only", () => {
    createClient({
      config: {
        consumerOnly: true,
      },
    });

    expect(clientConfig.createProducer).not.toHaveBeenCalled();
    expect(clientConfig.createConsumer).toHaveBeenCalled();
  });

  it("should consume event comming from one topic", () => {
    const dataMock = { test: "ok" };
    const cbMock = jest.fn();
    const keyMock = "someTopic:myEvent";
    const registeredCallbacks: {
      [key: string]: (data: any) => void;
    } = {};

    createClient({
      config: {
        consumerOnly: true,
      },
      consumerMock: {
        on: (eventName: string, callback: (data: any) => void) => {
          registeredCallbacks[eventName] = callback;
        },
      },
    });

    // Register a callback.
    kafkaClient.on(keyMock, cbMock);

    // Simulate an incoming data.
    expect(registeredCallbacks.data).toBeDefined();

    registeredCallbacks.data({
      value: Buffer.from(JSON.stringify({
        data: dataMock,
        key: keyMock,
      })),
    });

    expect(cbMock).toHaveBeenCalledWith(dataMock, keyMock);
  });

  it("should not consume event with no env 1", () => {
    const dataMock = { test: "ok" };
    const cbMock = jest.fn();
    const keyMock = "someTopic:myEvent";
    const registeredCallbacks: {
      [key: string]: (data: any) => void;
    } = {};

    // Client is running in "dev" env.
    createClient({
      config: {
        consumerOnly: true,
        env: "dev",
      },
      consumerMock: {
        on: (eventName: string, callback: (data: any) => void) => {
          registeredCallbacks[eventName] = callback;
        },
      },
    });

    /**
     * Register a callback but for a topic comming from
     * 'prod' environment.
     */
    kafkaClient.on(keyMock, cbMock);

    // Simulate an incoming data.
    expect(registeredCallbacks.data).toBeDefined();

    registeredCallbacks.data({
      value: Buffer.from(JSON.stringify({
        data: dataMock,
        key: keyMock,
      })),
    });

    expect(cbMock).not.toHaveBeenCalled();
  });

  it("should not consume event with no env 2", () => {
    const dataMock = { test: "ok" };
    const cbMock = jest.fn();
    const keyMock = "someTopic.dev:myEvent";
    const registeredCallbacks: {
      [key: string]: (data: any) => void;
    } = {};

    // Client is running in "prod" env.
    createClient({
      config: {
        consumerOnly: true,
      },
      consumerMock: {
        on: (eventName: string, callback: (data: any) => void) => {
          registeredCallbacks[eventName] = callback;
        },
      },
    });

    /**
     * Register a callback but for a topic comming from
     * 'dev' environment.
     */
    kafkaClient.on(keyMock, cbMock);

    // Simulate an incoming data.
    expect(registeredCallbacks.data).toBeDefined();

    registeredCallbacks.data({
      value: Buffer.from(JSON.stringify({
        data: dataMock,
        key: keyMock,
      })),
    });

    expect(cbMock).not.toHaveBeenCalled();
  });

  it("should not consume event with different env", () => {
    const dataMock = { test: "ok" };
    const cbMock = jest.fn();
    const keyMock = "someTopic.dev:myEvent";
    const registeredCallbacks: {
      [key: string]: (data: any) => void;
    } = {};

    // Client is running in "rc" env.
    createClient({
      config: {
        consumerOnly: true,
        env: "rc",
      },
      consumerMock: {
        on: (eventName: string, callback: (data: any) => void) => {
          registeredCallbacks[eventName] = callback;
        },
      },
    });

    /**
     * Register a callback but for a topic comming from
     * 'dev' environment.
     */
    kafkaClient.on(keyMock, cbMock);

    // Simulate an incoming data.
    expect(registeredCallbacks.data).toBeDefined();

    registeredCallbacks.data({
      value: Buffer.from(JSON.stringify({
        data: dataMock,
        key: keyMock,
      })),
    });

    expect(cbMock).not.toHaveBeenCalled();
  });

  it("should consume event with no env specified", () => {
    const dataMock = { test: "ok" };
    const cbMock = jest.fn();
    const keyMock = "someTopic:myEvent";
    const registeredCallbacks: {
      [key: string]: (data: any) => void;
    } = {};

    // Client is running in "prod" env.
    createClient({
      config: {
        consumerOnly: true,
      },
      consumerMock: {
        on: (eventName: string, callback: (data: any) => void) => {
          registeredCallbacks[eventName] = callback;
        },
      },
    });

    /**
     * Register a callback but for a topic comming from
     * 'prod' environment.
     */
    kafkaClient.on(keyMock, cbMock);

    // Simulate an incoming data.
    expect(registeredCallbacks.data).toBeDefined();

    registeredCallbacks.data({
      value: Buffer.from(JSON.stringify({
        data: dataMock,
        key: keyMock,
      })),
    });

    expect(cbMock).toHaveBeenCalledWith(dataMock, keyMock);
  });

  it("should consume event with same env", () => {
    const dataMock = { test: "ok" };
    const cbMock = jest.fn();
    const keyMock = "someTopic:myEvent";
    const incomingKeyMock = "someTopic.dev:myEvent";
    const registeredCallbacks: {
      [key: string]: (data: any) => void;
    } = {};

    // Client is running in "dev" env.
    createClient({
      config: {
        consumerOnly: true,
        env: "dev",
      },
      consumerMock: {
        on: (eventName: string, callback: (data: any) => void) => {
          registeredCallbacks[eventName] = callback;
        },
      },
    });

    /**
     * Register a callback but for a topic comming from
     * 'dev' environment.
     */
    kafkaClient.on(keyMock, cbMock);

    // Simulate an incoming data.
    expect(registeredCallbacks.data).toBeDefined();

    registeredCallbacks.data({
      value: Buffer.from(JSON.stringify({
        data: dataMock,
        key: incomingKeyMock,
      })),
    });

    expect(cbMock).toHaveBeenCalledWith(dataMock, keyMock);
  });

  it("should consume generic events for specific topic", () => {
    const dataMock = { test: "ok" };
    const cbMock = jest.fn();
    const keyMock = "someTopic:*";
    const incomingKeyMock = "someTopic:myEvent";
    const registeredCallbacks: {
      [key: string]: (data: any) => void;
    } = {};

    createClient({
      config: {
        consumerOnly: true,
      },
      consumerMock: {
        on: (eventName: string, callback: (data: any) => void) => {
          registeredCallbacks[eventName] = callback;
        },
      },
    });

    kafkaClient.on(keyMock, cbMock);

    // Simulate an incoming data.
    expect(registeredCallbacks.data).toBeDefined();

    registeredCallbacks.data({
      value: Buffer.from(JSON.stringify({
        data: dataMock,
        key: incomingKeyMock,
      })),
    });

    expect(cbMock).toHaveBeenCalledWith(dataMock, incomingKeyMock);
  });
});
