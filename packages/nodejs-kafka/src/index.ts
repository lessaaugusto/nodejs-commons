/* eslint-disable import/prefer-default-export */

//#####################################################
// Imports
//#####################################################
import EventEmitter from "events";

import {
  globalCtx,
  log,
  LogContext,
} from "@nosebit/nodejs-logger";
import Kafka from "node-rdkafka";
import lodash from "lodash";
import moment from "moment";

//#####################################################
// Types
//#####################################################
export interface IProducerOptions {
  "batch.num.messages"?: number;
  "client.id"?: string;
  "compression.codec"?: string;
  "dr_cb"?: boolean;
  "message.send.max.retries"?: number;
  "queue.buffering.max.messages"?: number;
  "queue.buffering.max.ms"?: number;
  "retry.backoff.ms"?: number;
  "socket.keepalive.enable"?: boolean;
}

export interface IConsumerOptions {
  "group.id"?: string;
}

interface IConsumerCallback {
  fn: (data: any, key?: string) => void;
  opts?: IEmitOpts;
}

/**
 * Interface for producer.
 */
export interface IProducer {
  connect: () => void;
  disconnect: (callback?: (error: any, data: any) => void) => void;
  on: (message: string, callback: (data: any) => void) => void;
  produce: (

    // Topic to send the message to.
    topic: string,

    /**
     * Optionally we can manually specify a partition for the message.
     * This defaults to -1 - which will use librdkafka's default
     * partitioner (consistent random for keyed messages, random
     * for unkeyed messages).
     */
    partition: number | null,

    // Message to send.
    messageBuffer: Buffer,

    // For keyed messages, we also specify the key - note that this field is optional
    key: string,

    /**
     * You can send a timestamp here. If your broker version
     * supports it, then it will get added. Otherwise, we
     * default to 0.
     */
    timestamp: number,
  ) => void;
}

/**
 * Interface for create producer method.
 */
export type ICreateProducerFunc = (
  opts: IProducerOptions,
) => IProducer;

/**
 * Interface for consumer and consumer group.
 */
export interface IConsumer {
  connect: () => void;
  disconnect: (callback?: (error: any, data: any) => void) => void;
  subscribe: (topics: string[]) => IConsumer;
  consume: () => void;
  on: (message: string, callback: (data: any) => void) => void;
}

/**
 * Interface for create consumer method.
 */
export type ICreateConsumerFunc = (
  opts: IConsumerOptions,
) => IConsumer;

/**
 * Interface for kafka client config options.
 */
export interface IKafkaClientConfig {
  env?: string;
  producerOnly?: boolean;
  consumerOnly?: boolean;
  kafkaHost?: string;
  producerOpts?: IProducerOptions;
  consumerOpts?: IConsumerOptions;
  consumerTopics?: string | string[];
  createProducer?: ICreateProducerFunc;
  createConsumer?: ICreateConsumerFunc;
  useNodeEmitter?: boolean;
}

interface IEmitOpts {
  internalOnly?: boolean;
}

/**
 * Interface for bare minimum methods exposed by kafka client.
 */
export interface IKafkaClient {
  emit: <T = any>(
    key: string,
    data: T,
    opts?: IEmitOpts,
    ctx?: LogContext,
  ) => void;
  on: <T = any>(
    key: string,
    callback: (data: T) => any,
    opts?: IEmitOpts,
    ctx?: LogContext,
  ) => void;
}

/**
 * Interface for a deliverable producer message.
 */
interface IProducerMessage {
  key: string;
  data: any;
}

//#####################################################
// Auxiliary Functions
//#####################################################
/**
 * This function converts a struct to a plain javascript
 * object that encapsulates the original types info within
 * the struct.
 *
 * @param data - Data struct to be converted.
 */
function convertToTypedObj(data: any): any {
  if (lodash.isMap(data)) {
    const dataObj = [
      ...data.entries(),
    ].reduce((accum, [key, val]) => ({
      ...accum,
      [key]: val,
    }), {} as any);

    return {
      __nsbKakfaType: "Map",
      value: convertToTypedObj(dataObj),
    };
  } else if (lodash.isSet(data)) {
    return {
      __nsbKakfaType: "Set",
      value: [...data.keys()].map((item) => convertToTypedObj(item)),
    };
  } else if (lodash.isArray(data)) {
    return data.map((val) => convertToTypedObj(val));
  } else if (lodash.isDate(data)) {
    return {
      __nsbKakfaType: "Date",
      value: data.toString(),
    };
  } else if (moment.isMoment(data)) {
    return {
      __nsbKakfaType: "Moment",
      value: data.toISOString(),
    };
  } else if (lodash.isError(data)) {
    return {
      __nsbKakfaType: "Error",
      stack: data.stack,
      value: data.message,
    };
  } else if (lodash.isObject(data)) {
    return Object.entries(data).reduce((accum, [key, val]) => ({
      ...accum,
      [key]: convertToTypedObj(val),
    }), {});
  } else if (
    lodash.isNumber(data)
    || lodash.isString(data)
    || lodash.isBoolean(data)
    || lodash.isNull(data)
  ) {
    return data;
  }

  return {
    __nsbKakfaType: "unknown",
    value: data,
  };
}

/**
 * This function converts a plain typed object to original struct
 * with correct types.
 *
 * @param data - Typed object to be converted.
 */
function convertFromTypedObj(data: any): any {
  if (lodash.isArray(data)) {
    return data.map((val) => convertFromTypedObj(val));
  } else if (lodash.isObject(data)) {
    data = data as any; // eslint-disable-line no-param-reassign

    switch (data.__nsbKakfaType) {
      case "Map": {
        return new Map(
          Object.entries(data.value).map(([key, val]) => // eslint-disable-line dot-notation
            [key, convertFromTypedObj(val)]),
        );
      }

      case "Set": {
        return new Set(data.value.map((val: any) => // eslint-disable-line dot-notation
          convertFromTypedObj(val)));
      }

      case "Date": {
        return new Date(data.value);
      }

      case "Moment": {
        return moment(data.value);
      }

      case "Error": {
        const error = new Error(data.value);
        error.stack = data.stack;
        return error;
      }

      case "unknown": {
        return data.value;
      }

      default: {
        break;
      }
    }

    /**
     * Iterate over each key.
     */
    return Object.entries(data).reduce((accum, [key, val]) => ({
      ...accum,
      [key]: convertFromTypedObj(val),
    }), {});
  }

  return data;
}

//#####################################################
// Main class
//#####################################################
/**
 * This function implements a kafka client. The typical
 * way to use this is to instantiate a producer only
 * instante using something like the following.
 *
 * ```bash
 * Kafka.init({
 *   producerOnly: true,
 *   kafkaHost: ...
 * });
 * ```
 *
 * or to create a producer/consumer.
 *
 * ```bash
 * Kafka.init({
 *   consumerTopics: [user],
 *   kafkaHost: ...
 * });
 * ```
 */
class KafkaClient implements IKafkaClient {
  /**
   * Shared instance.
   */
  public static _shared: KafkaClient;

  /**
   * This static function retrieves the shared instance.
   *
   * @param config - The client config.
   */
  public static sharedInit(config?: IKafkaClientConfig) {
    if (!KafkaClient._shared) {
      KafkaClient._shared = new KafkaClient(config);
    }

    return KafkaClient._shared;
  }

  /**
   * This function gets the shared client.
   */
  static get shared() {
    if (!KafkaClient._shared) {
      throw new Error("kafka shared client not initialized");
    }

    return KafkaClient._shared;
  }

  /**
   * Check if shared instance exists.
   */
  public static sharedExists() {
    return Boolean(KafkaClient._shared);
  }

  /**
   * Environment name which going to be used to scope topic names.
   */
  private _env: string;

  /**
   * Node emitter to be used instead of kafka. Useful when running a monolith
   * version of backend.
   */
  private _emitter: EventEmitter;

  /**
   * The producer queue which store all producer messages while
   * producer is not ready yet.
   */
  private readonly _producerQueue: IProducerMessage[] = [];

  /**
   * Indicates whether producer is ready.
   */
  private _producerIsReady = false;

  /**
   * Indicates if producer is already disconnected.
   */
  private _isProducerDisconnected = false;

  /**
   * Indicates if consumer is already disconnected.
   */
  private _isConsumerDisconnected = false;

  /**
   * The zookeeper host url/ip.
   */
  private _kafkaHost: string;

  /**
   * The producer.
   */
  private _producer: IProducer;

  /**
   * The consumer.
   */
  private _consumer: IConsumer;

  /**
   * Consumer callbacks.
   */
  private readonly _consumerCallbacks: {
    [key: string]: IConsumerCallback[];
  } = {};

  /**
   * This is a promise that gets resolved when client is fully initialized.
   */
  private readonly _initialized: Promise<void>;

  /**
   * This getter retrieves the initialized property.
   */
  public get initialized() {
    return this._initialized;
  }

  /**
   * This function creates a new instance of this class.
   *
   * @param config - The client config.
   */
  constructor(config: IKafkaClientConfig) {
    this._initialized = this._init(config);
  }

  /**
   * This function initializes the client.
   *
   * @param config - A set of config options.
   */
  private async _init(
    config: IKafkaClientConfig,
  ) {
    if (config.useNodeEmitter) {
      this._emitter = new EventEmitter();
    }

    this._env = config.env;
    this._kafkaHost = config.kafkaHost;

    const consumerOpts = {
      ...(config.consumerOpts || {}), // eslint-disable-line @typescript-eslint/no-extra-parens
    };

    const producerOpts = {
      "batch.num.messages": 1000000,
      "client.id": "kafka",
      "compression.codec": "none",
      "dr_cb": true, // eslint-disable-line
      "message.send.max.retries": 10,
      "queue.buffering.max.messages": 100000,
      "queue.buffering.max.ms": 1000,
      "retry.backoff.ms": 200,
      "socket.keepalive.enable": true,
      ...(config.producerOpts || {}), // eslint-disable-line @typescript-eslint/no-extra-parens
    };

    // Create the consumer.
    if (this._kafkaHost && !config.producerOnly) {
      const consumer = config.createConsumer
        ? config.createConsumer(consumerOpts)
        : new Kafka.KafkaConsumer({
          ...consumerOpts,
          "metadata.broker.list": this._kafkaHost,
        }, {});

      this._consumer = await this._setupConsumer(
        consumer,
        config.consumerTopics
      );
    }

    // Create the producer
    if (this._kafkaHost && !config.consumerOnly) {
      const producer = config.createProducer
        ? config.createProducer(producerOpts)
        : new Kafka.Producer({
          ...producerOpts,
          "metadata.broker.list": this._kafkaHost,
        });

      this._producerIsReady = false;
      this._producer = await this._setupProducer(producer);
    }
  }

  /**
   * This function setups a consumer for this client.
   *
   * @param consumer - Consumer to config.
   * @param opts - The consumer config options.
   * @param topics - The topic we going to consume.
   * @param ctx - The log context.
   */
  @log({
    resultToLog: false,
  })
  private async _setupConsumer(
    consumer: IConsumer,
    topics: string | string[],
    { logger }: LogContext = globalCtx,
  ): Promise<IConsumer> {
    const self = this;

    if (!this._kafkaHost) {
      return null;
    }

    // Process topics to add env.
    let topicsWithEnv = lodash.flatten([topics]);

    if (this._env) {
      topicsWithEnv = topicsWithEnv.map((topic) => `${topic}.${this._env}`);
    }

    return new Promise((resolve) => {
      // Log when producer is ready.
      consumer.on("ready", () => {
        logger.info("consumer ready");

        // Subscribe to events
        consumer.subscribe(topicsWithEnv);

        /**
         * Consume from the topics. This is what determines
         * the mode we are running in. By not specifying a
         * callback (or specifying only a callback) we get
         * messages as soon as they are available.
         */
        consumer.consume();

        // Return consumer to the caller.
        resolve(consumer);
      });

      // Log when consumer report an error.
      consumer.on("event.error", (error) => {
        logger.error("consumer error", error);

        // @TODO : Retry a little more times upon error.
        resolve(consumer);
      });

      // Consume incoming messages coming from kafka.
      consumer.on("data", (data) => {
        logger.info("new consumer data", LogContext.secret(data));
        self.consumeMessage(data);
      });

      // Connect to the broker manually
      consumer.connect();
    });
  }

  /**
   * This function setups a producer for this client.
   *
   * @param producer - The producer instance to setup.
   * @param ctx - The log context.
   */
  @log({
    resultToLog: false,
  })
  private async _setupProducer(
    producer: IProducer,
    { logger }: LogContext = globalCtx,
  ): Promise<IProducer> {
    const self = this;

    if (!this._kafkaHost) {
      return Promise.resolve(null);
    }

    return new Promise((resolve) => {
      // Connect to the broker manually
      producer.connect();

      // Log when producer is ready.
      producer.on("ready", () => {
        logger.info("producer is ready");
        self._producerIsReady = true;

        // Send all queued messages.
        while (self._producerQueue.length > 0) { // eslint-disable-line no-magic-numbers
          const { key, data } = self._producerQueue.shift();
          this.emit(key, data);
        }

        resolve(producer);
      });

      // On producer error close connection and try again later.
      producer.on("event.error", (error) => {
        logger.error("producer error", error);
      });
    });
  }

  /**
   * This function consume a new incoming message.
   *
   * @param message - Incoming message.
   * @param ctx - Log context.
   */
  @log({
    argsToLog: ["message|topic"],
    resultToLog: false,
  })
  public consumeMessage(
    message: Kafka.ConsumerStreamMessage,
    { logger }: LogContext = globalCtx,
  ) {
    const messageValue = message.value.toString();

    let value: {
      key: string;
      data: any;
    };

    try {
      value = messageValue
        ? JSON.parse(messageValue)
        : message;
    } catch (error) {
      logger.error("could not parse message", error);

      value = {
        data: messageValue,
        key: message.topic,
      };
    }

    value.data = convertFromTypedObj(value.data);

    /**
     * Find out the key coming from remote sender (from
     * message). The key sent is something in the form:
     *
     * TOPIC.ENV:EVT_NAME.
     */
    const keyParts = value.key.split(":");
    const topicNameWithEnv = keyParts.length ? keyParts[0] : null;
    const evtName = keyParts.length > 1 ? keyParts[1] : null;
    const topicNameParts = topicNameWithEnv.split(".");
    const topicName = topicNameParts.length ? topicNameParts[0] : null;
    const env = topicNameParts.length > 1 ? topicNameParts[1] : null;
    const keyWithoutEnv = evtName
      ? `${topicName}:${evtName}`
      : topicName;

    /**
     * Prevent processing event from different env. This way
     * if the code running this kafka client is running in
     * 'prod' mode it going to ignore any events coming from
     * any other env ('dev' for example).
     */
    if ((!env && !this._env) || env === this._env) {
      /**
       * Now we going to iterate over all registered consumer
       * callbacks to verify which callback we going to call
       * (i.e., which match the message key).
       */
      const keysWithCallbacks = Object.keys(this._consumerCallbacks);

      keysWithCallbacks.forEach((keyWithListener) => {
        const targetKeyParts = keyWithListener.split(":");
        const targetTopicName = targetKeyParts.length
          ? targetKeyParts[0]
          : null;
        const targetEvtName = targetKeyParts.length > 1
          ? targetKeyParts[1]
          : null;

        /**
         * Check if keyWithListener match incoming key.
         */
        if (
          keyWithListener === "*"
          || (
            topicName === targetTopicName
            && targetEvtName === "*"
          ) || keyWithoutEnv === keyWithListener
        ) {
          // Iterate over all registered callbacks.
          this._consumerCallbacks[keyWithListener].forEach((callback) => {
            callback.fn(value.data, keyWithoutEnv);
          });
        }
      });
    }
  }

  /**
   * This function emits an event to a specific topic.
   *
   * @param key - The key identifying both topic and eventName in the format `topic_1:..:topic_N:evtName`.
   * @param data - The data to be sent.
   * @param opts - Emit options.
   * @param ctx - The log context.
   */
  @log({
    argsToLog: ["key", "data"],
    resultToLog: false,
  })
  public emit<T = any>(
    key: string,
    data: T,
    opts: IEmitOpts = {},
    { logger }: LogContext = globalCtx,
  ) {
    // Try to convert data to plain object.
    const plainData = convertToTypedObj(data);

    // Use nodejs event emitter instead of kafka when instantiated.
    if (this._emitter) {
      this._emitter.emit(key, plainData);

      // Prevent emitting to outside world.
      if (!this._kafkaHost || opts.internalOnly) {
        return;
      }
    }

    logger.debug("data", {
      data,
      key,
      producerIsReady: this._producerIsReady,
    });

    if (!this._producer) {
      logger.debug("producer not available yet");
    }

    if (!this._producerIsReady) {
      logger.debug("producer is not ready yet");

      // Producer is not ready so let"s add message to producer queue.
      this._producerQueue.push({ data, key });
      return;
    }

    // Here producer is ready, so we going to produce the message.
    const keyParts = key.split(":");

    if (keyParts.length === 1) { // eslint-disable-line no-magic-numbers
      return;
    }

    const topics = keyParts.slice(0, keyParts.length - 1); // eslint-disable-line no-magic-numbers
    const evtName = keyParts[keyParts.length - 1]; // eslint-disable-line no-magic-numbers

    const timestamp = moment().unix();

    for (let i = 0; i < topics.length; i++) { // eslint-disable-line no-magic-numbers
      const topic = this._env ? `${topics[i]}.${this._env}` : topics[i];
      const message = {
        data: plainData,
        key: `${topic}:${evtName}`,
      };

      const messageStr = JSON.stringify(message);
      const messageBuffer = Buffer.from(messageStr);

      try {
        this._producer.produce(
          topic,
          null,
          messageBuffer,
          null,
          timestamp,
        );
      } catch (error) {
        logger.error("producer produce failed", error);
      }
    }
  }

  /**
   * This function register a callback to be called when kafka
   * consumer receive an event.
   *
   * @param key - Message key in the form topic:evtName.
   * @param callback - The callback that should be executed upon receiving the event.
   * @param opts - A set of options.
   * @param ctx - The log context.
   */
  @log({
    argsToLog: ["key"],
  })
  public on<T = any>(
    key: string,
    callback: (data: T, key?: string) => any,
    opts: IEmitOpts = {},
    { logger }: LogContext = globalCtx,
  ) {
    // Use nodejs event emitter instead of kafka when instantiated.
    if (this._emitter) {
      this._emitter.on(key, (data, aKey) => {
        callback(convertFromTypedObj(data), aKey);
      });

      // Prevent listening from outside world.
      if (!this._kafkaHost || opts.internalOnly) {
        return;
      }
    }

    /**
     * This is kind of silly, but we need to use consumer
     * so that typescript does not complain (fail to compile).
     */
    if (!this._consumer) {
      logger.debug("consumer not available yet");
    }

    // Register to callback map.
    this._consumerCallbacks[key] = this._consumerCallbacks[key] || [];

    this._consumerCallbacks[key].push({
      fn: callback,
      opts,
    });
  }

  /**
   * This function disconnect this client from the remote broker.
   */
  @log()
  public async disconnect() {
    const promises = [];

    if (this._consumer && !this._isConsumerDisconnected) {
      promises.push(new Promise<any>((resolve, reject) => {
        this._consumer.disconnect((error: any, data: any) => {
          if (error) {
            reject(error);
          } else {
            resolve(data);
            this._isConsumerDisconnected = true;
          }
        });
      }));
    }

    if (this._producer && !this._isProducerDisconnected) {
      promises.push(new Promise<any>((resolve, reject) => {
        this._producer.disconnect((error: any, data: any) => {
          if (error) {
            reject(error);
          } else {
            resolve(data);
            this._isProducerDisconnected = true;
          }
        });
      }));
    }

    if (!promises.length) {
      return Promise.resolve();
    }

    return Promise.all(promises);
  }
}

//#####################################################
// Export
//#####################################################
export {
  KafkaClient,
};
