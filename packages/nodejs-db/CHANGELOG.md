# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.2.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@2.2.0...@nosebit/nodejs-db@2.2.1) (2019-11-30)

**Note:** Version bump only for package @nosebit/nodejs-db





# [2.2.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@2.1.1...@nosebit/nodejs-db@2.2.0) (2019-11-30)


### Bug Fixes

* Update yarn.lock which was making lint and test to fail on ci. ([119aad5](https://github.com/nosebit/nodejs-commons/commit/119aad539d6f808e9ff3c7eaf2484445a486d69f))


### Features

* Add volta config and replace redis by ioredis. ([24f11f2](https://github.com/nosebit/nodejs-commons/commit/24f11f2cc2ca16538c0457c5ccd3de562db74051))





## [2.1.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@2.1.0...@nosebit/nodejs-db@2.1.1) (2019-11-29)

**Note:** Version bump only for package @nosebit/nodejs-db





# [2.1.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@2.0.0...@nosebit/nodejs-db@2.1.0) (2019-11-28)


### Features

* Add volta config and replace redis by ioredis. ([cdc915a](https://github.com/nosebit/nodejs-commons/commit/cdc915a))





# [2.0.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.7.1...@nosebit/nodejs-db@2.0.0) (2019-11-27)


### Bug Fixes

* **nodejs-db:** Add some bug fixes to redis and a new namespace option ([8fa2214](https://github.com/nosebit/nodejs-commons/commit/8fa2214))
* lint ([99ceb82](https://github.com/nosebit/nodejs-commons/commit/99ceb82))
* **nodejs-db:** Check if data and data.Value exists before trying to parse it on system-config module. ([07c3966](https://github.com/nosebit/nodejs-commons/commit/07c3966))
* Fix issue with reference to nodejs-utils which breaks on prod. ([6c45199](https://github.com/nosebit/nodejs-commons/commit/6c45199))
* fix spread of cachedConfig ([009da1f](https://github.com/nosebit/nodejs-commons/commit/009da1f))
* imports ([c8c9106](https://github.com/nosebit/nodejs-commons/commit/c8c9106))
* lint ([15b0c21](https://github.com/nosebit/nodejs-commons/commit/15b0c21))
* lint ([a662efa](https://github.com/nosebit/nodejs-commons/commit/a662efa))
* lint ([954c70c](https://github.com/nosebit/nodejs-commons/commit/954c70c))
* Upgrade @types/node version which was conflicting with typescript own internal types and prevent lib from being built. ([f18913f](https://github.com/nosebit/nodejs-commons/commit/f18913f))
* Use ~ in package.jsons instead of ^. ([d31040a](https://github.com/nosebit/nodejs-commons/commit/d31040a))


### Features

* **nodejs-db:** Add a third param to parseMongoResultsForThrift to allow user to pass a function to process each result. ([ef1665d](https://github.com/nosebit/nodejs-commons/commit/ef1665d))
* add a new parser of querys to mongo ([4c5a1f3](https://github.com/nosebit/nodejs-commons/commit/4c5a1f3))
* add new query options for parsing to mongo ([f13f5b4](https://github.com/nosebit/nodejs-commons/commit/f13f5b4))
* add new query options for parsing to mongo ([e4a5773](https://github.com/nosebit/nodejs-commons/commit/e4a5773))
* add new setBatch in systemConfig ([37b282e](https://github.com/nosebit/nodejs-commons/commit/37b282e))
* add parameter that defines if searh will include deleted entities or not ([aa9f922](https://github.com/nosebit/nodejs-commons/commit/aa9f922))
* convert strings in to regular expressions ([49da72c](https://github.com/nosebit/nodejs-commons/commit/49da72c))
* New parsing of nosebit search queries to mongo nose queries. ([02d024f](https://github.com/nosebit/nodejs-commons/commit/02d024f))
* New way of handling shared instances. ([79cc300](https://github.com/nosebit/nodejs-commons/commit/79cc300))
* Some features required for squad27. ([4cf9cc2](https://github.com/nosebit/nodejs-commons/commit/4cf9cc2))


### BREAKING CHANGES

* Not using mutable exports anymore and removed init and shared functions to initialize shared instance.





# [1.0.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.12.0...@nosebit/nodejs-db@1.0.0) (2019-11-15)


### Features

* New way of handling shared instances. ([8b67d8f](https://github.com/nosebit/nodejs-commons/commit/8b67d8f))


### BREAKING CHANGES

* Not using mutable exports anymore and removed init and shared functions to initialize shared instance.





# [0.12.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.7.1...@nosebit/nodejs-db@0.12.0) (2019-11-10)


### Bug Fixes

* Fix issue with reference to nodejs-utils which breaks on prod. ([6c45199](https://github.com/nosebit/nodejs-commons/commit/6c45199))
* fix spread of cachedConfig ([009da1f](https://github.com/nosebit/nodejs-commons/commit/009da1f))
* imports ([c8c9106](https://github.com/nosebit/nodejs-commons/commit/c8c9106))
* lint ([99ceb82](https://github.com/nosebit/nodejs-commons/commit/99ceb82))
* lint ([15b0c21](https://github.com/nosebit/nodejs-commons/commit/15b0c21))
* lint ([a662efa](https://github.com/nosebit/nodejs-commons/commit/a662efa))
* lint ([954c70c](https://github.com/nosebit/nodejs-commons/commit/954c70c))
* **nodejs-db:** Add some bug fixes to redis and a new namespace option ([8fa2214](https://github.com/nosebit/nodejs-commons/commit/8fa2214))
* **nodejs-db:** Check if data and data.Value exists before trying to parse it on system-config module. ([07c3966](https://github.com/nosebit/nodejs-commons/commit/07c3966))


### Features

* **nodejs-db:** Add a third param to parseMongoResultsForThrift to allow user to pass a function to process each result. ([ef1665d](https://github.com/nosebit/nodejs-commons/commit/ef1665d))
* add a new parser of querys to mongo ([4c5a1f3](https://github.com/nosebit/nodejs-commons/commit/4c5a1f3))
* add new query options for parsing to mongo ([f13f5b4](https://github.com/nosebit/nodejs-commons/commit/f13f5b4))
* add new query options for parsing to mongo ([e4a5773](https://github.com/nosebit/nodejs-commons/commit/e4a5773))
* add new setBatch in systemConfig ([37b282e](https://github.com/nosebit/nodejs-commons/commit/37b282e))
* convert strings in to regular expressions ([49da72c](https://github.com/nosebit/nodejs-commons/commit/49da72c))
* New parsing of nosebit search queries to mongo nose queries. ([02d024f](https://github.com/nosebit/nodejs-commons/commit/02d024f))
* Some features required for squad27. ([4cf9cc2](https://github.com/nosebit/nodejs-commons/commit/4cf9cc2))





## [0.11.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.11.1...@nosebit/nodejs-db@0.11.2) (2019-10-01)


### Bug Fixes

* **nodejs-db:** Add some bug fixes to redis and a new namespace option ([f832210](https://github.com/nosebit/nodejs-commons/commit/f832210))





## [0.11.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.11.0...@nosebit/nodejs-db@0.11.1) (2019-09-23)


### Bug Fixes

* fix spread of cachedConfig ([78ae2ac](https://github.com/nosebit/nodejs-commons/commit/78ae2ac))





# [0.11.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.10.0...@nosebit/nodejs-db@0.11.0) (2019-09-21)


### Features

* add new setBatch in systemConfig ([37655f0](https://github.com/nosebit/nodejs-commons/commit/37655f0))





# [0.10.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.9.0...@nosebit/nodejs-db@0.10.0) (2019-09-17)


### Features

* convert strings in to regular expressions ([a2558c1](https://github.com/nosebit/nodejs-commons/commit/a2558c1))





# [0.9.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.8.1...@nosebit/nodejs-db@0.9.0) (2019-09-13)


### Bug Fixes

* imports ([adf8c53](https://github.com/nosebit/nodejs-commons/commit/adf8c53))
* lint ([fc8dd73](https://github.com/nosebit/nodejs-commons/commit/fc8dd73))
* lint ([c184cdc](https://github.com/nosebit/nodejs-commons/commit/c184cdc))
* lint ([1dd8395](https://github.com/nosebit/nodejs-commons/commit/1dd8395))
* lint ([81df46f](https://github.com/nosebit/nodejs-commons/commit/81df46f))


### Features

* add a new parser of querys to mongo ([8a304c4](https://github.com/nosebit/nodejs-commons/commit/8a304c4))
* add new query options for parsing to mongo ([45729c5](https://github.com/nosebit/nodejs-commons/commit/45729c5))
* add new query options for parsing to mongo ([ce57b09](https://github.com/nosebit/nodejs-commons/commit/ce57b09))





## [0.8.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.8.0...@nosebit/nodejs-db@0.8.1) (2019-09-02)


### Bug Fixes

* Fix issue with reference to nodejs-utils which breaks on prod. ([982721c](https://github.com/nosebit/nodejs-commons/commit/982721c))





# [0.8.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.7.2...@nosebit/nodejs-db@0.8.0) (2019-09-02)


### Features

* **nodejs-db:** Add a third param to parseMongoResultsForThrift to allow user to pass a function to process each result. ([01dc8cc](https://github.com/nosebit/nodejs-commons/commit/01dc8cc))





## [0.7.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.7.1...@nosebit/nodejs-db@0.7.2) (2019-08-23)


### Bug Fixes

* **nodejs-db:** Check if data and data.Value exists before trying to parse it on system-config module. ([07c3966](https://github.com/nosebit/nodejs-commons/commit/07c3966))





## [0.7.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.7.0...@nosebit/nodejs-db@0.7.1) (2019-08-23)


### Bug Fixes

* **nodejs-db:** Prevent rising error if config object coming from consul is undefined in system-config. ([344dba5](https://github.com/nosebit/nodejs-commons/commit/344dba5))





# [0.7.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.6.4...@nosebit/nodejs-db@0.7.0) (2019-08-23)


### Features

* **nodejs-db:** Add system-config to manage global shared config. ([4760bd5](https://github.com/nosebit/nodejs-commons/commit/4760bd5))





## [0.6.4](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.6.3...@nosebit/nodejs-db@0.6.4) (2019-08-21)

**Note:** Version bump only for package @nosebit/nodejs-db





## [0.6.3](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.6.2...@nosebit/nodejs-db@0.6.3) (2019-08-16)

**Note:** Version bump only for package @nosebit/nodejs-db





## [0.6.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.6.1...@nosebit/nodejs-db@0.6.2) (2019-08-16)


### Bug Fixes

* Adds try catch to get method ([0a1dbd4](https://github.com/nosebit/nodejs-commons/commit/0a1dbd4))


### Performance Improvements

* **nodejs-db/redis:** Calls JSON.stringify before inserting stuff into redis and JSON.parse after getting data and before returning the result ([6090092](https://github.com/nosebit/nodejs-commons/commit/6090092))





## [0.6.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.6.0...@nosebit/nodejs-db@0.6.1) (2019-07-24)


### Performance Improvements

* **nodejs-db:** Adds disconnect function to redis client ([14da190](https://github.com/nosebit/nodejs-commons/commit/14da190))





# [0.6.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.5.2...@nosebit/nodejs-db@0.6.0) (2019-07-15)


### Bug Fixes

* Corrects bug with redis unit test and makes the code more consistent ([4d74caf](https://github.com/nosebit/nodejs-commons/commit/4d74caf))


### Features

* **nodejs-db:** Adds redis client to commons. ([6f45d6b](https://github.com/nosebit/nodejs-commons/commit/6f45d6b))


### Performance Improvements

* Adds a way to check if the redis client has aready finished its initialization ([9ed6b27](https://github.com/nosebit/nodejs-commons/commit/9ed6b27))





## [0.5.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.5.1...@nosebit/nodejs-db@0.5.2) (2019-07-10)

**Note:** Version bump only for package @nosebit/nodejs-db





## [0.5.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.5.0...@nosebit/nodejs-db@0.5.1) (2019-07-09)


### Bug Fixes

* **nodejs-db:** Use existence of internalClient as criteria to prevent double connection/disconnection. ([03ecd1f](https://github.com/nosebit/nodejs-commons/commit/03ecd1f))





# [0.5.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.4.2...@nosebit/nodejs-db@0.5.0) (2019-07-06)


### Features

* **nodejs-db:** Expose createFindCursorMock, parseMongoResultsForThrift and parseSearchQueryToMongoQuery auxiliary functions. ([44dd8f5](https://github.com/nosebit/nodejs-commons/commit/44dd8f5))





## [0.4.2](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.4.1...@nosebit/nodejs-db@0.4.2) (2019-06-25)


### Bug Fixes

* **nodejs-db:** Add missing sort and limit fields to IMongoCursor type. ([5d5fa34](https://github.com/nosebit/nodejs-commons/commit/5d5fa34))





## [0.4.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-db@0.4.0...@nosebit/nodejs-db@0.4.1) (2019-06-09)

**Note:** Version bump only for package @nosebit/nodejs-db





# 0.4.0 (2019-05-12)


### Features

* migrate current code from github. ([7da766d](https://github.com/nosebit/nodejs-commons/commit/7da766d))
