"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Import
//#####################################################
const nodejs_logger_1 = require("@nosebit/nodejs-logger");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const moment_1 = __importDefault(require("moment"));
const redis_1 = require("./redis");
//#####################################################
// Main class
//#####################################################
/**
 * This class specify a token manager.
 */
class TokenManager {
    /**
     * Creates an instance of TokenManager.
     *
     * @param config - A set of confit options.
     */
    constructor(config = {}) {
        const WEEK_NUM_SECONDS = 24 * 60 * 60; // eslint-disable-line @typescript-eslint/no-magic-numbers
        this._expiration = config.expiration || WEEK_NUM_SECONDS;
        this._secret = config.secret || "shhhhhhh";
        this._type = config.type || "token";
        this._init(config);
    }
    /**
     * This function initializes the manager.
     *
     * @param config - A set of config options.
     * @param _ctx - The log context.
     */
    _init(config, _ctx = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            // Set JWT client
            this._jwtClient = config.jwtClient || jsonwebtoken_1.default;
            // Set redis client
            this._redisClient = config.redisClient
                || new redis_1.RedisClient(config.redisConfig);
            return this._redisClient.initialized;
        });
    }
    /**
     * This function creates a new token.
     *
     * @param data - The data to be inserted.
     * @param ctx - The log context.
     */
    create(data, ctx = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            ctx.logger.debug("data to encode", {
                data,
            });
            return new Promise((res, rej) => {
                this._jwtClient.sign(data, this._secret, {
                    expiresIn: this._expiration,
                }, (error, token) => {
                    if (error) {
                        ctx.logger.error("jwt.sign error", error);
                        return rej(error);
                    }
                    return res(token);
                });
            });
        });
    }
    /**
     * This function decodes a token to original object.
     *
     * @param token - The token to be decoded.
     * @param ctx - The log context.
     */
    decode(token, ctx = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            ctx.logger.debug("token to be decoded", token);
            const decoded = yield new Promise((resolve, reject) => {
                this._jwtClient.verify(token, this._secret, (error, jwtDecoded) => {
                    if (error) {
                        ctx.logger.error("jwt verify error", error);
                        return reject(error);
                    }
                    ctx.logger.info("jwt verify success", jwtDecoded);
                    return resolve(jwtDecoded);
                });
            });
            /**
             * Check if token is not in the black list (manually expired).
             *
             * @description : JsonWebToken sucks here because it"s not possible
             * to manually expire a token. To check if a token is already
             * expired we keep a "black list" in redis.
             */
            const key = `${this._type}_${token}`;
            try {
                const result = yield this._redisClient.get(key, ctx);
                ctx.logger.info("redis client get success", result);
            }
            catch (error) {
                ctx.logger.error("redis client get error", error);
                throw error;
            }
            return decoded;
        });
    }
    /**
     * This function expires a token (make it invalid).
     *
     * @param token - The token to be expired.
     * @param ctx - The log context.
     */
    expire(token, ctx = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield new Promise((resolve, reject) => {
                    this._jwtClient.verify(token, this._secret, (error, jwtDecoded) => {
                        if (error) {
                            return reject(error);
                        }
                        ctx.logger.info("jwt verify success", jwtDecoded);
                        return resolve(jwtDecoded);
                    });
                });
            }
            catch (error) {
                ctx.logger.error("jwt verify error", error);
                if (error.name === "TokenExpiredError") {
                    return;
                }
                throw error;
            }
            /**
             * Put token in blacklist since now it's considered invalid.
             */
            const key = `${this._type}_${token}`;
            try {
                const result = yield this._redisClient.set(key, moment_1.default().toISOString(), 0, ctx);
                ctx.logger.info("redis client set success", result);
            }
            catch (error) {
                ctx.logger.error("redis client set error", error);
                throw error;
            }
            // Remove the token from the black list after some time.
            try {
                yield this._redisClient.expire(token, this._expiration);
                ctx.logger.info("client expire success");
            }
            catch (error) {
                ctx.logger.error("redis client expire error", error);
            }
        });
    }
}
__decorate([
    nodejs_logger_1.log()
], TokenManager.prototype, "_init", null);
__decorate([
    nodejs_logger_1.log({
        argsToLog: ["data"],
    })
], TokenManager.prototype, "create", null);
__decorate([
    nodejs_logger_1.log()
], TokenManager.prototype, "decode", null);
__decorate([
    nodejs_logger_1.log()
], TokenManager.prototype, "expire", null);
exports.default = TokenManager;
//# sourceMappingURL=token.js.map