"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Imports
//#####################################################
const nodejs_logger_1 = require("@nosebit/nodejs-logger");
const bip39_1 = __importDefault(require("bip39"));
const hdkey_1 = __importDefault(require("hdkey"));
const node_vault_1 = __importDefault(require("node-vault"));
//#####################################################
// Main class
//#####################################################
/**
 * This is the main class implementing vault client.
 */
class VaultClient {
    /**
     * This function creates a new instance of this class.
     *
     * @param config - A set of config options.
     */
    constructor(config) {
        this._driver = node_vault_1.default({
            endpoint: config.url,
            token: config.token,
        });
    }
    /**
     * This function initializes a new shared instance of this class.
     *
     * @param config - A set of config options.
     */
    static sharedInit(config) {
        if (!VaultClient._shared) {
            VaultClient._shared = new VaultClient(config);
        }
        return VaultClient._shared;
    }
    /**
     * This function retreives the shared instance.
     */
    static get shared() {
        if (!VaultClient._shared) {
            throw new Error("vault shared client not initialized");
        }
        return VaultClient._shared;
    }
    /**
     * Check if shared instance exists.
     */
    static sharedExists() {
        return Boolean(VaultClient._shared);
    }
    /**
     * This function retrieves the vault driver.
     */
    get driver() {
        return this._driver;
    }
    /**
     * This function creates new seed.
     *
     * @param id - Identification string to be associated with seed.
     * @param ctx - Log context.
     */
    createSeed(id, ctx = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            // @TODO : Should we send mnemonic to user in the response?
            const mnemonic = bip39_1.default.generateMnemonic();
            ctx.logger.debug("mnemonic", nodejs_logger_1.LogContext.secret(mnemonic));
            const seed = bip39_1.default.mnemonicToSeed(mnemonic);
            ctx.logger.debug("seed", nodejs_logger_1.LogContext.secret(seed.toString("hex")));
            try {
                yield this._driver.write(`secret/${id}`, {
                    value: seed.toString("hex"),
                });
                ctx.logger.debug("seed store success");
            }
            catch (error) {
                ctx.logger.error("seed store fail", error);
                throw error;
            }
            return mnemonic;
        });
    }
    /**
     * This function derives a new key pay for a specific id.
     *
     * @param id - The identification associated with seed.
     * @param path - The hierarchical path to generate key pair.
     * @param ctx - The log context.
     */
    deriveKeyPair(id, path, ctx = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            let seed;
            /**
             * Retrieve the seed from vault.
             *
             * @todo : This is very sensitive so this should be tested
             * for all possible failures/attacks.
             */
            try {
                seed = (yield this._driver.read(`secret/${id}`));
                ctx.logger.debug("vault read success", seed);
            }
            catch (error) {
                ctx.logger.error("vault read fail", error);
                throw error;
            }
            // Regenerate the root key.
            const root = hdkey_1.default.fromMasterSeed(Buffer.from(seed));
            // Derive keyPair associated with provided path.
            const keyPair = root.derive(path);
            // Return derived key pair.
            return keyPair;
        });
    }
}
__decorate([
    nodejs_logger_1.log()
], VaultClient.prototype, "createSeed", null);
__decorate([
    nodejs_logger_1.log({
        argsToLog: ["id", "path"],
    })
], VaultClient.prototype, "deriveKeyPair", null);
exports.VaultClient = VaultClient;
//# sourceMappingURL=vault.js.map