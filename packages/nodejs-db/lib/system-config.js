"use strict";
/* eslint-disable import/prefer-default-export */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const nodejs_logger_1 = require("@nosebit/nodejs-logger");
const dns_1 = require("@nosebit/nodejs-utils/lib/dns");
const consul_1 = __importDefault(require("consul"));
const lodash_1 = __importDefault(require("lodash"));
//#####################################################
// Main class
//#####################################################
/**
 * This class implements a shared config.
 */
class SystemConfig {
    /**
     *
     * @param opts - A set of options to be passed down to redis client.
     */
    constructor(opts) {
        /**
         * Flags if cache is disabled.
         */
        this._isCacheDisabled = false;
        /**
         * Counts the number of times we called get function.
         */
        this._getCount = 0;
        this._isCacheDisabled = opts.isCacheDisabled;
        this._cacheCountdown = opts.cacheCountdown || 100; // eslint-disable-line @typescript-eslint/no-magic-numbers
        this._namespace = opts.namespace || "nosebit.global";
        this._initialized = this._init(opts);
    }
    /**
     * This function either retrieves a redis client, if it's already created, or initializes one (uses the Singleton pattern).
     *
     * @param opts - Options passed down to the redis client, used to create it with a particular configuration.
     */
    static sharedInit(opts) {
        if (!SystemConfig._shared) {
            SystemConfig._shared = new SystemConfig(opts);
        }
        return SystemConfig._shared;
    }
    /**
     * Get shared instance.
     */
    static get shared() {
        if (!SystemConfig._shared) {
            throw new Error("system config shared instance not initialized");
        }
        return SystemConfig._shared;
    }
    /**
     * Check if shared instance exists.
     */
    static sharedExists() {
        return Boolean(SystemConfig._shared);
    }
    /**
     * This function returns the initialized attribute.
     */
    get initialized() {
        return this._initialized;
    }
    /**
     * This function initializes this shared config.
     *
     * @param opts - A set of config options.
     * @param ctx - Log context.
     */
    _init(opts, { logger } = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const resolvedUrl = yield dns_1.resolveUrl(opts.consulUrl);
                logger.debug("Resolved url", resolvedUrl);
                this._consulClient = consul_1.default({
                    host: resolvedUrl.hostname,
                    port: `${resolvedUrl.port}`,
                });
            }
            catch (error) {
                logger.debug("Error trying to resolve redis url");
            }
        });
    }
    /**
     * This private function going to refresh cache if needed.
     *
     * @param opts - A list of options.
     * @param ctx - Log context.
     */
    _refreshCacheIfNeeeded(opts, { logger } = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                if ((opts && opts.forceCacheRefresh)
                    || this._isCacheDisabled
                    || !this._cachedConfig
                    || this._getCount >= this._cacheCountdown) {
                    return this._consulClient.kv.get(this._namespace, (error, data) => {
                        if (error) {
                            return reject(error);
                        }
                        let configObj = {};
                        if (data && data.Value) {
                            try {
                                configObj = JSON.parse(data.Value);
                            }
                            catch (_parseError) {
                                logger.error("could not parse config object from consul", data.Value);
                            }
                        }
                        this._cachedConfig = configObj;
                        return resolve(configObj);
                    });
                }
                return resolve(this._cachedConfig);
            });
        });
    }
    /**
     * This function get a config value by name.
     *
     * @param key - Config key.
     * @param opts - Options.
     * @param logger - Logger context.
     */
    get(key, opts, { logger } = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this._refreshCacheIfNeeeded(opts);
            this._getCount++;
            const value = lodash_1.default.get(this._cachedConfig, key);
            logger.debug("found value", { key, value });
            if (lodash_1.default.isUndefined(value)) {
                throw new Error("not found");
            }
            return value;
        });
    }
    /**
     * This function set a config value to shared config.
     *
     * @param key - Config key.
     * @param value - Config value.
     * @param logger - Logger context.
     */
    set(key, value, { logger } = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this._cachedConfig) {
                this._cachedConfig = {};
            }
            /**
             * Write config back to consul to propagate change.
             */
            yield this._refreshCacheIfNeeeded({
                forceCacheRefresh: true,
            });
            // Set new config.
            lodash_1.default.set(this._cachedConfig, key, value);
            const serializedConfig = JSON.stringify(this._cachedConfig);
            return new Promise((resolve, reject) => this._consulClient.kv.set(this._namespace, serializedConfig, (error) => {
                if (error) {
                    logger.error("consulClient set error", error);
                    reject(error);
                }
                else {
                    logger.debug("consulClient set success", this._cachedConfig);
                    resolve();
                }
            }));
        });
    }
    /**
     * This function set all config object at once.
     *
     * @param value - Config value.
     * @param logger - Logger context.
     */
    setAll(value, { logger } = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            this._cachedConfig = value;
            /**
             * Write config back to consul to propagate change.
             */
            const serializedConfig = JSON.stringify(this._cachedConfig);
            return new Promise((resolve, reject) => this._consulClient.kv.set(this._namespace, serializedConfig, (error) => {
                if (error) {
                    logger.error("consulClient set error", error);
                    reject(error);
                }
                else {
                    logger.debug("consulClient set success", this._cachedConfig);
                    resolve();
                }
            }));
        });
    }
    /**
     * This function set a config based on a object and his keys.
     *
     * @param batch - Config object.
     * @param logger - Logger context.
     */
    batchSet(batch, { logger } = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this._cachedConfig) {
                this._cachedConfig = {};
            }
            /**
             * Write config back to consul to propagate change.
             */
            yield this._refreshCacheIfNeeeded({
                forceCacheRefresh: true,
            });
            this._cachedConfig = lodash_1.default.merge({}, this._cachedConfig, batch);
            const serializedConfig = JSON.stringify(this._cachedConfig);
            return new Promise((resolve, reject) => this._consulClient.kv.set(this._namespace, serializedConfig, (error) => {
                if (error) {
                    logger.error("consulClient set error", error);
                    reject(error);
                }
                else {
                    logger.debug("consulClient set success", this._cachedConfig);
                    resolve();
                }
            }));
        });
    }
}
__decorate([
    nodejs_logger_1.log({
        argsToLog: ["config"],
        resultToLog: false,
    })
], SystemConfig.prototype, "_init", null);
__decorate([
    nodejs_logger_1.log({
        argsToLog: ["opts.{forceCacheRefresh}"],
        ctxArgIdx: 1,
        resultToLog: false,
    })
], SystemConfig.prototype, "_refreshCacheIfNeeeded", null);
__decorate([
    nodejs_logger_1.log({
        argsToLog: [],
        ctxArgIdx: 2,
        resultToLog: false,
    })
], SystemConfig.prototype, "get", null);
__decorate([
    nodejs_logger_1.log({
        argsToLog: [],
        resultToLog: false,
    })
], SystemConfig.prototype, "set", null);
__decorate([
    nodejs_logger_1.log({
        argsToLog: [],
        resultToLog: false,
    })
], SystemConfig.prototype, "setAll", null);
__decorate([
    nodejs_logger_1.log({
        argsToLog: [],
        resultToLog: false,
    })
], SystemConfig.prototype, "batchSet", null);
exports.SystemConfig = SystemConfig;
//# sourceMappingURL=system-config.js.map