/// <reference types="node" />
import { LogContext } from "@nosebit/nodejs-logger";
import vault from "node-vault";
interface IVaultClientConfig {
    url: string;
    token: string;
}
interface IVaultClient {
    init?: (config: IVaultClientConfig) => IVaultClient;
    driver?: {
        write: (path: string, data: any) => Promise<any>;
        read: (path: string) => Promise<any>;
    };
    createSeed?: (id: string) => Promise<string>;
    deriveKeyPair?: (id: string, path: string) => Promise<IKeyPair>;
}
interface IKeyPair {
    privateKey: Buffer;
    publicKey: Buffer;
}
/**
 * This is the main class implementing vault client.
 */
declare class VaultClient {
    /**
     * Shared instance.
     */
    private static _shared;
    /**
     * This function initializes a new shared instance of this class.
     *
     * @param config - A set of config options.
     */
    static sharedInit(config: IVaultClientConfig): VaultClient;
    /**
     * This function retreives the shared instance.
     */
    static get shared(): VaultClient;
    /**
     * Check if shared instance exists.
     */
    static sharedExists(): boolean;
    /**
     * This is the vault driver.
     */
    private readonly _driver;
    /**
     * This function retrieves the vault driver.
     */
    get driver(): vault.client;
    /**
     * This function creates a new instance of this class.
     *
     * @param config - A set of config options.
     */
    constructor(config: IVaultClientConfig);
    /**
     * This function creates new seed.
     *
     * @param id - Identification string to be associated with seed.
     * @param ctx - Log context.
     */
    createSeed(id: string, ctx?: LogContext): Promise<string>;
    /**
     * This function derives a new key pay for a specific id.
     *
     * @param id - The identification associated with seed.
     * @param path - The hierarchical path to generate key pair.
     * @param ctx - The log context.
     */
    deriveKeyPair(id: string, path: string, ctx?: LogContext): Promise<IKeyPair>;
}
export { IKeyPair, IVaultClient, IVaultClientConfig, VaultClient, };
