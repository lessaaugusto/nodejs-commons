"use strict";
/* eslint-disable import/prefer-default-export */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Imports
//#####################################################
const nodejs_logger_1 = require("@nosebit/nodejs-logger");
const mongo_1 = require("./mongo");
//#####################################################
// Main class
//#####################################################
/**
 * This class handles the counters collection.
 */
class Counter {
    /**
     * This function creates a new instance.
     *
     * @param config - A set of config options.
     */
    constructor(config = {}) {
        this._collection = config.collection
            || mongo_1.MongoClient.shared.db.collection("counters");
    }
    /**
     * This function initializes a shared mongo client.
     *
     * @param config - A set of config options.
     */
    static sharedInit(config) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!Counter._shared) {
                Counter._shared = new Counter();
                // Select counter right away.
                if (config.name) {
                    yield Counter._shared.select(config.name);
                }
            }
            return Counter._shared;
        });
    }
    /**
     * This function gets the shared instance.
     */
    static get shared() {
        if (!Counter._shared) {
            throw new Error("shared counter not initialized");
        }
        return Counter._shared;
    }
    /**
     * Check if shared instance exists.
     */
    static sharedExists() {
        return Boolean(Counter._shared);
    }
    /**
     * This function setup the counter collection.
     *
     * @param db - The database connector instance.
     * @param parentCtx - Log parent context.
     */
    static setupCollection(db, parentCtx = nodejs_logger_1.globalCtx) {
        return nodejs_logger_1.LogContext.trace({
            fileName: __filename,
            methodId: "setupCollection",
            parentCtx,
        }, (ctx) => __awaiter(this, void 0, void 0, function* () {
            const dbInstance = db || mongo_1.MongoClient.shared.db;
            // Try to create the collection
            try {
                yield dbInstance.createCollection("counters", {
                    readPreference: "secondaryPreferred",
                });
                ctx.logger.debug("collection create success");
            }
            catch (error) {
                ctx.logger.error("collection create error", error);
            }
        }));
    }
    /**
     * This function selects an entry in counters collection.
     *
     * @param name - The name of collection to be selected.
     * @param ctx - The log context.
     */
    select(name, ctx = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this._collection.insertOne({
                    _id: name,
                    count: 0,
                });
                ctx.logger.debug("collection insertOne success", {
                    id: result.insertedId,
                });
            }
            catch (error) {
                ctx.logger.error("collection insertOne error", error);
            }
            // Set name as selected.
            this._name = name;
        });
    }
    /**
     * This function generate next count for a specific name.
     *
     * @param name - The name of collection that we should get next count.
     * @param ctx - The log context.
     */
    getNextCount(name, ctx = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!name && !this._name) {
                throw new Error("no counter selected");
            }
            const result = yield this._collection.findOneAndUpdate({
                _id: name || this._name,
            }, { $inc: { count: 1 } }, { returnOriginal: false, upsert: true });
            ctx.logger.debug("collection findOneAndUpdate success", result);
            return result.value.count;
        });
    }
}
__decorate([
    nodejs_logger_1.log({
        argsToLog: ["name"],
    })
], Counter.prototype, "select", null);
__decorate([
    nodejs_logger_1.log({
        argsToLog: ["name"],
    })
], Counter.prototype, "getNextCount", null);
exports.Counter = Counter;
//# sourceMappingURL=counter.js.map