import { LogContext } from "@nosebit/nodejs-logger";
interface ISystemConfigOpts {
    consulUrl: string;
    isCacheDisabled?: boolean;
    cacheCountdown?: number;
    namespace: string;
}
/**
 * This class implements a shared config.
 */
declare class SystemConfig {
    /**
     * Shared instance.
     */
    private static _shared;
    /**
     * This function either retrieves a redis client, if it's already created, or initializes one (uses the Singleton pattern).
     *
     * @param opts - Options passed down to the redis client, used to create it with a particular configuration.
     */
    static sharedInit(opts: ISystemConfigOpts): SystemConfig;
    /**
     * Get shared instance.
     */
    static get shared(): SystemConfig;
    /**
     * Check if shared instance exists.
     */
    static sharedExists(): boolean;
    /**
     * Consul client to be used throughout this instance.
     */
    private _consulClient;
    /**
     * Promise indicating if everything is already initialized.
     */
    private readonly _initialized;
    /**
     * Namespace for this config.
     */
    private readonly _namespace;
    /**
     * Flags if cache is disabled.
     */
    private readonly _isCacheDisabled;
    /**
     * Counts the number of times we called get function.
     */
    private _getCount;
    /**
     * Refresh cache after this amount of get requests.
     */
    private readonly _cacheCountdown;
    /**
     * We cache the whole config locally and update it from time to
     * time.
     */
    private _cachedConfig;
    /**
     * This function returns the initialized attribute.
     */
    get initialized(): Promise<void>;
    /**
     *
     * @param opts - A set of options to be passed down to redis client.
     */
    constructor(opts: ISystemConfigOpts);
    /**
     * This function initializes this shared config.
     *
     * @param opts - A set of config options.
     * @param ctx - Log context.
     */
    private _init;
    /**
     * This private function going to refresh cache if needed.
     *
     * @param opts - A list of options.
     * @param ctx - Log context.
     */
    private _refreshCacheIfNeeeded;
    /**
     * This function get a config value by name.
     *
     * @param key - Config key.
     * @param opts - Options.
     * @param logger - Logger context.
     */
    get<T = any>(key: string, opts?: {
        forceCacheRefresh?: boolean;
    }, { logger }?: LogContext): Promise<T>;
    /**
     * This function set a config value to shared config.
     *
     * @param key - Config key.
     * @param value - Config value.
     * @param logger - Logger context.
     */
    set(key: string, value: any, { logger }?: LogContext): Promise<void>;
    /**
     * This function set all config object at once.
     *
     * @param value - Config value.
     * @param logger - Logger context.
     */
    setAll(value: any, { logger }?: LogContext): Promise<void>;
    /**
     * This function set a config based on a object and his keys.
     *
     * @param batch - Config object.
     * @param logger - Logger context.
     */
    batchSet(batch: object, { logger }?: LogContext): Promise<void>;
}
export { SystemConfig, };
