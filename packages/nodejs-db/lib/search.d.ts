/**
 * This is the interface for normalized search queries
 * (all databases). We can call this Nosebit search
 * query.
 */
export interface ISearchQuery {
    key?: string;
    valueType?: string;
    _eqStr?: string;
    _eqInt?: number;
    _eqFloat?: number;
    _eqBool?: boolean;
    _eqStrList?: string[];
    _eqIntList?: number[];
    _eqFloatList?: number[];
    _eqBoolList?: boolean[];
    _mapEqStr?: Map<string, string>;
    _mapEqInt?: Map<string, number>;
    _mapEqFloat?: Map<string, number>;
    _regex?: string;
    _gtStr?: string;
    _gteStr?: string;
    _ltStr?: string;
    _lteStr?: string;
    _gtInt?: number;
    _gteInt?: number;
    _ltInt?: number;
    _lteInt?: number;
    _gtFloat?: number;
    _gteFloat?: number;
    _ltFloat?: number;
    _lteFloat?: number;
    _and?: ISearchQuery[];
    _or?: ISearchQuery[];
    _not?: ISearchQuery;
    _elemMatch?: ISearchQuery;
}
/**
 * This is the common interface for params all search methods
 * going to receive (all databases).
 */
export interface ISearchParams {
    query: ISearchQuery;
    sort?: string[];
    limit?: number;
    includeDeleted?: boolean;
}
