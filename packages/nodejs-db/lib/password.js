"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Imports
//#####################################################
const nodejs_logger_1 = require("@nosebit/nodejs-logger");
const bcryptjs_1 = __importDefault(require("bcryptjs"));
//#####################################################
// Constants
//#####################################################
const SALT_WORK_FACTOR = 10;
//#####################################################
// Methods
//#####################################################
/**
 * This function compares a raw password and a hashed password to
 * see if both matches.
 *
 * @param args - The arguments object.
 * @param args.password - The raw password as entered by the user.
 * @param args.hashedPassword - The hashed password as stored in db.
 * @param traceInfo - The trace info.
 */
function comparePassword(args, traceInfo) {
    return __awaiter(this, void 0, void 0, function* () {
        const logger = new nodejs_logger_1.Logger("comparePasswords", { traceInfo });
        logger.info("args", {
            hashedPassword: nodejs_logger_1.LogContext.secret(args.hashedPassword),
            password: nodejs_logger_1.LogContext.secret(args.password),
        });
        return new Promise((resolve, reject) => {
            bcryptjs_1.default.compare(args.password, args.hashedPassword, (error, match) => {
                if (error) {
                    logger.error("bcrypt compare error", error);
                    return reject(error);
                }
                logger.info("bcrypt compare success", { match });
                return resolve(match);
            });
        });
    });
}
exports.comparePassword = comparePassword;
/**
 * This function hashes a password.
 *
 * @param value - The value to be hashed.
 * @param traceInfo - The trace info.
 */
function hashPassword(value = "", traceInfo) {
    return __awaiter(this, void 0, void 0, function* () {
        const logger = new nodejs_logger_1.Logger("hashPassword", { traceInfo });
        // Local variables
        logger.info("args", {
            value: nodejs_logger_1.LogContext.secret(value),
        });
        return new Promise((resolve, reject) => {
            // We use a bcrypt algorithm (see http://en.wikipedia.org/wiki/Bcrypt).
            bcryptjs_1.default.genSalt(SALT_WORK_FACTOR, (error, salt) => {
                if (error) {
                    logger.error("bcrypt genSalt error", error);
                    return reject(error);
                }
                logger.info("bcrypt genSalt success", salt);
                // Hash with generated salt.
                return bcryptjs_1.default.hash(value, salt, (error1, hash) => {
                    if (error1) {
                        logger.error("bcrypt hash error", error1);
                        return reject(error1);
                    }
                    logger.info("bcrypt hash success", hash);
                    return resolve(hash);
                });
            });
        });
    });
}
exports.hashPassword = hashPassword;
//# sourceMappingURL=password.js.map