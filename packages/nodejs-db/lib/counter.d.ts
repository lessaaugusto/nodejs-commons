import { LogContext } from "@nosebit/nodejs-logger";
import { FilterQuery, UpdateQuery, FindOneAndUpdateOption } from "mongodb";
import { Db } from "./mongo";
/**
 * Counter data that is stored in db.
 */
interface ICounter {
    _id: string;
    count: number;
}
/**
 * Interface for the insert result.
 */
interface IDbInsertResult {
    insertedId: string | {
        toString: () => string;
    };
}
/**
 * Interface for the insert result.
 */
interface IDbFindOneAndUpdateResult {
    value?: ICounter;
}
/**
 * Interface for mongodb collection driver.
 */
interface ICounterCollection {
    insertOne: (counter: ICounter) => Promise<IDbInsertResult>;
    findOneAndUpdate: (filter: FilterQuery<ICounter>, update: UpdateQuery<ICounter>, option: FindOneAndUpdateOption) => Promise<IDbFindOneAndUpdateResult>;
}
/**
 * Interface for counter config options.
 */
interface ICounterConfig {
    mode?: string;
    collection?: ICounterCollection;
    name?: string;
}
/**
 * This class handles the counters collection.
 */
declare class Counter {
    /**
     * Shared instance.
     */
    private static _shared;
    /**
     * This function initializes a shared mongo client.
     *
     * @param config - A set of config options.
     */
    static sharedInit(config: ICounterConfig): Promise<Counter>;
    /**
     * This function gets the shared instance.
     */
    static get shared(): Counter;
    /**
     * Check if shared instance exists.
     */
    static sharedExists(): boolean;
    /**
     * This function setup the counter collection.
     *
     * @param db - The database connector instance.
     * @param parentCtx - Log parent context.
     */
    static setupCollection(db?: Partial<Db>, parentCtx?: LogContext): any;
    /**
     * The selected name for this counter.
     */
    private _name;
    /**
     * The collection.
     */
    private readonly _collection;
    /**
     * This function creates a new instance.
     *
     * @param config - A set of config options.
     */
    constructor(config?: ICounterConfig);
    /**
     * This function selects an entry in counters collection.
     *
     * @param name - The name of collection to be selected.
     * @param ctx - The log context.
     */
    select(name: string, ctx?: LogContext): Promise<void>;
    /**
     * This function generate next count for a specific name.
     *
     * @param name - The name of collection that we should get next count.
     * @param ctx - The log context.
     */
    getNextCount(name?: string, ctx?: LogContext): Promise<number>;
}
export { Counter, };
