import { LogContext } from "@nosebit/nodejs-logger";
import { Cursor, Db, MongoClientOptions, ObjectId } from "mongodb";
interface IMongoClient {
    db: (name: string) => Partial<Db>;
    close: () => Promise<void>;
}
/**
 * @todo : Maybe a better name for this is IMongoConnector.
 */
interface IMongoDriver {
    connect: (url: string, opts?: MongoClientOptions) => Promise<IMongoClient>;
}
interface IMongoClientConfig {
    driver?: IMongoDriver;
}
export interface IMongoConnectArgs {
    dbName?: string;
    url: string;
}
export interface IMongoCursor<T> {
    limit: (value: number) => IMongoCursor<T>;
    sort: (opts: string | object | object[], direction?: number) => IMongoCursor<T>;
    toArray: () => Promise<T[]>;
}
interface IStringMap {
    [key: string]: any;
}
declare type INewable<TTypeA, TTypeB> = new (d: TTypeA) => TTypeB;
/**
 * This is a base mock for find cursor.
 *
 * @param overrides - A set of cursor properties to be ovewritten.
 */
declare function createFindCursorMock<T>(overrides: Partial<IMongoCursor<T>>): IMongoCursor<T>;
/**
 * This function parse dates and other data that comes from
 * mongo and that we wish to pass to thrift (which allow only
 * primitive data types).
 *
 * @param results - Data to be parsed.
 * @param DataClass - A class which should be applied to each
 * result.
 * @param mapFn - A function to process items.
 */
declare function parseMongoResultsForThrift<TTypeA extends IStringMap, TTypeB>(results: TTypeA[], DataClass: INewable<TTypeA, TTypeB>, mapFn?: (item: TTypeA) => TTypeA): TTypeB[];
/**
 * This function convert a search query to a mongo
 * query so we can perform queries in mongo.
 *
 * @deprecated - Use new search module instead.
 *
 * @param searchParams - The search params to be parsed.
 * @param filterStrategyMap - An object mapping a strategy
 * key (like 'in', 'range', etc) to a list of searchParams
 * keys.
 * @param _ctx - A log context.
 */
declare function parseSearchQueryToMongoQuery(searchParams: {
    [key: string]: any;
}, filterStrategyMap?: {
    [key: string]: string[];
}, _ctx?: LogContext): any;
/**
 * This class implements a basic wrapper around mongo driver.
 */
declare class MongoClient {
    /**
     * The shared instance of this mongo client.
     */
    private static _shared;
    /**
     * This function initializes a shared instance of
     * mongoClient.
     */
    static sharedInit(): MongoClient;
    /**
     * This function gets the shared instance.
     */
    static get shared(): MongoClient;
    /**
     * Check if shared instance exists.
     */
    static sharedExists(): boolean;
    /**
     * This function generates a name id based on a specific name.
     *
     * @param args - The list of arguments.
     * @param args.nameValue - The base name.
     * @param args.nameKey - The document key where name is stored.
     * @param args.collection - A collection that should drive the find process.
     * @param args.extraQueryParams - An extra query params to drive the find process.
     * @param args.type - Name id type we should generate.
     * @param parentCtx - The logger context.
     */
    static generateNameId(args: {
        nameValue: string;
        nameKey: string;
        collection: {
            find: (query: any, opts: any) => IMongoCursor<any>;
        };
        extraQueryParams?: object;
        type?: string;
        forceSuffix?: boolean;
    }, parentCtx?: LogContext): any;
    /**
     * The main mongo driver.
     */
    private readonly _driver;
    /**
     * The internal mongo client.
     */
    private _internalClient;
    /**
     * This represent a connection to a specific database.
     */
    private _db;
    /**
     * Public getter for dabatase connection representation.
     */
    get db(): Partial<Db>;
    /**
     * This function creates a new instance.
     *
     * @param config - A set of config options.
     */
    constructor(config?: IMongoClientConfig);
    /**
     * This function stabilish a connection with a remoto mongo instance.
     *
     * @param args - Connect arguments.
     * @param args.host - The mongo instance base host url.
     * @param args.dbName - Database name to connect to.
     * @param args.port - Database port to connect to.
     * @param ctx - The logger context.
     */
    connect(args: IMongoConnectArgs, ctx?: LogContext): Promise<Partial<Db>>;
    /**
     * This function selects a database.
     *
     * @param dbName - Database name to be selected.
     */
    selectDb(dbName: string): Db;
    /**
     * This function disconnect from remote db.
     *
     * @param ctx - The logger context.
     */
    disconnect(ctx?: LogContext): Promise<void>;
}
export * from "./search";
export { createFindCursorMock, Cursor, Db, MongoClient, ObjectId, parseMongoResultsForThrift, parseSearchQueryToMongoQuery, };
