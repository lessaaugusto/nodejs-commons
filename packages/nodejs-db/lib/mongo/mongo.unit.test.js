"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
require("jest");
const _1 = require(".");
//#####################################################
// Test definitions
//#####################################################
describe("mongo client test", () => {
    it("should call driver connect", () => __awaiter(void 0, void 0, void 0, function* () {
        const dbNameMock = "DbMock";
        // Mock mongo driver connect function
        const clientMock = {
            close: jest.fn(),
            db: jest.fn(),
        };
        const driverMock = {
            connect: jest.fn(() => __awaiter(void 0, void 0, void 0, function* () {
                return Promise.resolve(clientMock);
            })),
        };
        // Create the client
        const mongoClient = new _1.MongoClient({
            driver: driverMock,
        });
        const url = "mongodb://localhost:27017";
        yield mongoClient.connect({ dbName: dbNameMock, url });
        expect(driverMock.connect).toHaveBeenCalledWith(url, expect.any(Object));
        expect(clientMock.db).toHaveBeenCalledWith(dbNameMock);
    }));
});
//# sourceMappingURL=mongo.unit.test.js.map