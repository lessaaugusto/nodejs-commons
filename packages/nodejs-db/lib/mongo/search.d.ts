import { LogContext } from "@nosebit/nodejs-logger";
import { ISearchParams, ISearchQuery } from "../search";
interface IMongoSearchParams {
    query: {
        [key: string]: any;
    };
    sort?: {
        [key: string]: number;
    };
    limit?: number;
}
/**
 * This function convert a nosebit search query to a mongo
 * query so we can perform queries in mongo.
 *
 * @param query - The nosebit search query.
 * @param ctx - A log context.
 */
declare function parseSearchQuery(query: ISearchQuery, ctx?: LogContext): {
    [key: string]: any;
};
/**
 * This function convert a search params to a mongo
 * query so we can perform queries in mongo.
 *
 * @param params - The search params to be parsed.
 * @param ctx - A log context.
 */
declare function parseSearchParams(params: ISearchParams, ctx?: LogContext): IMongoSearchParams;
export { parseSearchQuery, parseSearchParams, };
