"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Imports
//#####################################################
const nodejs_logger_1 = require("@nosebit/nodejs-logger");
const dns_1 = require("@nosebit/nodejs-utils/lib/dns");
const lodash_1 = __importDefault(require("lodash"));
const mongodb_1 = require("mongodb");
exports.Cursor = mongodb_1.Cursor;
exports.Db = mongodb_1.Db;
exports.ObjectId = mongodb_1.ObjectId;
const moment_1 = __importDefault(require("moment"));
//#####################################################
// Auxiliary functions
//#####################################################
/**
 * This is a base mock for find cursor.
 *
 * @param overrides - A set of cursor properties to be ovewritten.
 */
function createFindCursorMock(overrides) {
    return Object.assign({ 
        /**
         * This function returns a cursor after limit.
         *
         * @param _value - The limit value.
         */
        limit(_value) {
            return this;
        },
        /**
         * This function returns a cursor after sort.
         *
         * @param _opts - The sort opts.
         */
        sort(_opts) {
            return this;
        },
        /**
         * This function returns a mock toArray.
         */
        toArray() {
            return __awaiter(this, void 0, void 0, function* () {
                return Promise.resolve([]);
            });
        } }, overrides);
}
exports.createFindCursorMock = createFindCursorMock;
/**
 * This function parse dates and other data that comes from
 * mongo and that we wish to pass to thrift (which allow only
 * primitive data types).
 *
 * @param results - Data to be parsed.
 * @param DataClass - A class which should be applied to each
 * result.
 * @param mapFn - A function to process items.
 */
function parseMongoResultsForThrift(results, DataClass, mapFn) {
    const parsedResults = [];
    lodash_1.default.forEach(results, (result) => {
        let resultClone = lodash_1.default.cloneDeep(result);
        lodash_1.default.forEach(resultClone, (value, key) => {
            if (lodash_1.default.isDate(value)) {
                lodash_1.default.set(resultClone, key, moment_1.default(value).toISOString());
            }
            else if (value instanceof mongodb_1.ObjectId) {
                lodash_1.default.set(resultClone, key, lodash_1.default.toString(value));
            }
        });
        if (mapFn) {
            resultClone = mapFn(resultClone);
        }
        parsedResults.push(new DataClass(resultClone));
    });
    return parsedResults;
}
exports.parseMongoResultsForThrift = parseMongoResultsForThrift;
/**
 * This function convert a search query to a mongo
 * query so we can perform queries in mongo.
 *
 * @deprecated - Use new search module instead.
 *
 * @param searchParams - The search params to be parsed.
 * @param filterStrategyMap - An object mapping a strategy
 * key (like 'in', 'range', etc) to a list of searchParams
 * keys.
 * @param _ctx - A log context.
 */
function parseSearchQueryToMongoQuery(searchParams, filterStrategyMap = {}, _ctx = nodejs_logger_1.globalCtx) {
    const mongoQuery = {};
    const keys = Object.keys(searchParams);
    for (const key of keys) {
        const value = searchParams[key]; // eslint-disable-line no-extra-parens
        if (value === null) {
            continue;
        }
        /**
         * Process "$in" strategy.
         */
        if (filterStrategyMap.$eq
            && filterStrategyMap.$eq.includes(key)) {
            mongoQuery[key] = value;
        }
        else if (filterStrategyMap.$regex
            && filterStrategyMap.$regex.includes(key)) {
            /**
             * Mongo db supports both syntaxes for regex match:
             * • key: { $regex: value }
             * • key: value (where value = /pattern/options).
             *
             * We going to use the second form here and therefore
             * we need to ensure value is in form /pattern/options.
             */
            mongoQuery[key] = value;
        }
        else if (filterStrategyMap.$in
            && filterStrategyMap.$in.includes(key)) {
            mongoQuery[key] = { $in: value };
        }
        else if (filterStrategyMap.$range
            && filterStrategyMap.$range.includes(key)) {
            const compKeys = Object.keys(value);
            const compQuery = {};
            for (const compKey of compKeys) {
                const compValue = value[compKey];
                if (compKey === null) {
                    continue;
                }
                compQuery[`$${compKey}`] = compValue;
            }
            if (lodash_1.default.size(compQuery) > 0) {
                mongoQuery[key] = compQuery;
            }
        }
        else if (key === "includeDeleted" && value) {
            mongoQuery.deletedAt = {
                $type: "null",
            };
        }
    }
    return mongoQuery;
}
exports.parseSearchQueryToMongoQuery = parseSearchQueryToMongoQuery;
//#####################################################
// Main class
//#####################################################
/**
 * This class implements a basic wrapper around mongo driver.
 */
class MongoClient {
    /**
     * This function creates a new instance.
     *
     * @param config - A set of config options.
     */
    constructor(config = {}) {
        this._driver = config.driver || mongodb_1.MongoClient;
    }
    /**
     * This function initializes a shared instance of
     * mongoClient.
     */
    static sharedInit() {
        if (!MongoClient._shared) {
            MongoClient._shared = new MongoClient();
        }
        return MongoClient._shared;
    }
    /**
     * This function gets the shared instance.
     */
    static get shared() {
        if (!MongoClient._shared) {
            throw new Error("mongo shared client not initialized");
        }
        return MongoClient._shared;
    }
    /**
     * Check if shared instance exists.
     */
    static sharedExists() {
        return Boolean(MongoClient._shared);
    }
    /**
     * This function generates a name id based on a specific name.
     *
     * @param args - The list of arguments.
     * @param args.nameValue - The base name.
     * @param args.nameKey - The document key where name is stored.
     * @param args.collection - A collection that should drive the find process.
     * @param args.extraQueryParams - An extra query params to drive the find process.
     * @param args.type - Name id type we should generate.
     * @param parentCtx - The logger context.
     */
    static generateNameId(args, parentCtx) {
        return nodejs_logger_1.LogContext.trace({
            fileName: __filename,
            methodId: "generateNameId",
            parentCtx,
        }, ({ logger }) => __awaiter(this, void 0, void 0, function* () {
            let records;
            const typeParts = args.type ? args.type.split(":") : ["name"];
            const [type, ...typeOpts] = typeParts;
            let fullNameValue = lodash_1.default.kebabCase(args.nameValue);
            let nameValue = fullNameValue;
            if (type === "initials") {
                const size = typeOpts.length
                    ? parseInt(typeOpts[0], 10)
                    : 3;
                fullNameValue = args.nameValue.replace(" ", "").toUpperCase();
                nameValue = fullNameValue.substr(0, size);
            }
            const query = Object.assign(Object.assign({}, args.extraQueryParams), { [args.nameKey]: new RegExp(nameValue, "gi") });
            try {
                records = yield args.collection.find(query, { [args.nameKey]: 1 }).toArray();
            }
            catch (error) {
                logger.error("collection find error", error);
                throw error;
            }
            // Iterate over results to get available nameId.
            let count = 0;
            const unavailableNames = lodash_1.default.reduce(records, (map, record) => {
                map[record[args.nameKey]] = 1;
                return map;
            }, {});
            logger.debug("unavailableNames", unavailableNames);
            const MAX_TRIES_COUNT = 10000000;
            while (count < MAX_TRIES_COUNT) {
                let inc = args.forceSuffix
                    ? `-${count + 1}`
                    : count > 0
                        ? `-${count}`
                        : "";
                if (type === "initials") {
                    const incStr = fullNameValue.substr(nameValue.length, count);
                    inc = nameValue.length + count > fullNameValue.length
                        ? `${incStr}${count - nameValue.length}`
                        : incStr;
                }
                const tryNameValue = `${nameValue}${inc}`;
                logger.debug(`check nameValue = ${tryNameValue}`);
                if (!unavailableNames[tryNameValue]) {
                    logger.debug(`nameValue "${tryNameValue}" is available`);
                    return tryNameValue;
                }
                count++;
            }
            throw new Error("could not generate name");
        }));
    }
    /**
     * Public getter for dabatase connection representation.
     */
    get db() {
        return this._db;
    }
    /**
     * This function stabilish a connection with a remoto mongo instance.
     *
     * @param args - Connect arguments.
     * @param args.host - The mongo instance base host url.
     * @param args.dbName - Database name to connect to.
     * @param args.port - Database port to connect to.
     * @param ctx - The logger context.
     */
    connect(args, ctx = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            // Prevent double connection.
            if (this._internalClient) {
                return this._db;
            }
            let urlParts = dns_1.getUrlParts(args.url);
            if (urlParts.hostname && !urlParts.port) {
                try {
                    urlParts = yield dns_1.resolveUrl(urlParts.hostname);
                    ctx.logger.debug("url resolved", urlParts);
                }
                catch (error) {
                    ctx.logger.error("could not resolve url", error);
                }
            }
            const fullUrl = `mongodb://${urlParts.hostname}:${urlParts.port}`;
            ctx.logger.debug("fullUrl", fullUrl);
            try {
                this._internalClient = yield this._driver.connect(fullUrl, { useNewUrlParser: true });
                // Select a default db after connect.
                if (args.dbName) {
                    this._db = this.selectDb(args.dbName);
                }
            }
            catch (error) {
                ctx.logger.error("mongo driver connect error", error);
                throw error;
            }
            return this._db;
        });
    }
    /**
     * This function selects a database.
     *
     * @param dbName - Database name to be selected.
     */
    selectDb(dbName) {
        if (!this._internalClient) {
            throw new Error("Client not connected");
        }
        return this._internalClient.db(dbName);
    }
    /**
     * This function disconnect from remote db.
     *
     * @param ctx - The logger context.
     */
    disconnect(ctx = nodejs_logger_1.globalCtx) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this._internalClient) {
                try {
                    yield this._internalClient.close();
                    this._internalClient = null;
                }
                catch (error) {
                    ctx.logger.error("mongo driver close error", error);
                    throw error;
                }
            }
        });
    }
}
__decorate([
    nodejs_logger_1.log({
        argsToLog: ["args"],
        resultToLog: false,
    })
], MongoClient.prototype, "connect", null);
__decorate([
    nodejs_logger_1.log()
], MongoClient.prototype, "disconnect", null);
exports.MongoClient = MongoClient;
//#####################################################
// Export
//#####################################################
__export(require("./search"));
//# sourceMappingURL=index.js.map