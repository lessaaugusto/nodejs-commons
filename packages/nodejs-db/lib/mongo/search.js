"use strict";
/* eslint-disable import/prefer-default-export */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const nodejs_logger_1 = require("@nosebit/nodejs-logger");
const lodash_1 = __importDefault(require("lodash"));
const mongodb_1 = require("mongodb");
const moment_1 = __importDefault(require("moment"));
/**
 * This function parse a primitive type to a more complex type
 * based on type user informed.
 *
 * @param value - The primitive value.
 * @param type - The type we wish to convert the primitive value to.
 */
function parseValue(value, type) {
    switch (type) {
        case "id": {
            return new mongodb_1.ObjectId(value);
        }
        case "idList": {
            return value.map((aValue) => new mongodb_1.ObjectId(aValue));
        }
        case "date": {
            return moment_1.default(value).toDate();
        }
        default: {
            break;
        }
    }
    return value;
}
/**
 * This function convert a nosebit search query to a mongo
 * query so we can perform queries in mongo.
 *
 * @param query - The nosebit search query.
 * @param ctx - A log context.
 */
function parseSearchQuery(query, ctx = nodejs_logger_1.globalCtx) {
    const searchKey = query.key;
    const { valueType } = query;
    const mongoQuery = {};
    if (query._and) {
        mongoQuery.$and = query._and.map((nestedQuery) => parseSearchQuery(nestedQuery, ctx));
    }
    else if (query._or) {
        mongoQuery.$or = query._or.map((nestedQuery) => parseSearchQuery(nestedQuery, ctx));
    }
    else if (query._not) {
        mongoQuery.$not = parseSearchQuery(query._not, ctx);
    }
    else if (query._elemMatch) {
        mongoQuery[searchKey] = {
            $elemMatch: parseSearchQuery(query._elemMatch, ctx),
        };
    }
    else {
        const keys = Object.keys(query);
        for (const key of keys) {
            const value = lodash_1.default.get(query, key);
            if (key.startsWith("_map")) {
                value.forEach((mapVal, mapKey) => {
                    mongoQuery[mapKey] = parseValue(mapVal, valueType);
                });
            }
            else if (key.startsWith("_eq")) {
                mongoQuery[searchKey] = parseValue(value, valueType);
            }
            else if ((/^_gt|_lt/).test(key)) {
                const opId = key.replace(/_|Str|Int|Float/g, "");
                lodash_1.default.set(mongoQuery, `${searchKey}.$${opId}`, parseValue(value, valueType));
            }
            else if ((/^_regex$/).test(key)) {
                const match = value.match(/\/([^/]+)\/([^/]+)/);
                if (match) {
                    mongoQuery[searchKey] = {
                        $options: lodash_1.default.get(match, "2"),
                        $regex: match[1],
                    };
                }
            }
            else if (key.startsWith("_in")) {
                mongoQuery[searchKey] = {
                    $in: parseValue(value, valueType),
                };
            }
            else if (key.startsWith("_all")) {
                mongoQuery[searchKey] = {
                    $all: parseValue(value, valueType),
                };
            }
        }
    }
    return mongoQuery;
}
exports.parseSearchQuery = parseSearchQuery;
/**
 * This function convert a search params to a mongo
 * query so we can perform queries in mongo.
 *
 * @param params - The search params to be parsed.
 * @param ctx - A log context.
 */
function parseSearchParams(params, ctx = nodejs_logger_1.globalCtx) {
    const { query, sort } = params;
    const result = {
        limit: params.limit,
        query: parseSearchQuery(query),
    };
    if (!params.includeDeleted) {
        result.query = Object.assign(Object.assign({}, result.query), { deletedAt: { $type: "null" } });
    }
    if (sort) {
        result.sort = sort.reduce((accum, item) => {
            const parts = item.split(":");
            let order = 1;
            if (parts.length > 1) {
                try {
                    order = parseInt(parts[1], 10);
                }
                catch (error) {
                    ctx.logger.error("could not parse order to int", error);
                }
            }
            accum[parts[0]] = order;
            return accum;
        }, {});
    }
    return result;
}
exports.parseSearchParams = parseSearchParams;
//# sourceMappingURL=search.js.map