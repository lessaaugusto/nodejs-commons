/// <reference types="node" />
import { LogContext } from "@nosebit/nodejs-logger";
declare type KeyType = string | Buffer;
/**
 * Interface for redis driver which going to be used
 * under the hood to communicate with redis.
 */
interface IRedisDriver {
    get(key: KeyType, callback: (error: Error, res: string | null) => void): void;
    expire(key: KeyType, seconds: number, callback: (error: Error, res: 0 | 1) => void): void;
    set(key: KeyType, value: any, callback: (error: Error, res: string) => void): void;
    set(key: KeyType, value: any, expiryMode: string, time: number | string, callback: (error: Error, res: string) => void): void;
    quit(callback: (error: Error, res: string) => void): void;
}
/**
 * Config we can pass to our own redis client.
 */
interface IClientConfig {
    _redisDriver?: IRedisDriver;
    url?: string;
    host?: string;
    port?: number;
    index?: number;
    namespace?: string;
}
/**
 * This class implements a basic wrapper around mongo driver.
 */
declare class RedisClient {
    /**
     * Shared instance.
     */
    private static _shared;
    /**
     * Store created redis clients statically so we can reuse them.
     */
    private static readonly _drivers;
    /**
     * This function either retrieves a redis client, if it's already created, or initializes one (uses the Singleton pattern).
     *
     * @param config - Options passed down to the redis client, used to create it with a particular configuration.
     */
    static sharedInit(config: IClientConfig): RedisClient;
    /**
     * Get shared driver.
     */
    static get shared(): RedisClient;
    /**
     * Check if shared instance exists.
     */
    static sharedExists(): boolean;
    /**
     * Private driver.
     */
    private _driver;
    /**
     * This function returns the instance of the client.
     */
    get driver(): IRedisDriver;
    /**
     * The feature which this client is related with.
     */
    private _namespace;
    /**
     * Value that tells whether or not this class finished its initialization.
     */
    private readonly _initialized;
    /**
     * This function returns the initialized attribute.
     */
    get initialized(): Promise<void>;
    /**
     *
     * @param config - A set of config options to be passed down to redis client.
     */
    constructor(config: IClientConfig);
    /**
     * This method puts the namespace prefix before the key and returns it.
     *
     * @param key - The key to be inserted.
     */
    private _getNamespacedKey;
    /**
     *
     * @param config - A set of config options.
     */
    init(config: IClientConfig): Promise<void>;
    /**
     *
     * @param config - A set of config options.
     * @param logger - Logger context.
     */
    createDriver(config: IClientConfig, { logger }?: LogContext): Promise<IRedisDriver>;
    /**
     *
     * @param key - Key to be inserted into redis.
     * @param value  - Value to be inserted with the key into redis.
     * @param expTimeInSec - Expiration time to be set.
     * @param logger - The log context.
     */
    set(key: string, value: any, expTimeInSec?: number, { logger }?: LogContext): Promise<string>;
    /**
     *
     * @param key - Key to get the value from.
     * @param logger - The log context.
     */
    get(key: string, { logger }?: LogContext): Promise<any>;
    /**
     *
     * @param key - Key to get the value from.
     * @param expTimeInSec - Expiration time to be set.
     * @param logger - The log context.
     */
    expire(key: string, expTimeInSec: number, { logger }?: LogContext): Promise<number>;
    /**
     *
     * @param ctx - Log context.
     */
    disconnect({ logger }?: LogContext): Promise<string>;
}
export { RedisClient, IRedisDriver, IClientConfig, };
