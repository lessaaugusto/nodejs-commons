//#####################################################
// Imports
//#####################################################
import {
  LogContext,
  Logger,
  TraceInfo,
} from "@nosebit/nodejs-logger";
import bcrypt from "bcryptjs";

//#####################################################
// Types
//#####################################################
interface IComparePasswordArgs {
  password: string;
  hashedPassword: string;
}

//#####################################################
// Constants
//#####################################################
const SALT_WORK_FACTOR = 10;

//#####################################################
// Methods
//#####################################################
/**
 * This function compares a raw password and a hashed password to
 * see if both matches.
 *
 * @param args - The arguments object.
 * @param args.password - The raw password as entered by the user.
 * @param args.hashedPassword - The hashed password as stored in db.
 * @param traceInfo - The trace info.
 */
async function comparePassword(
  args: IComparePasswordArgs,
  traceInfo?: TraceInfo,
): Promise<boolean> {
  const logger = new Logger("comparePasswords", { traceInfo });

  logger.info("args", {
    hashedPassword: LogContext.secret(args.hashedPassword),
    password: LogContext.secret(args.password),
  });

  return new Promise<boolean>((resolve, reject) => {
    bcrypt.compare(args.password, args.hashedPassword, (error, match) => {
      if (error) {
        logger.error("bcrypt compare error", error);
        return reject(error);
      }

      logger.info("bcrypt compare success", { match });
      return resolve(match);
    });
  });
}

/**
 * This function hashes a password.
 *
 * @param value - The value to be hashed.
 * @param traceInfo - The trace info.
 */
async function hashPassword(
  value = "",
  traceInfo?: TraceInfo,
): Promise<string> {
  const logger = new Logger("hashPassword", { traceInfo });

  // Local variables
  logger.info("args", {
    value: LogContext.secret(value),
  });

  return new Promise<string>((resolve, reject) => {
    // We use a bcrypt algorithm (see http://en.wikipedia.org/wiki/Bcrypt).
    bcrypt.genSalt(SALT_WORK_FACTOR, (error, salt) => {
      if (error) {
        logger.error("bcrypt genSalt error", error);
        return reject(error);
      }

      logger.info("bcrypt genSalt success", salt);

      // Hash with generated salt.
      return bcrypt.hash(value, salt, (error1, hash) => {
        if (error1) {
          logger.error("bcrypt hash error", error1);
          return reject(error1);
        }

        logger.info("bcrypt hash success", hash);
        return resolve(hash);
      });
    });
  });
}

//#####################################################
// Export
//#####################################################
export {
  comparePassword,
  hashPassword,
  IComparePasswordArgs,
};
