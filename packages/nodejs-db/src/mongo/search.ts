/* eslint-disable import/prefer-default-export */

import {
  globalCtx,
  LogContext,
} from "@nosebit/nodejs-logger";
import lodash from "lodash";
import { ObjectId } from "mongodb";
import moment from "moment";

import {
  ISearchParams,
  ISearchQuery,
} from "../search";

interface IMongoSearchParams {
  query: {[key: string]: any};
  sort?: {[key: string]: number};
  limit?: number;
}

/**
 * This function parse a primitive type to a more complex type
 * based on type user informed.
 *
 * @param value - The primitive value.
 * @param type - The type we wish to convert the primitive value to.
 */
function parseValue(
  value: any,
  type: string,
) {
  switch (type) {
    case "id": {
      return new ObjectId(value);
    }

    case "idList": {
      return value.map((aValue: any) => new ObjectId(aValue));
    }

    case "date": {
      return moment(value).toDate();
    }

    default: {
      break;
    }
  }

  return value === "null" ? null : value;
}

/**
 * This function convert a nosebit search query to a mongo
 * query so we can perform queries in mongo.
 *
 * @param query - The nosebit search query.
 * @param ctx - A log context.
 */
function parseSearchQuery(
  query: ISearchQuery,
  ctx: LogContext = globalCtx,
) {
  const searchKey = query.key;
  const { valueType } = query;
  const mongoQuery: {[key: string]: any} = {};

  if (query._and) {
    mongoQuery.$and = query._and.map((nestedQuery) =>
      parseSearchQuery(nestedQuery, ctx));
  } else if (query._or) {
    mongoQuery.$or = query._or.map((nestedQuery) =>
      parseSearchQuery(nestedQuery, ctx));
  } else if (query._not) {
    mongoQuery.$not = parseSearchQuery(query._not, ctx);
  } else if (query._elemMatch) {
    mongoQuery[searchKey] = {
      $elemMatch: parseSearchQuery(query._elemMatch, ctx),
    };
  } else {
    const keys = Object.keys(query);

    for (const key of keys) {
      const value: any = lodash.get(query, key);

      if (key.startsWith("_map")) {
        value.forEach((mapVal: any, mapKey: string) => {
          mongoQuery[mapKey] = parseValue(mapVal, valueType);
        });
      } else if (key.startsWith("_eq")) {
        mongoQuery[searchKey] = parseValue(value, valueType);
      } else if ((/^_gt|_lt/).test(key)) {
        const opId = key.replace(/_|Str|Int|Float/g, "");

        lodash.set(
          mongoQuery,
          `${searchKey}.$${opId}`,
          parseValue(value, valueType),
        );
      } else if ((/^_regex$/).test(key)) {
        const match = value.match(/\/([^/]+)\/([^/]+)/);

        if (match) {
          mongoQuery[searchKey] = {
            $options: lodash.get(match, "2"),
            $regex: match[1],
          };
        }
      } else if (key.startsWith("_in")) {
        mongoQuery[searchKey] = {
          $in: parseValue(value, valueType),
        };
      } else if (key.startsWith("_all")) {
        mongoQuery[searchKey] = {
          $all: parseValue(value, valueType),
        };
      } else if (key.startsWith("_ne")) {
        mongoQuery[searchKey] = {
          $ne: parseValue(value, valueType),
        };
      }
    }
  }

  return mongoQuery;
}

/**
 * This function convert a search params to a mongo
 * query so we can perform queries in mongo.
 *
 * @param params - The search params to be parsed.
 * @param ctx - A log context.
 */
function parseSearchParams(
  params: ISearchParams,
  ctx: LogContext = globalCtx,
): IMongoSearchParams {
  const { query, sort } = params;

  const result: IMongoSearchParams = {
    limit: params.limit,
    query: parseSearchQuery(query),
  };

  if (!params.includeDeleted) {
    result.query = {
      ...result.query,
      deletedAt: { $type: "null" },
    };
  }

  if (sort) {
    result.sort = sort.reduce<{
      [key: string]: number;
    }>((accum, item) => {
      const parts = item.split(":");
      let order = 1;

      if (parts.length > 1) {
        try {
          order = parseInt(parts[1], 10);
        } catch (error) {
          ctx.logger.error("could not parse order to int", error);
        }
      }

      accum[parts[0]] = order;
      return accum;
    }, {});
  }

  return result;
}

//#####################################################
// Exports
//#####################################################
export {
  parseSearchQuery,
  parseSearchParams,
};
