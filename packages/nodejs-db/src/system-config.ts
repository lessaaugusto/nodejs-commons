/* eslint-disable import/prefer-default-export */

import {
  globalCtx,
  log,
  LogContext,
} from "@nosebit/nodejs-logger";
import { resolveUrl } from "@nosebit/nodejs-utils/lib/dns";
import consul, { Consul } from "consul";
import lodash from "lodash";

//#####################################################
// Types
//#####################################################
interface ISystemConfigOpts {
  consulUrl: string;
  isCacheDisabled?: boolean;
  cacheCountdown?: number;
  namespace: string;
}

//#####################################################
// Main class
//#####################################################
/**
 * This class implements a shared config.
 */
class SystemConfig {
  /**
   * Shared instance.
   */
  private static _shared: SystemConfig;

  /**
   * This function either retrieves a redis client, if it's already created, or initializes one (uses the Singleton pattern).
   *
   * @param opts - Options passed down to the redis client, used to create it with a particular configuration.
   */
  public static sharedInit(opts: ISystemConfigOpts) {
    if (!SystemConfig._shared) {
      SystemConfig._shared = new SystemConfig(opts);
    }

    return SystemConfig._shared;
  }

  /**
   * Get shared instance.
   */
  public static get shared() {
    if (!SystemConfig._shared) {
      throw new Error("system config shared instance not initialized");
    }

    return SystemConfig._shared;
  }

  /**
   * Check if shared instance exists.
   */
  public static sharedExists() {
    return Boolean(SystemConfig._shared);
  }

  /**
   * Consul client to be used throughout this instance.
   */
  private _consulClient: Consul;

  /**
   * Promise indicating if everything is already initialized.
   */
  private readonly _initialized: Promise<void>;

  /**
   * Namespace for this config.
   */
  private readonly _namespace: string;

  /**
   * Flags if cache is disabled.
   */
  private readonly _isCacheDisabled: boolean = false;

  /**
   * Counts the number of times we called get function.
   */
  private _getCount = 0;

  /**
   * Refresh cache after this amount of get requests.
   */
  private readonly _cacheCountdown: number;

  /**
   * We cache the whole config locally and update it from time to
   * time.
   */
  private _cachedConfig: any;

  /**
   * This function returns the initialized attribute.
   */
  get initialized() {
    return this._initialized;
  }

  /**
   *
   * @param opts - A set of options to be passed down to redis client.
   */
  constructor(opts: ISystemConfigOpts) {
    this._isCacheDisabled = opts.isCacheDisabled;
    this._cacheCountdown = opts.cacheCountdown || 100; // eslint-disable-line @typescript-eslint/no-magic-numbers
    this._namespace = opts.namespace || "nosebit.global";
    this._initialized = this._init(opts);
  }

  /**
   * This function initializes this shared config.
   *
   * @param opts - A set of config options.
   * @param ctx - Log context.
   */
  @log({
    argsToLog: ["config"],
    resultToLog: false,
  })
  private async _init(
    opts: ISystemConfigOpts,
    { logger }: LogContext = globalCtx,
  ) {
    try {
      const resolvedUrl = await resolveUrl(opts.consulUrl);
      logger.debug("Resolved url", resolvedUrl);

      this._consulClient = consul({
        host: resolvedUrl.hostname,
        port: `${resolvedUrl.port}`,
      });
    } catch (error) {
      logger.debug("Error trying to resolve redis url");
    }
  }

  /**
   * This private function going to refresh cache if needed.
   *
   * @param opts - A list of options.
   * @param ctx - Log context.
   */
  @log({
    argsToLog: ["opts.{forceCacheRefresh}"],
    ctxArgIdx: 1,
    resultToLog: false,
  })
  private async _refreshCacheIfNeeeded(
    opts?: {
      forceCacheRefresh?: boolean;
    },
    { logger }: LogContext = globalCtx,
  ) {
    return new Promise<any>((resolve, reject) => {
      if (
        (opts && opts.forceCacheRefresh)
        || this._isCacheDisabled
        || !this._cachedConfig
        || this._getCount >= this._cacheCountdown
      ) {
        return this._consulClient.kv.get(
          this._namespace,
          (error: any, data: { Value: string }) => {
            if (error) {
              return reject(error);
            }

            let configObj = {};

            if (data && data.Value) {
              try {
                configObj = JSON.parse(data.Value);
              } catch (_parseError) {
                logger.error(
                  "could not parse config object from consul",
                  data.Value,
                );
              }
            }

            this._cachedConfig = configObj;

            return resolve(configObj);
          }
        );
      }

      return resolve(this._cachedConfig);
    });
  }

  /**
   * This function get a config value by name.
   *
   * @param key - Config key.
   * @param opts - Options.
   * @param logger - Logger context.
   */
  @log({
    argsToLog: [],
    ctxArgIdx: 2,
    resultToLog: false,
  })
  public async get<T = any>(
    key: string,
    opts?: {
      forceCacheRefresh?: boolean;
    },
    { logger }: LogContext = globalCtx,
  ): Promise<T> {
    await this._refreshCacheIfNeeeded(opts);

    this._getCount++;

    const value = lodash.get(this._cachedConfig, key);

    logger.debug("found value", { key, value });

    if (lodash.isUndefined(value)) {
      throw new Error("not found");
    }

    return value;
  }

  /**
   * This function set a config value to shared config.
   *
   * @param key - Config key.
   * @param value - Config value.
   * @param logger - Logger context.
   */
  @log({
    argsToLog: [],
    resultToLog: false,
  })
  public async set(
    key: string,
    value: any,
    { logger }: LogContext = globalCtx,
  ): Promise<void> {
    if (!this._cachedConfig) {
      this._cachedConfig = {};
    }

    /**
     * Write config back to consul to propagate change.
     */
    await this._refreshCacheIfNeeeded({
      forceCacheRefresh: true,
    });

    // Set new config.
    lodash.set(this._cachedConfig, key, value);

    const serializedConfig = JSON.stringify(this._cachedConfig);

    return new Promise((resolve, reject) => this._consulClient.kv.set(
      this._namespace,
      serializedConfig,
      (error) => {
        if (error) {
          logger.error("consulClient set error", error);
          reject(error);
        } else {
          logger.debug("consulClient set success", this._cachedConfig);
          resolve();
        }
      }
    ));
  }

  /**
   * This function set all config object at once.
   *
   * @param value - Config value.
   * @param logger - Logger context.
   */
  @log({
    argsToLog: [],
    resultToLog: false,
  })
  public async setAll(
    value: any,
    { logger }: LogContext = globalCtx,
  ): Promise<void> {
    this._cachedConfig = value;

    /**
     * Write config back to consul to propagate change.
     */
    const serializedConfig = JSON.stringify(this._cachedConfig);

    return new Promise((resolve, reject) => this._consulClient.kv.set(
      this._namespace,
      serializedConfig,
      (error) => {
        if (error) {
          logger.error("consulClient set error", error);
          reject(error);
        } else {
          logger.debug("consulClient set success", this._cachedConfig);
          resolve();
        }
      }
    ));
  }

  /**
   * This function set a config based on a object and his keys.
   *
   * @param batch - Config object.
   * @param logger - Logger context.
   */
  @log({
    argsToLog: [],
    resultToLog: false,
  })
  public async batchSet(
    batch: object,
    { logger }: LogContext = globalCtx,
  ): Promise<void> {
    if (!this._cachedConfig) {
      this._cachedConfig = {};
    }

    /**
     * Write config back to consul to propagate change.
     */
    await this._refreshCacheIfNeeeded({
      forceCacheRefresh: true,
    });

    this._cachedConfig = lodash.merge({}, this._cachedConfig, batch);

    const serializedConfig = JSON.stringify(this._cachedConfig);

    return new Promise((resolve, reject) => this._consulClient.kv.set(
      this._namespace,
      serializedConfig,
      (error) => {
        if (error) {
          logger.error("consulClient set error", error);
          reject(error);
        } else {
          logger.debug("consulClient set success", this._cachedConfig);
          resolve();
        }
      }
    ));
  }
}

//#####################################################
// Export
//#####################################################
export {
  SystemConfig,
};
