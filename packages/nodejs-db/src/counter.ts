/* eslint-disable import/prefer-default-export */

//#####################################################
// Imports
//#####################################################
import {
  globalCtx,
  log,
  LogContext,
} from "@nosebit/nodejs-logger";
import {
  FilterQuery,
  UpdateQuery,
  FindOneAndUpdateOption,
} from "mongodb";

import { MongoClient, Db } from "./mongo";

//#####################################################
// Types
//#####################################################
/**
 * Counter data that is stored in db.
 */
interface ICounter {
  _id: string;
  count: number;
}

/**
 * Interface for the insert result.
 */
interface IDbInsertResult {
  insertedId: string | {
    toString: () => string;
  };
}

/**
 * Interface for the insert result.
 */
interface IDbFindOneAndUpdateResult {
  value?: ICounter;
}

/**
 * Interface for mongodb collection driver.
 */
interface ICounterCollection {
  insertOne: (counter: ICounter) => Promise<IDbInsertResult>;
  findOneAndUpdate: (
    filter: FilterQuery<ICounter>,
    update: UpdateQuery<ICounter>,
    option: FindOneAndUpdateOption,
  ) => Promise<IDbFindOneAndUpdateResult>;
}

/**
 * Interface for counter config options.
 */
interface ICounterConfig {
  mode?: string;
  collection?: ICounterCollection;
  name?: string;
}

//#####################################################
// Main class
//#####################################################
/**
 * This class handles the counters collection.
 */
class Counter {
  /**
   * Shared instance.
   */
  private static _shared: Counter;

  /**
   * This function initializes a shared mongo client.
   *
   * @param config - A set of config options.
   */
  public static async sharedInit(config: ICounterConfig) {
    if (!Counter._shared) {
      Counter._shared = new Counter();

      // Select counter right away.
      if (config.name) {
        await Counter._shared.select(config.name);
      }
    }

    return Counter._shared;
  }

  /**
   * This function gets the shared instance.
   */
  public static get shared() {
    if (!Counter._shared) {
      throw new Error("shared counter not initialized");
    }

    return Counter._shared;
  }

  /**
   * Check if shared instance exists.
   */
  public static sharedExists() {
    return Boolean(Counter._shared);
  }

  /**
   * This function setup the counter collection.
   *
   * @param db - The database connector instance.
   * @param parentCtx - Log parent context.
   */
  public static setupCollection(
    db?: Partial<Db>,
    parentCtx: LogContext = globalCtx,
  ) {
    return LogContext.trace({
      fileName: __filename,
      methodId: "setupCollection",
      parentCtx,
    }, async (ctx: LogContext) => {
      const dbInstance = db || MongoClient.shared.db;

      // Try to create the collection
      try {
        await dbInstance.createCollection("counters", {
          readPreference: "secondaryPreferred",
        });

        ctx.logger.debug("collection create success");
      } catch (error) {
        ctx.logger.error("collection create error", error);
      }
    });
  }

  /**
   * The selected name for this counter.
   */
  private _name: string;

  /**
   * The collection.
   */
  private readonly _collection: ICounterCollection;

  /**
   * This function creates a new instance.
   *
   * @param config - A set of config options.
   */
  constructor(config: ICounterConfig = {}) {
    this._collection = config.collection
      || MongoClient.shared.db.collection("counters");
  }

  /**
   * This function selects an entry in counters collection.
   *
   * @param name - The name of collection to be selected.
   * @param ctx - The log context.
   */
  @log({
    argsToLog: ["name"],
  })
  public async select(
    name: string,
    ctx: LogContext = globalCtx,
  ) {
    try {
      const result = await this._collection.insertOne({
        _id: name,
        count: 0,
      });

      ctx.logger.debug("collection insertOne success", {
        id: result.insertedId,
      });
    } catch (error) {
      ctx.logger.error("collection insertOne error", error);
    }

    // Set name as selected.
    this._name = name;
  }

  /**
   * This function generate next count for a specific name.
   *
   * @param name - The name of collection that we should get next count.
   * @param ctx - The log context.
   */
  @log({
    argsToLog: ["name"],
  })
  public async getNextCount(
    name?: string,
    ctx: LogContext = globalCtx,
  ) {
    if (!name && !this._name) {
      throw new Error("no counter selected");
    }

    const result = await this._collection.findOneAndUpdate({
      _id: name || this._name,
    }, { $inc: { count: 1 } }, { returnOriginal: false, upsert: true });

    ctx.logger.debug("collection findOneAndUpdate success", result);

    return result.value.count;
  }
}

//#####################################################
// Export
//#####################################################
export {
  Counter,
};
