declare module "hdkey" {

  /**
   * HDNode class.
   */
  export default class HDNode {
    public static fromMasterSeed(seed: Buffer): HDNode;

    public publicKey: Buffer;

    public privateKey: Buffer;

    public publicExtendedKey: string;

    public privateExtendedKey: string;

    public chainCode: Buffer;

    public derive(path: string): HDNode;
  }
}
