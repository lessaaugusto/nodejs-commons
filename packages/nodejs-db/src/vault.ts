//#####################################################
// Imports
//#####################################################
import {
  globalCtx,
  log,
  LogContext,
} from "@nosebit/nodejs-logger";
import bip39 from "bip39";
import hdkey from "hdkey";
import vault, { client as VaultDriver } from "node-vault";

//#####################################################
// Types
//#####################################################
interface IVaultClientConfig {
  url: string;
  token: string;
}

interface IVaultClient {
  init?: (config: IVaultClientConfig) => IVaultClient;

  driver?: {
    write: (path: string, data: any) => Promise<any>;
    read: (path: string) => Promise<any>;
  };

  createSeed?: (id: string) => Promise<string>;
  deriveKeyPair?: (id: string, path: string) => Promise<IKeyPair>;
}

interface IKeyPair {
  privateKey: Buffer;
  publicKey: Buffer;
}

//#####################################################
// Main class
//#####################################################
/**
 * This is the main class implementing vault client.
 */
class VaultClient {
  /**
   * Shared instance.
   */
  private static _shared: VaultClient;

  /**
   * This function initializes a new shared instance of this class.
   *
   * @param config - A set of config options.
   */
  public static sharedInit(config: IVaultClientConfig) {
    if (!VaultClient._shared) {
      VaultClient._shared = new VaultClient(config);
    }

    return VaultClient._shared;
  }

  /**
   * This function retreives the shared instance.
   */
  public static get shared() {
    if (!VaultClient._shared) {
      throw new Error("vault shared client not initialized");
    }

    return VaultClient._shared;
  }

  /**
   * Check if shared instance exists.
   */
  public static sharedExists() {
    return Boolean(VaultClient._shared);
  }

  /**
   * This is the vault driver.
   */
  private readonly _driver: VaultDriver;

  /**
   * This function retrieves the vault driver.
   */
  public get driver() {
    return this._driver;
  }

  /**
   * This function creates a new instance of this class.
   *
   * @param config - A set of config options.
   */
  constructor(config: IVaultClientConfig) {
    this._driver = vault({
      endpoint: config.url,
      token: config.token,
    });
  }

  /**
   * This function creates new seed.
   *
   * @param id - Identification string to be associated with seed.
   * @param ctx - Log context.
   */
  @log()
  public async createSeed(
    id: string,
    ctx: LogContext = globalCtx,
  ): Promise<string> {
    // @TODO : Should we send mnemonic to user in the response?
    const mnemonic = bip39.generateMnemonic();
    ctx.logger.debug("mnemonic", LogContext.secret(mnemonic));

    const seed = bip39.mnemonicToSeed(mnemonic);
    ctx.logger.debug("seed", LogContext.secret(seed.toString("hex")));

    try {
      await this._driver.write(`secret/${id}`, {
        value: seed.toString("hex"),
      });

      ctx.logger.debug("seed store success");
    } catch (error) {
      ctx.logger.error("seed store fail", error);
      throw error;
    }

    return mnemonic;
  }

  /**
   * This function derives a new key pay for a specific id.
   *
   * @param id - The identification associated with seed.
   * @param path - The hierarchical path to generate key pair.
   * @param ctx - The log context.
   */
  @log({
    argsToLog: ["id", "path"],
  })
  public async deriveKeyPair(
    id: string,
    path: string,
    ctx: LogContext = globalCtx,
  ): Promise<IKeyPair> {
    let seed: string;

    /**
     * Retrieve the seed from vault.
     *
     * @todo : This is very sensitive so this should be tested
     * for all possible failures/attacks.
     */
    try {
      seed = await this._driver.read(`secret/${id}`) as string;

      ctx.logger.debug("vault read success", seed);
    } catch (error) {
      ctx.logger.error("vault read fail", error);
      throw error;
    }

    // Regenerate the root key.
    const root = hdkey.fromMasterSeed(Buffer.from(seed));

    // Derive keyPair associated with provided path.
    const keyPair = root.derive(path);

    // Return derived key pair.
    return keyPair;
  }
}

//#####################################################
// Export
//#####################################################
export {
  IKeyPair,
  IVaultClient,
  IVaultClientConfig,
  VaultClient,
};
