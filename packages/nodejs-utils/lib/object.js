"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Methods
//#####################################################
/**
 * This function convert an object like entity to plain object.
 *
 * @param objLike - A structure that could be parsed to plain object.
 */
function toPlainObject(objLike) {
    return JSON.parse(JSON.stringify(objLike));
}
exports.toPlainObject = toPlainObject;
/**
 * This function remove all nulls presented in an object.
 *
 * @param objLike - A structure that could be parsed to plain object.
 */
function removeNulls(objLike) {
    const plainObj = toPlainObject(objLike);
    Object.keys(plainObj).forEach((key) => ( // eslint-disable-line @typescript-eslint/no-extra-parens
    (plainObj[key] === null) && delete plainObj[key] // eslint-disable-line @typescript-eslint/no-dynamic-delete
    ));
    return plainObj;
}
exports.removeNulls = removeNulls;
//# sourceMappingURL=object.js.map