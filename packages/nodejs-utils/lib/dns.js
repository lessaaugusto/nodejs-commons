"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Imports
//#####################################################
const dns_1 = __importDefault(require("dns"));
//#####################################################
// Methods
//#####################################################
/**
 * This function breaks an url into it's parts. By url
 * we mean somethig with the following schema.
 *
 * [protocol://user:password@hostname:port/path].
 *
 * @param url - The url to be broken.
 * @param opts - A set of options.
 */
function getUrlParts(url, opts = {}) {
    const result = {
        host: null,
        hostname: null,
        origin: null,
        password: null,
        path: null,
        port: null,
        protocol: null,
        user: null,
        userinfo: null,
    };
    let parts = url.split("://");
    if (parts.length > 1) {
        result.protocol = parts.shift();
    }
    parts = parts[0].split("@");
    if (parts.length === 2) {
        result.userinfo = parts.shift();
        const userParts = result.userinfo.split(":");
        result.user = userParts.shift();
        result.password = userParts.length ? userParts.shift() : null;
    }
    parts = parts[0].split(":");
    if (parts.length === 2) {
        result.hostname = parts.shift();
        result.port = parseInt(parts.shift(), 10);
        result.host = `${result.hostname}:${result.port}`;
    }
    if (parts.length === 1) {
        result.hostname = parts.shift();
        if (opts.port) {
            result.port = opts.port;
            result.host = `${result.hostname}:${result.port}`;
        }
        else {
            result.host = result.hostname;
        }
    }
    result.origin = result.protocol ? `${result.protocol}://` : "";
    result.origin += result.hostname;
    result.origin += result.port ? `:${result.port}` : "";
    return result;
}
exports.getUrlParts = getUrlParts;
/**
 * This function resolves an url hostname to ip.
 *
 * @param url - The url to be resolved.
 * @param opts - A set of options.
 */
function resolveUrlHostname(url, opts) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            const urlParts = getUrlParts(url);
            dns_1.default.resolve4(urlParts.hostname, (error, ips) => {
                if (error) {
                    return reject(error.message);
                }
                else if (ips.length === 0) {
                    return reject(new Error("could not resolve hostname"));
                }
                let ip;
                // Try to find an non-ignored ip.
                if (opts && opts.ignoredIps) {
                    for (let i = 0; i < ips.length; i++) {
                        if (!opts.ignoredIps.find((ignoredIp) => ignoredIp === ips[i])) {
                            ip = ips[i];
                            break;
                        }
                    }
                    // We could not find a suitable address.
                    if (!ip) {
                        return reject(new Error("coild not resolve hostname"));
                    }
                }
                else {
                    ip = ips[0];
                }
                urlParts.hostname = ip;
                const protocolPrefix = urlParts.protocol ? `${urlParts.protocol}://` : "";
                const portSuffix = urlParts.port ? `:${urlParts.port}` : "";
                urlParts.host = `${ip}${portSuffix}`;
                urlParts.origin = `${protocolPrefix}${urlParts.host}`;
                return resolve(urlParts);
            });
        });
    });
}
exports.resolveUrlHostname = resolveUrlHostname;
/**
 * This function resolves a service port.
 *
 * @param url - The url to be resolved.
 */
function resolveUrlPort(url) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            const urlParts = getUrlParts(url);
            if (urlParts.port) {
                resolve(urlParts);
            }
            else {
                dns_1.default.resolveSrv(urlParts.hostname, (error, addrs) => {
                    if (error) {
                        return reject(error.message);
                    }
                    else if (addrs.length === 0) {
                        return reject(new Error("service not found"));
                    }
                    else if (!addrs[0].port) {
                        return reject(new Error("service port not found"));
                    }
                    const [addr] = addrs;
                    urlParts.port = addr.port;
                    urlParts.origin += `:${addr.port}`;
                    urlParts.host += `:${addr.port}`;
                    return resolve(urlParts);
                });
            }
        });
    });
}
exports.resolveUrlPort = resolveUrlPort;
/**
 * This function resolves a service url.
 *
 * @deprecated In favor of resolveUrlPort and resolveUrlHostname
 *
 * @param url - The url to be resolved.
 */
function resolveUrl(url) {
    return __awaiter(this, void 0, void 0, function* () {
        return new Promise((resolve, reject) => {
            const urlParts = getUrlParts(url);
            if (urlParts.port) {
                resolve(urlParts);
            }
            else {
                dns_1.default.resolveSrv(urlParts.hostname, (error, addrs) => {
                    if (error) {
                        return reject(error.message);
                    }
                    else if (addrs.length === 0) {
                        return reject(new Error("service not found"));
                    }
                    const [addr] = addrs;
                    urlParts.port = addr.port;
                    urlParts.origin += `:${addr.port}`;
                    urlParts.host += `:${addr.port}`;
                    return resolve(urlParts);
                });
            }
        });
    });
}
exports.resolveUrl = resolveUrl;
//# sourceMappingURL=dns.js.map