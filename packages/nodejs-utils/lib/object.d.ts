/**
 * This function convert an object like entity to plain object.
 *
 * @param objLike - A structure that could be parsed to plain object.
 */
declare function toPlainObject<T extends object>(objLike: T): {
    [key: string]: any;
};
/**
 * This function remove all nulls presented in an object.
 *
 * @param objLike - A structure that could be parsed to plain object.
 */
declare function removeNulls<T extends object>(objLike: T): Partial<T>;
export { removeNulls, toPlainObject, };
