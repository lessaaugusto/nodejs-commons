/**
 * Options to retrieve caller info.
 */
interface ICallerInfoOpts {
    depth: number;
}
/**
 * Caller info.
 */
interface ICallerInfo {
    file: string;
    name: string;
    line: string;
    column: string;
    id: string;
}
/**
 * This function gets caller info like caller file name,
 * function name and others.
 *
 * @param stack - The error stack.
 * @param opts - The options.
 */
declare function getCallerInfo(stack: string, { depth }: ICallerInfoOpts): ICallerInfo;
/**
 * This function gets only the filepath of the caller.
 *
 * @param stack - The call stack.
 * @param opts - The options.
 */
declare function getCallerFilepath(stack: string, { depth }: ICallerInfoOpts): string;
/**
 * This function retrieves the current call stack.
 */
declare function getCallStack(): ICallerInfo[];
export { ICallerInfoOpts, ICallerInfo, getCallerInfo, getCallerFilepath, getCallStack, };
