"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//#####################################################
// Functions
//#####################################################
/**
 * This function gets caller info like caller file name,
 * function name and others.
 *
 * @param stack - The error stack.
 * @param opts - The options.
 */
function getCallerInfo(stack, { depth = 1 }) {
    let stackList = stack.replace(/(  +)|(at )/g, "").split("\n");
    // Remove error and direct caller
    stackList = stackList.slice(1 + depth); // eslint-disable-line no-magic-numbers
    const callerInfo = stackList.shift().split(/ ?\(/);
    // Caller
    const name = callerInfo.length > 1 // eslint-disable-line no-magic-numbers
        ? callerInfo[0]
        : null;
    // File
    const fileInfo = (callerInfo.length > 1 // eslint-disable-line no-magic-numbers
        ? callerInfo[1]
        : callerInfo[0]).replace(")", "").split(":");
    const file = fileInfo[0].replace(process.cwd(), "").replace(/^\//, "");
    const [, line, column] = fileInfo;
    return {
        column,
        file,
        id: `${file} - ${name}`,
        line,
        name,
    };
}
exports.getCallerInfo = getCallerInfo;
/**
 * This function gets only the filepath of the caller.
 *
 * @param stack - The call stack.
 * @param opts - The options.
 */
function getCallerFilepath(stack, { depth = 1 }) {
    let stackList = stack.replace(/(  +)|(at )/g, "").split("\n");
    // Remove error and direct caller
    stackList = stackList.slice(1 + depth); // eslint-disable-line no-magic-numbers
    const callerInfo = stackList.shift().split(/ ?\(/);
    // File
    const fileInfo = (callerInfo.length > 1 // eslint-disable-line no-magic-numbers
        ? callerInfo[1]
        : callerInfo[0]).replace(")", "").split(":");
    const file = fileInfo[0].replace(process.cwd(), "").replace(/^\//, "");
    const [, line, column] = fileInfo;
    return `${file}:${line}:${column}`;
}
exports.getCallerFilepath = getCallerFilepath;
/**
 * This function retrieves the current call stack.
 */
function getCallStack() {
    const { stack } = new Error();
    let stackList = stack.replace(/(  +)|(at )/g, "").split("\n");
    stackList = stackList.slice(1); // eslint-disable-line no-magic-numbers
    const callStack = [];
    for (let idx = 0; idx < stackList.length; idx++) { // eslint-disable-line no-magic-numbers
        const stackItem = stackList[idx];
        const callerInfo = stackItem.split(/ ?\(/);
        // Caller
        const name = callerInfo.length > 1 // eslint-disable-line no-magic-numbers
            ? callerInfo[0]
            : null;
        // File
        const fileInfo = (callerInfo.length > 1 // eslint-disable-line no-magic-numbers
            ? callerInfo[1]
            : callerInfo[0]).replace(")", "").split(":");
        const file = fileInfo[0].replace(process.cwd(), "").replace(/^\//, "");
        const [, line, column] = fileInfo;
        callStack.push({
            column,
            file,
            id: `${file} - ${name}`,
            line,
            name,
        });
    }
    return callStack;
}
exports.getCallStack = getCallStack;
//# sourceMappingURL=caller.js.map