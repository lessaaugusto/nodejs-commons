/**
 * This type is used to modify thrift types to match
 * what going to be retrieved from mongodb.
 */
export declare type Modify<TTypeA, TTypeB> = Pick<TTypeA, Exclude<keyof TTypeA, keyof TTypeB>> & TTypeB;
