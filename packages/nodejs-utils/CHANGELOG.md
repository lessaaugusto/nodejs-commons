# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.8.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-utils@0.8.0...@nosebit/nodejs-utils@0.8.1) (2019-11-30)

**Note:** Version bump only for package @nosebit/nodejs-utils





# [0.8.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-utils@0.7.0...@nosebit/nodejs-utils@0.8.0) (2019-11-30)


### Features

* Add volta config and replace redis by ioredis. ([24f11f2](https://github.com/nosebit/nodejs-commons/commit/24f11f2cc2ca16538c0457c5ccd3de562db74051))





# [0.7.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-utils@0.6.1...@nosebit/nodejs-utils@0.7.0) (2019-11-28)


### Features

* Add volta config and replace redis by ioredis. ([cdc915a](https://github.com/nosebit/nodejs-commons/commit/cdc915a))





## [0.6.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-utils@0.6.0...@nosebit/nodejs-utils@0.6.1) (2019-11-27)


### Bug Fixes

* Upgrade @types/node version which was conflicting with typescript own internal types and prevent lib from being built. ([f18913f](https://github.com/nosebit/nodejs-commons/commit/f18913f))
* Use ~ in package.jsons instead of ^. ([d31040a](https://github.com/nosebit/nodejs-commons/commit/d31040a))





# [0.6.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-utils@0.5.0...@nosebit/nodejs-utils@0.6.0) (2019-08-21)


### Features

* **nodejs-utils:** Add types file to nodejs-utils to expose type aux stuff. ([35c5365](https://github.com/nosebit/nodejs-commons/commit/35c5365))





# [0.5.0](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-utils@0.4.1...@nosebit/nodejs-utils@0.5.0) (2019-07-06)


### Features

* **nodejs-utils:** Add method to delay execution for some time using async/await. ([330d0f2](https://github.com/nosebit/nodejs-commons/commit/330d0f2))





## [0.4.1](https://github.com/nosebit/nodejs-commons/compare/@nosebit/nodejs-utils@0.4.0...@nosebit/nodejs-utils@0.4.1) (2019-06-09)

**Note:** Version bump only for package @nosebit/nodejs-utils





# 0.4.0 (2019-05-12)


### Features

* migrate current code from github. ([7da766d](https://github.com/nosebit/nodejs-commons/commit/7da766d))
