#!/bin/bash
################################################
# Global Variables
################################################
export PATH=$(pwd)/node_modules/.bin:$(pwd)/../../node_modules/.bin:$PATH

################################################
# Aux functions
################################################
log() {
  echo ">> [nodejs-commons] " $@
}

################################################
# Bake Recipes
################################################
###%
#@ bake build
##
## This recipe transpile typescript to javascript.
##
bake-build() {
  log "Building ..."
  lerna run tsc
}

###%
#@ bake build-push
##
## This recipe builds the project and then push the changes.
##
bake-build-push() {
  log "Building and pushing"
  bake-build

  log "Git Status"
  git status

  log "Git Status Porcelain"
  git status --porcelain

  if [ -n "$(git status --porcelain)" ]; then
    git add .
    git commit -m 'build(release): build libs for release [skip-ci]'
    git push
  else
    log "Nothing to commit after build"
  fi
}

###%
#@ bake commit
##
## This recipe going to commit using commitzen.
##
bake-commit() {
  git-cz
}

###%
#@ bake commit-all
##
## This recipe going to add everything to stage and then
## commit using commitzen.
##
bake-commit-all() {
  git add .
	bake-commit
}

###%
#@ bake deps
##
## This recipe install all necessary dependencies.
##
bake-deps() {
  log "Installing dependencies"
  yarn install
}

###%
#@ bake git-hooks
##
## This recipe going to install git hooks for this repo.
##
bake-git-hooks() {
  log "Installing git hooks"

  basedir=$PWD
	mkdir -p .git/hooks
	rm -f .git/hooks/*
	chmod u+x ./hooks/*

	for f in $(ls ./hooks); do
	  ln -s ../../hooks/$f $basedir/.git/hooks/$f
	done
}

###%
#@ bake init
##
## This recipe going to initialize this app/service.
##
bake-init() {
  log "Initializing"
  bake-deps
  bake-setup
}

###%
#@ bake lint
##
## This recipe going to lint the code.
##
bake-lint() {
  log "Linting"
  eslint . --ext .ts --ext .js
}

###%
#@ bake publish
##
## This recipe going to publish all packages in this workspace.
##
bake-publish() {
  lerna publish --yes
}

###%
#@ bake publish-npm
##
## This recipe going to publish all packages in this workspace.
##
bake-publish-npm() {
  npm publish --access=public
}

###%
#@ bake clone
##
## This recipe going to setup this context.
##
bake-setup() {
  log "Setting up"
  bake-git-hooks
}

###%
#@ bake test-unit
##
## This recipe going to run local unit tests.
##
bake-test-unit() {
  log "Running unit tests"
	NODE_ENV=test-unit jest $@ --passWithNoTests --config ./jest.config.js
}

###%
#@ bake test-unit-ci
##
## This recipe going to run CI unit tests.
##
bake-test-unit-ci() {
  log "Running CI unit tests"
	NODE_ENV=test-unit-ci jest --passWithNoTests --config ./jest.config.js
}

###%
#@ bake version
##
## This recipe going to check version.
##
bake-version() {
	lerna version
}

################################################
# Main
################################################
# Process subcommand
subcommand=$1

case $subcommand in
  "help" |  "-h" | "--help")
  bake-help
  ;;
  *)
    shift
    bake-${subcommand} $@
    EXIT_CODE=$(echo $?)

    if [ "$EXIT_CODE" = 127 ]; then
        echo "Error: '$subcommand' is not a known subcommand." >&2
        echo "       Run '$ProgName --help' for a list of known subcommands." >&2
    fi

    exit $EXIT_CODE
  ;;
esac
